<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFullWidthFeatruesTable extends Migration
{
    public function up()
    {
        Schema::create('full_width_featrues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->longText('description');
            $table->string('image')->nullable();
            $table->string('lang');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('full_width_featrues');
    }
}
