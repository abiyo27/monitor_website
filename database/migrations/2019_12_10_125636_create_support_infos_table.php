<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportInfosTable extends Migration
{
    
    public function up()
    {
        Schema::create('support_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('details');
            $table->string('icon');
            $table->string('lang');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('support_infos');
    }
}
