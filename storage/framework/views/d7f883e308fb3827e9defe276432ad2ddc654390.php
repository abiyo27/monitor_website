<?php $__currentLoopData = $all_full_width_features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($user_select_lang_slug == $data->lang): ?>
    <div class="block-feature-area padding-top-120" id="<?php echo e(get_static_option('about_page_slug')); ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-feature-item">
                        <div class="row">
                            <div class="col-lg-6 <?php if($key % 2): ?> order-lg-2 <?php endif; ?>">
                                <div class="img-wrapper box-shadow-90">
                                    <?php echo render_image_markup_by_attachment_id($data->image); ?>

                                </div>
                            </div>
                            <div class="col-lg-6 <?php if($key % 2): ?> order-lg-1 <?php endif; ?>">
                                <div class="content-block-area  <?php if(!$key % 2): ?> padding-left-50 <?php endif; ?>">
                                    <a href="<?php echo e(route('frontend.full.feature.details',['id' => $data->id, 'any' => Str::slug($data->title)])); ?>"><h4 class="title wow fadeInUp"><?php echo e($data->title); ?></h4></a>
                                    <div class="content-area">
                                        <?php echo Str::words($data->description,35); ?>

                                    </div>
                                    <div class="btn-wrapper margin-top-20 wow fadeInDown">
                                        <a href="<?php echo e(route('frontend.full.feature.details',['id' => $data->id, 'any' => Str::slug($data->title)])); ?>" class="boxed-btn gd-bg br-5 w180px"><?php echo e(__('Read More')); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH /var/www/resources/views/frontend/partials/full-width-feature.blade.php ENDPATH**/ ?>