<div class="testimonial-area padding-bottom-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="testimonial-carousel-wrapper">

                    <ul class="slider-nav">
                        <?php $__currentLoopData = $all_testimonial; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($user_select_lang_slug == $data->lang): ?>
                            <li class="single-nav-item">
                                <div class="img-wrap">
                                    <?php echo render_image_markup_by_attachment_id($data->image); ?>

                                </div>
                            </li>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                    <div class="testimonial-carousel slider-for">
                        <?php $__currentLoopData = $all_testimonial; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($user_select_lang_slug == $data->lang): ?>
                            <div class="single-testimonial-item">
                                <div class="icon">
                                    <i class="fas fa-quote-left"></i>
                                </div>
                                <div class="description">
                                    <p><?php echo e($data->description); ?></p>
                                    <div class="author-meta">
                                        <h5 class="name"><?php echo e($data->name); ?></h5>
                                        <span class="designation"><?php echo e($data->designation); ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /var/www/resources/views/frontend/partials/testimonial.blade.php ENDPATH**/ ?>