<div class="topbar-area black home-<?php echo e(get_static_option('home_page_variant')); ?> ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="topbar-inner">
                    <div class="left-content-area">
                        <ul class="info-items">
                            <?php $__currentLoopData = $all_support_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($user_select_lang_slug == $data->lang): ?>
                                <li><i class="<?php echo e($data->icon); ?>"></i> <?php echo e($data->details); ?></li>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                    <div class="right-content-area">
                        <div class="right-inner">
                            <ul class="social-icons">
                                <?php $__currentLoopData = $all_social_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e($data->url); ?>"><i class="<?php echo e($data->icon); ?>"></i></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php if(auth()->check()): ?>
                                    <?php
                                        $route = auth()->guest() == 'admin' ? route('admin.home') : route('user.home');
                                    ?>
                                    <li><a href="<?php echo e($route); ?>"><?php echo e(__('Dashboard')); ?></a>  <span>/</span>
                                        <a href="<?php echo e(route('user.logout')); ?>"
                                           onclick="event.preventDefault();
                                                     document.getElementById('userlogout-form').submit();">
                                            <?php echo e(__('Logout')); ?>

                                        </a>
                                        <form id="userlogout-form" action="<?php echo e(route('user.logout')); ?>" method="POST" style="display: none;">
                                            <?php echo csrf_field(); ?>
                                        </form>
                                    </li>
                                <?php else: ?>
                                    <li><a href="<?php echo e(route('user.login')); ?>"><?php echo e(__('Login')); ?></a> <span>/</span> <a href="<?php echo e(route('user.register')); ?>"><?php echo e(__('Register')); ?></a></li>
                                <?php endif; ?>
                            </ul>
                            <?php if(!empty(get_static_option('language_select_option'))): ?>
                            <select id="langchange">
                                <?php $__currentLoopData = $all_language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option <?php if(session()->get('lang') == $lang->slug): ?> selected <?php endif; ?> value="<?php echo e($lang->slug); ?>"><?php echo e($lang->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /var/www/resources/views/frontend/partials/support.blade.php ENDPATH**/ ?>