<?php $home_variant = isset($home_variant) ?? ''; ?> 
<nav class="navbar navbar-area navbar-expand-lg absolute">
    <div class="container-fluid nav-container">
        <div class="logo-wrapper navbar-brand">
            <a href="<?php echo e(url('/')); ?>" class="logo">
              <?php if(!empty($home_variant)): ?>
                 <?php echo render_image_markup_by_attachment_id(get_static_option('site_logo')); ?>

              <?php else: ?> 
                <?php if(request()->is('/') ): ?>
                    <?php echo render_image_markup_by_attachment_id(get_static_option('site_logo')); ?>

                <?php else: ?>
                    <?php echo render_image_markup_by_attachment_id(get_static_option('site_white_logo')); ?>

                <?php endif; ?>
              <?php endif; ?>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="cgency">
            <!-- navbar collapse start -->
            <ul class="navbar-nav" id="primary-menu">
                <!-- navbar- nav -->
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo e(url('/')); ?>"><?php echo e(__('Home')); ?></a>
                </li>
                <?php if(!empty(get_static_option('full_width_features_section_status'))): ?>
                <li class="nav-item">
                    <a class="nav-link" <?php if(request()->path() == "/"): ?>
                                        href="#<?php echo e(get_static_option('about_page_slug')); ?>"
                                        <?php else: ?>
                                        href="<?php echo e(url('/#'.get_static_option('about_page_slug'))); ?>"
                                        <?php endif; ?> ><?php echo e(get_static_option('about_page_'.$user_select_lang_slug.'_name')); ?></a>
                </li>
                <?php endif; ?>
                <?php if(!empty(get_static_option('why_us_section_status'))): ?>
                <li class="nav-item dropdown">
                    <a class="nav-link" <?php if(request()->path() == "/"): ?>
                                        href="#<?php echo e(get_static_option('feature_page_slug')); ?>"
                                        <?php else: ?>
                                        href="<?php echo e(url('/#'.get_static_option('feature_page_slug'))); ?>"
                                        <?php endif; ?> ><?php echo e(get_static_option('feature_page_'.$user_select_lang_slug.'_name')); ?></a>
                </li>
                <?php endif; ?>
                <?php if(!empty(get_static_option('price_plan_section_status'))): ?>
                <li class="nav-item">
                    <a class="nav-link" <?php if(request()->path() == "/"): ?>
                                        href="#<?php echo e(get_static_option('price_plan_page_slug')); ?>"
                                        <?php else: ?>
                                        href="<?php echo e(url('/#'.get_static_option('price_plan_page_slug'))); ?>"
                                        <?php endif; ?> ><?php echo e(get_static_option('price_plan_page_'.$user_select_lang_slug.'_name')); ?></a>
                </li>
                <?php endif; ?>
                <?php if(!empty(get_static_option('team_member_section_status'))): ?>
                <li class="nav-item">
                    <a class="nav-link" <?php if(request()->path() == "/"): ?>
                                        href="#<?php echo e(get_static_option('team_page_slug')); ?>"
                                        <?php else: ?>
                                        href="<?php echo e(url('/#'.get_static_option('team_page_slug'))); ?>"
                                        <?php endif; ?> ><?php echo e(get_static_option('team_page_'.$user_select_lang_slug.'_name')); ?></a>
                </li>
                <?php endif; ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo e(route('frontend.blog')); ?>"><?php echo e(get_static_option('blog_page_'.$user_select_lang_slug.'_name')); ?></a>
                </li>
                <?php if(!empty(get_static_option('contact_section_status'))): ?>
                <li class="nav-item">
                    <a class="nav-link" <?php if(request()->path() == "/"): ?>
                                        href="#<?php echo e(get_static_option('contact_page_slug')); ?>"
                                        <?php else: ?>
                                        href="<?php echo e(url('/#'.get_static_option('contact_page_slug'))); ?>"
                                        <?php endif; ?> ><?php echo e(get_static_option('contact_page_'.$user_select_lang_slug.'_name')); ?></a>
                </li>
                <?php endif; ?>
            </ul>
            <!-- /.navbar-nav -->
        </div>
        <!-- /.navbar btn wrapper -->
        <div class="responsive-mobile-menu">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#cgency" aria-controls="cgency"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <!-- navbar collapse end -->
        <?php if(!empty(get_static_option('navbar_button'))): ?>
        <div class="nav-right-content">
            <ul>
                <li class="nav-btn">
                    <a href="<?php echo e(get_static_option('navbar_button_url_'.$user_select_lang_slug)); ?>" class="boxed-btn <?php if(!empty(filter_static_option_value('site_rtl_enabled',$global_static_field_data)) || get_user_lang_direction() == 'rtl'): ?>gd-bg <?php else: ?> blank  <?php endif; ?>"><?php echo e(get_static_option('navbar_button_text_'.$user_select_lang_slug)); ?></a>
                </li>
                
            </ul>
        </div>
        <?php endif; ?>
    </div>
</nav>
<!-- navbar area end --><?php /**PATH /var/www/resources/views/frontend/partials/navbar.blade.php ENDPATH**/ ?>