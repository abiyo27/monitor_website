
<div class="intro-video-area intro-video-bg padding-120"
<?php echo render_background_image_markup_by_attachment_id(get_static_option('home_page_01_intro_video_bg_image')); ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="intro-video-inner">
                    <div class="img-wrapper">
                        <?php echo render_image_markup_by_attachment_id(get_static_option('home_page_01_intro_video_image')); ?>

                        <div class="hover">
                            <a href="<?php echo e(get_static_option('home_page_01_intro_video_url')); ?>" class="video-play-btn mfp-iframe"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div><?php /**PATH /var/www/resources/views/frontend/partials/intro-video.blade.php ENDPATH**/ ?>