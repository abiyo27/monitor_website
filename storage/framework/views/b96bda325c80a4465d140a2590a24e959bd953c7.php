<style>
    :root {
        --main-color-one: <?php echo e(get_static_option('site_color')); ?>;
        --secondary-color: <?php echo e(get_static_option('site_main_color_two')); ?>;
        --heading-font: "<?php echo e(get_static_option('heading_font_family')); ?>",sans-serif;
        --body-font:"<?php echo e(get_static_option('body_font_family')); ?>",sans-serif;
    }
</style><?php /**PATH /var/www/resources/views/frontend/partials/root-style.blade.php ENDPATH**/ ?>