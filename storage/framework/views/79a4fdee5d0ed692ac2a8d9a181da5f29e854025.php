<!-- header area start -->
<div class="header-area header-bg-2 style-two" id="home"
<?php echo render_background_image_markup_by_attachment_id(get_static_option('home_page_02_header_bg_image')); ?>>
    <div class="header-area-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="header-inner"><!-- header inner -->
                        <h1 class="title wow FadeInDown"><?php echo e(get_static_option('home_page_header_title_'.$user_select_lang_slug)); ?></h1>
                        <p><?php echo e(get_static_option('home_page_header_description_'.$user_select_lang_slug)); ?></p>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(get_static_option('home_page_header_btn_one_url_'.$user_select_lang_slug)); ?>" class="boxed-btn gd-bg"><?php echo e(get_static_option('home_page_header_btn_one_text_'.$user_select_lang_slug)); ?></a>
                        </div>
                    </div><!-- //. header inner -->
                </div>
            </div>
        </div>
    </div>
    <div class="header-right-image">
        <div class="header-right-image-animation">
            <?php echo render_image_markup_by_attachment_id(get_static_option('home_page_02_header_right_image')); ?>

        </div>
    </div>
    <?php if(!empty(get_static_option('key_feature_section_status'))): ?>
    <div class="header-bottom-area padding-top-130">
        <div class="container">
            <div class="row">
                <?php $__currentLoopData = $all_key_features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($user_select_lang_slug == $data->lang): ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-feature-item-01  wow zoomIn">
                            <div class="icon">
                                <i class="<?php echo e($data->icon); ?>"></i>
                            </div>
                            <div class="content">
                                <h4 class="title"><?php echo e($data->title); ?></h4>
                                <p><?php echo e($data->description); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>

<?php if(!empty(get_static_option('full_width_features_section_status'))): ?>
<?php echo $__env->make('frontend.partials.full-width-feature', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('why_us_section_status'))): ?>
<?php echo $__env->make('frontend.partials.why-us-area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('testimonial_section_status'))): ?>
<?php echo $__env->make('frontend.partials.testimonial', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('intro_video_section_status'))): ?>
<?php echo $__env->make('frontend.partials.intro-video', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('price_plan_section_status'))): ?>
<?php echo $__env->make('frontend.partials.price-plan-area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('team_member_section_status'))): ?>
<?php echo $__env->make('frontend.partials.team-member-area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('contact_section_status'))): ?>
<?php echo $__env->make('frontend.partials.contact-area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
<?php if(!empty(get_static_option('faq_section_status'))): ?>
<?php echo $__env->make('frontend.partials.faq-area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>

<?php /**PATH /var/www/resources/views/frontend/home-pages/home-02.blade.php ENDPATH**/ ?>