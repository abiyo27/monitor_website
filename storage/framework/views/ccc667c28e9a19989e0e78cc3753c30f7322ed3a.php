<div class="contact-area contact-bg" id="<?php echo e(get_static_option('contact_page_slug')); ?>"
<?php echo render_background_image_markup_by_attachment_id(get_static_option('home_page_01_contact_bg_image')); ?>>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="contact-outer-area">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="content-form-wrapper">
                                <div class="section-title center-aligned">
                                    <h2 class="title"><?php echo e(get_static_option('home_page_01_contact_title_'.$user_select_lang_slug)); ?></h2>
                                    <p><?php echo e(get_static_option('home_page_01_contact_description_'.$user_select_lang_slug)); ?></p>
                                </div>
                                <?php echo $__env->make('backend.partials.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php echo $__env->make('backend.partials.error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <form action="<?php echo e(route('frontend.contact.message')); ?>" class="contact-form" id="get_in_touch_form" method="post">
                                    <?php echo csrf_field(); ?>
                                    <div class="error-message"></div>
                                    <div class="success-message"></div>
                                            <?php echo render_form_field_for_frontend(get_static_option('contact_page_contact_form_fields')); ?>

                                                <div class="col-lg-12">
                                                    <div class="btn-wrapper">
                                                        <button type="submit" id="get_in_touch_submit_btn"
                                                        class="submit-btn w180px gd-bg"><?php echo e(__('Submit Now')); ?>

                                                    </button>
                                                </div>   
                                            </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#get_in_touch_submit_btn', function (e) {
                e.preventDefault();
                var el = $(this);
                var myForm = document.getElementById('get_in_touch_form');
                var formData = new FormData(myForm);

                $.ajax({
                    type: "POST",
                    url: "<?php echo e(route('frontend.contact.message')); ?>",
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function(){
                        el.text('<?php echo e(__("Submitting ....")); ?>');
                    },
                    success: function (data) {
                        var errMsgContainer = $('#get_in_touch_form').find('.error-message');
                        var succmsgContainer = $('#get_in_touch_form').find('.success-message');
                        var succmsg = '<?php echo e(__("Email sent successfully!")); ?>';
                        errMsgContainer.html('');
                        el.text('<?php echo e(__("Submit")); ?>');
                        if(data.status == '400'){
                            errMsgContainer.append('<span class="text-danger">'+data.msg+'</span>');
                        }else{
                            succmsgContainer.append('<span class="text-success">'+succmsg+'</span>');
                            $('#get_in_touch_form').get(0).reset()
                           location.reload();
                        }
                        console.log(data);
                    },
                    error: function (data) {
                        var error = data.responseJSON;
                        var errMsgContainer = $('#get_in_touch_form').find('.error-message');
                        errMsgContainer.html('');
                        $.each(error.errors,function (index,value) {
                            errMsgContainer.append('<span class="text-danger">'+value+'</span>');
                        });
                        el.text('<?php echo e(__("Submit")); ?>');
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?><?php /**PATH /var/www/resources/views/frontend/partials/contact-area.blade.php ENDPATH**/ ?>