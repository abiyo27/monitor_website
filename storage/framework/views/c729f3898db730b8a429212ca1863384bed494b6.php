<section class="why-chose-us why-choose-us-bg padding-top-110 padding-bottom-55 margin-top-120" id="<?php echo e(get_static_option('feature_page_slug')); ?>"
<?php echo render_background_image_markup_by_attachment_id(get_static_option('home_page_01_why_choose_us_bg_image')); ?>

>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title center-aligned">
                    <h2 class="title"><?php echo e(get_static_option('home_page_01_why_choose_us_title_'.$user_select_lang_slug)); ?></h2>
                    <p><?php echo e(get_static_option('home_page_01_why_choose_us_description_'.$user_select_lang_slug)); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $__currentLoopData = $why_choose_us; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($user_select_lang_slug == $data->lang): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="why-us-box-01 margin-bottom-30">
                        <div class="icon">
                            <i class="<?php echo e($data->icon); ?>"></i>
                        </div>
                        <div class="content">
                            <h4 class="title"><?php echo e($data->title); ?></h4>
                            <p><?php echo e($data->description); ?></p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section><?php /**PATH /var/www/resources/views/frontend/partials/why-us-area.blade.php ENDPATH**/ ?>