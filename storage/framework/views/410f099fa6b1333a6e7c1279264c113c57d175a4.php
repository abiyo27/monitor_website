<div class="team-member-area padding-bottom-115" id="<?php echo e(get_static_option('team_page_slug')); ?>">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title center-aligned">
                    <h2 class="title"><?php echo e(get_static_option('home_page_01_team_member_title_'.$user_select_lang_slug)); ?></h2>
                    <p><?php echo e(get_static_option('home_page_01_team_member_description_'.$user_select_lang_slug)); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="team-slider">
                    <?php $__currentLoopData = $all_team_members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($user_select_lang_slug == $data->lang): ?>
                        <div class="single-team-member-01">
                            <div class="thumb">
                                <?php echo render_image_markup_by_attachment_id($data->image); ?>

                                <div class="hover">
                                    <ul class="social-icon">
                                        <?php if(!empty($data->icon_one) && !empty($data->icon_one_url)): ?>
                                            <li><a href="<?php echo e($data->icon_one_url); ?>"><i class="<?php echo e($data->icon_one); ?>"></i></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($data->icon_two) && !empty($data->icon_two_url)): ?>
                                            <li><a href="<?php echo e($data->icon_two_url); ?>"><i class="<?php echo e($data->icon_two); ?>"></i></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($data->icon_three) && !empty($data->icon_three_url)): ?>
                                            <li><a href="<?php echo e($data->icon_three_url); ?>"><i class="<?php echo e($data->icon_three); ?>"></i></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="description">
                                <h4 class="name"><?php echo e($data->name); ?></h4>
                                <span class="designation"><?php echo e($data->designation); ?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /var/www/resources/views/frontend/partials/team-member-area.blade.php ENDPATH**/ ?>