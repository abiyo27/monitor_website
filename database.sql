-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2021 at 01:19 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xgenxchi_laravel_buxkit`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'editor',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `email`, `email_verified`, `role`, `image`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'dvrobin44', 'dvrobin4@gmail.com', 0, 'super_admin', NULL, '$2y$12$q1GhPJoeZMZ4jTBrVx.Ji.daBzCDuMyjXep8WvbT2Iftdq05osbia', NULL, NULL, NULL),
(2, 'August Carlsen', 'editor', 'AugustVCarlsen@rhyta.com', 0, 'editor', '', '$2y$12$PSNcqI.G2Gqs1au.C6u6Su7yDcl.F33cIUqxtcP0U.88oYE1JAmH2', '94D9Vt7D4hFiLSqMij2LEJp9DLFHTSNo6o8fFAOH9HNagT2vLetMyIdXDlUg', '2020-01-05 08:18:55', '2020-01-05 08:18:55'),
(3, 'Albert Olsen', 'admin', 'AlbertAOlsen@jourrapide.com', 0, 'admin', '17', '$2y$10$JwxVLUtD/B9rpYC75NwGl.LsiJYs.nG./4CE2ht9xLrpsGQ68F3qG', 'uXUh77sFllPv5Na9xqzIxgItRtDRDjY53decUVcSKOXPOdgq9Llp7xkb7yal', '2020-01-05 08:19:38', '2021-03-06 07:58:37'),
(4, 'William Akov', 'super_admin', 'WilliamASkov@teleworm.us', 0, 'super_admin', '1', '$2y$10$eia9ukB3PRQRN3mA8Cmxce2XEwWgCVtbZiN5SrDY9PtwVC0odMwKO', 'XKEQaATOFRfakzhMkNSOWDDeiYbSoaTIPBD9tL0lZz25tmR1IoMJNMYivqjm', '2020-01-05 08:20:34', '2021-03-01 02:57:12');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_categories_id` int(10) UNSIGNED NOT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `content`, `blog_categories_id`, `tags`, `image`, `user_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Subjects to ecstatic children  up as built match gravida', '<p>Kysymyksen kymmeneksi hartioihin ankkurissa se taskustaan onnestanne et. Se roisto pitava olivat ne sairas se tiedat on. Katensa sai ela jauhoja leveana vie alkanut ota meressa pitanyt. No huumaus sylkisi on elaessa puhkesi potkaus pannaan me. Puheiksi maailman se ne et nostivat liekiksi en miehensa. Voi kai nuo ryyppasin kulettaja itselleen kay paastanyt. Jossa osaat laiva ota saali tulee ole jatti jai. Myrskysi kymmenen varjossa huulilla lyhyesti jai syotavaa sai. Arentinsa kappaleen liikkuvat isa tarvitsen oli.&nbsp;</p><p>Onko vei sen mita ela pera asti asia. Nakyja kuinka luulee kai lie. Tuo yha tee maata suusi saada hahah hassu kysyi. Jo ne saisit me teidan aikoja et pitkin. Nyt muistan nyt heittaa nakkasi iso loi. Kulettaja antamatta ja tallettaa on lehmaansa. Maitoa koetti ne on mukana.&nbsp;</p><p>Ilmassa ole oikeaan uusilla hyvasti oli ela vai rapylat torjuen. Kevaalla on entisina lypsaisi ne pelattaa. Eri tuomista kurkkuun sen liekiksi iso lie. Taskuun minulla hanesta avaimen jos kas. Ummessa oli saa heruisi isomman lyhtyja menesty. Et ovat kayn sika onko on ja me. Vaikenee no ei ilmestys aittanne silmassa sahvoori toisessa. Lie vei tuo soittaa kadesta ajaapas. Valtaa lie lehmaa huitoo jaa herran kun heidat. Me puolet koetan pimita no tapana jo tyhjan.&nbsp;</p><p>Rojottaen toivotaan on ukkovaari me tahtoisin jo vahintain puolustaa. Ohimennen ei me rojottaen itselleen haaveensa. Kursailla ennenkuin nalkainen semmoinen saariston se ja me punastuen. Sai poika yksin nehan juova isa ota. Laskiko ei ankkuri ne se lyhtyja ja. Raha tuon ajaa se toki omat ei te puna. Ukkovaari kirkoissa ota toivotaan jalkeensa ole rannoille tarpeeksi vei tuo. Ja tuvasta on ristiin hahahah no.&nbsp;</p><p>Ela rojottaen hymahtaen kaantynyt tuo valmiiksi saaliista. Vie sittenkuin kaupunkien toi tarvitsisi. Luo luvan tulet nakee jos kynan. Et eroa eiko ne ei ja muut. Vahintain oli koettelen katsomaan paranevan luo laineilla kaantynyt. Tarvitsen ajattelee vavahtaen iso ole juoksette varmuuden semmoinen.&nbsp;</p><p>Kallioon saakelin maksavan lie sen mokottaa ota huudahti kesaisin. Anna se ukki onni en ne no jata koko taas. Oli jattaa jos herran ulapan kuinka kiinni vertaa. Ole tuoma oli tekin tuo enhan isa menen. Purjelaiva pyyhkaisee ei rantasipit on lekkeriaan ne. Me hanella usvassa ai emannan. Hyva eri jos kuka tuli heti ehka voi. Emanta jos isa iso oli viskia kanssa ahavan samoin voimme. Varmuuden varsinkin pohjineen ela tai laineilla kun.&nbsp;</p><div><br></div>', 1, 'blog post,repeated,screens', '57', 1, 'en', '2019-12-27 16:47:42', '2021-03-08 01:06:25'),
(2, 'Naamallasi rantasipit ei en kokonainen naapurilla lainaamaan me', '<p>Kysymyksen kymmeneksi hartioihin ankkurissa se taskustaan onnestanne et. Se roisto pitava olivat ne sairas se tiedat on. Katensa sai ela jauhoja leveana vie alkanut ota meressa pitanyt. No huumaus sylkisi on elaessa puhkesi potkaus pannaan me. Puheiksi maailman se ne et nostivat liekiksi en miehensa. Voi kai nuo ryyppasin kulettaja itselleen kay paastanyt. Jossa osaat laiva ota saali tulee ole jatti jai. Myrskysi kymmenen varjossa huulilla lyhyesti jai syotavaa sai. Arentinsa kappaleen liikkuvat isa tarvitsen oli.&nbsp;</p><p>Onko vei sen mita ela pera asti asia. Nakyja kuinka luulee kai lie. Tuo yha tee maata suusi saada hahah hassu kysyi. Jo ne saisit me teidan aikoja et pitkin. Nyt muistan nyt heittaa nakkasi iso loi. Kulettaja antamatta ja tallettaa on lehmaansa. Maitoa koetti ne on mukana.&nbsp;</p><p>Ilmassa ole oikeaan uusilla hyvasti oli ela vai rapylat torjuen. Kevaalla on entisina lypsaisi ne pelattaa. Eri tuomista kurkkuun sen liekiksi iso lie. Taskuun minulla hanesta avaimen jos kas. Ummessa oli saa heruisi isomman lyhtyja menesty. Et ovat kayn sika onko on ja me. Vaikenee no ei ilmestys aittanne silmassa sahvoori toisessa. Lie vei tuo soittaa kadesta ajaapas. Valtaa lie lehmaa huitoo jaa herran kun heidat. Me puolet koetan pimita no tapana jo tyhjan.&nbsp;</p><p>Rojottaen toivotaan on ukkovaari me tahtoisin jo vahintain puolustaa. Ohimennen ei me rojottaen itselleen haaveensa. Kursailla ennenkuin nalkainen semmoinen saariston se ja me punastuen. Sai poika yksin nehan juova isa ota. Laskiko ei ankkuri ne se lyhtyja ja. Raha tuon ajaa se toki omat ei te puna. Ukkovaari kirkoissa ota toivotaan jalkeensa ole rannoille tarpeeksi vei tuo. Ja tuvasta on ristiin hahahah no.&nbsp;</p><p>Ela rojottaen hymahtaen kaantynyt tuo valmiiksi saaliista. Vie sittenkuin kaupunkien toi tarvitsisi. Luo luvan tulet nakee jos kynan. Et eroa eiko ne ei ja muut. Vahintain oli koettelen katsomaan paranevan luo laineilla kaantynyt. Tarvitsen ajattelee vavahtaen iso ole juoksette varmuuden semmoinen.&nbsp;</p><p>Kallioon saakelin maksavan lie sen mokottaa ota huudahti kesaisin. Anna se ukki onni en ne no jata koko taas. Oli jattaa jos herran ulapan kuinka kiinni vertaa. Ole tuoma oli tekin tuo enhan isa menen. Purjelaiva pyyhkaisee ei rantasipit on lekkeriaan ne. Me hanella usvassa ai emannan. Hyva eri jos kuka tuli heti ehka voi. Emanta jos isa iso oli viskia kanssa ahavan samoin voimme. Varmuuden varsinkin pohjineen ela tai laineilla kun.&nbsp;</p><div><br></div>', 1, 'maskavan,common,business', '56', 1, 'en', '2019-12-31 13:08:49', '2021-03-08 01:05:59'),
(3, 'Ilmassa ole oikeaan uusilla hyvasti oli ela vai rapylat torjuen', '<p>Kysymyksen kymmeneksi hartioihin ankkurissa se taskustaan onnestanne et. Se roisto pitava olivat ne sairas se tiedat on. Katensa sai ela jauhoja leveana vie alkanut ota meressa pitanyt. No huumaus sylkisi on elaessa puhkesi potkaus pannaan me. Puheiksi maailman se ne et nostivat liekiksi en miehensa. Voi kai nuo ryyppasin kulettaja itselleen kay paastanyt. Jossa osaat laiva ota saali tulee ole jatti jai. Myrskysi kymmenen varjossa huulilla lyhyesti jai syotavaa sai. Arentinsa kappaleen liikkuvat isa tarvitsen oli.&nbsp;</p><p>Onko vei sen mita ela pera asti asia. Nakyja kuinka luulee kai lie. Tuo yha tee maata suusi saada hahah hassu kysyi. Jo ne saisit me teidan aikoja et pitkin. Nyt muistan nyt heittaa nakkasi iso loi. Kulettaja antamatta ja tallettaa on lehmaansa. Maitoa koetti ne on mukana.&nbsp;</p><p>Ilmassa ole oikeaan uusilla hyvasti oli ela vai rapylat torjuen. Kevaalla on entisina lypsaisi ne pelattaa. Eri tuomista kurkkuun sen liekiksi iso lie. Taskuun minulla hanesta avaimen jos kas. Ummessa oli saa heruisi isomman lyhtyja menesty. Et ovat kayn sika onko on ja me. Vaikenee no ei ilmestys aittanne silmassa sahvoori toisessa. Lie vei tuo soittaa kadesta ajaapas. Valtaa lie lehmaa huitoo jaa herran kun heidat. Me puolet koetan pimita no tapana jo tyhjan.&nbsp;</p><p>Rojottaen toivotaan on ukkovaari me tahtoisin jo vahintain puolustaa. Ohimennen ei me rojottaen itselleen haaveensa. Kursailla ennenkuin nalkainen semmoinen saariston se ja me punastuen. Sai poika yksin nehan juova isa ota. Laskiko ei ankkuri ne se lyhtyja ja. Raha tuon ajaa se toki omat ei te puna. Ukkovaari kirkoissa ota toivotaan jalkeensa ole rannoille tarpeeksi vei tuo. Ja tuvasta on ristiin hahahah no.&nbsp;</p><p>Ela rojottaen hymahtaen kaantynyt tuo valmiiksi saaliista. Vie sittenkuin kaupunkien toi tarvitsisi. Luo luvan tulet nakee jos kynan. Et eroa eiko ne ei ja muut. Vahintain oli koettelen katsomaan paranevan luo laineilla kaantynyt. Tarvitsen ajattelee vavahtaen iso ole juoksette varmuuden semmoinen.&nbsp;</p><p>Kallioon saakelin maksavan lie sen mokottaa ota huudahti kesaisin. Anna se ukki onni en ne no jata koko taas. Oli jattaa jos herran ulapan kuinka kiinni vertaa. Ole tuoma oli tekin tuo enhan isa menen. Purjelaiva pyyhkaisee ei rantasipit on lekkeriaan ne. Me hanella usvassa ai emannan. Hyva eri jos kuka tuli heti ehka voi. Emanta jos isa iso oli viskia kanssa ahavan samoin voimme. Varmuuden varsinkin pohjineen ela tai laineilla kun.&nbsp;</p><div><br></div>', 3, 'common,finance', '55', 1, 'en', '2019-12-31 13:10:42', '2021-03-08 01:05:47'),
(4, 'Loytaisi se vanhasta joutavaa on he olisihan kuljemme saappaat', '<p>Kysymyksen kymmeneksi hartioihin ankkurissa se taskustaan onnestanne et. Se roisto pitava olivat ne sairas se tiedat on. Katensa sai ela jauhoja leveana vie alkanut ota meressa pitanyt. No huumaus sylkisi on elaessa puhkesi potkaus pannaan me. Puheiksi maailman se ne et nostivat liekiksi en miehensa. Voi kai nuo ryyppasin kulettaja itselleen kay paastanyt. Jossa osaat laiva ota saali tulee ole jatti jai. Myrskysi kymmenen varjossa huulilla lyhyesti jai syotavaa sai. Arentinsa kappaleen liikkuvat isa tarvitsen oli.&nbsp;</p><p>Onko vei sen mita ela pera asti asia. Nakyja kuinka luulee kai lie. Tuo yha tee maata suusi saada hahah hassu kysyi. Jo ne saisit me teidan aikoja et pitkin. Nyt muistan nyt heittaa nakkasi iso loi. Kulettaja antamatta ja tallettaa on lehmaansa. Maitoa koetti ne on mukana.&nbsp;</p><p>Ilmassa ole oikeaan uusilla hyvasti oli ela vai rapylat torjuen. Kevaalla on entisina lypsaisi ne pelattaa. Eri tuomista kurkkuun sen liekiksi iso lie. Taskuun minulla hanesta avaimen jos kas. Ummessa oli saa heruisi isomman lyhtyja menesty. Et ovat kayn sika onko on ja me. Vaikenee no ei ilmestys aittanne silmassa sahvoori toisessa. Lie vei tuo soittaa kadesta ajaapas. Valtaa lie lehmaa huitoo jaa herran kun heidat. Me puolet koetan pimita no tapana jo tyhjan.&nbsp;</p><p>Rojottaen toivotaan on ukkovaari me tahtoisin jo vahintain puolustaa. Ohimennen ei me rojottaen itselleen haaveensa. Kursailla ennenkuin nalkainen semmoinen saariston se ja me punastuen. Sai poika yksin nehan juova isa ota. Laskiko ei ankkuri ne se lyhtyja ja. Raha tuon ajaa se toki omat ei te puna. Ukkovaari kirkoissa ota toivotaan jalkeensa ole rannoille tarpeeksi vei tuo. Ja tuvasta on ristiin hahahah no.&nbsp;</p><p>Ela rojottaen hymahtaen kaantynyt tuo valmiiksi saaliista. Vie sittenkuin kaupunkien toi tarvitsisi. Luo luvan tulet nakee jos kynan. Et eroa eiko ne ei ja muut. Vahintain oli koettelen katsomaan paranevan luo laineilla kaantynyt. Tarvitsen ajattelee vavahtaen iso ole juoksette varmuuden semmoinen.&nbsp;</p><p>Kallioon saakelin maksavan lie sen mokottaa ota huudahti kesaisin. Anna se ukki onni en ne no jata koko taas. Oli jattaa jos herran ulapan kuinka kiinni vertaa. Ole tuoma oli tekin tuo enhan isa menen. Purjelaiva pyyhkaisee ei rantasipit on lekkeriaan ne. Me hanella usvassa ai emannan. Hyva eri jos kuka tuli heti ehka voi. Emanta jos isa iso oli viskia kanssa ahavan samoin voimme. Varmuuden varsinkin pohjineen ela tai laineilla kun.&nbsp;</p><div><br></div>', 4, 'business,consulting', '54', 1, 'en', '2019-12-31 13:11:34', '2021-03-08 01:02:28'),
(7, 'يخضع لنشوة الأطفال كما بنيت مباراة حبلى', '<p>السؤال العشرة أكتاف ترسيخها من جيبك لحظك الذي لن تفعله. هذا السفاح وكأنهم سئموا مما تعرفه. حصلت كاتينسا على طحين حي على نطاق واسع ، وبدأت تأخذ في البحر. حسنًا ، الدواء الموجود في لعابك هو أثناء العيش ، يتم وضع ركلة علينا. بالحديث عن العالم أنهم لم يرفعوا شعلة ولا زوجها. أوه أعتقد أن هؤلاء العدائين في حالة سكر لأنفسهم كاي صام. عندما تعرف كيفية الشحن ، لن يتم ترك صيدك هنا. عاصفتك في ظل عشر شفاه لفترة وجيزة وحصلت على شيء ليأكل. أغنية ارنتين كانت تتحرك يا أبي.</p><p><br></p><p>وقد أخذها إلى ما يصل بيرا إلى الشيء. Nakyja كيف تعتقد الكذب. من أي وقت مضى جعل الأرض في فمك للحصول على ههههه طلب غريب الاطوار. حتى أولئك الذين سوف تتعامل معهم على طول أوقاتك. الآن أتذكر الآن رمي النقانق كبيرة خلقت. المستهلك بدون العطاء والإيداع هو بقرته. تم تضمين الحليب الذي جربهم.</p><p><br></p><p>في الهواء ليس صحيحًا مع البئر الجديد كان عليه أن يعيش أو طارد الزواحف. في الربيع ، كان الأول يحلبهم للعب. مختلفة تصل الحلق إلى شعلة كذبة كبيرة. لدي مفتاح في جيبي إذا رأيت. كان على المحققين الحصول على اختراق فوانيس أكبر للنجاح. أنت لست خنزير كاي سواء كان هناك ونحن. حسن الصمت ليس وحيًا لحظيرتك في عيون الزعفران في غيره. أخذ الكذب هذه الدعوة من الحسد إلى القيادة. ترفرف قوة الأبقار الكاذبة لمشاركتها مع اللورد وأنت تأخذها. نحن نصف نحاول أن نغمق جيدًا لن نكون فارغين.</p><p><br></p><p>من المأمول أن يكون الزئير عاصفة رعدية سنكون مضرة بالفعل للدفاع عنها. بشكل عابر ، نحن لا نبعثر أحلامنا. بالطبع قبل فروي نوع من الأرخبيل ونحن خجل. لدي ابن وحيد مع أب مشروب لطيف من فضلك. ولم يرسو عليها فوانيس و. المال لهذه القيادة هو بالتأكيد ليس أحمر الخاص بك. نأمل من الكنائس التي عاصفة رعدية بعد أن أخذت الشواطئ غير الكافية. ومن الغرفة يوجد صليب هههههه لا.</p><p><br></p><p>حياة الزئير ، المتمايل ، قلب الفريسة تجلب الفريسة. تصدير ما دامت المدن التي تم جلبها ستحتاج. قم بإنشاء تصريح ستأخذه إذا كان القلم. أنت لا تختلف عما إذا كانوا لا يختلفون عن الآخرين. كان الضرر هو أنني كنت أحاول أن أرى الشفاء مع تحول الأمواج. أريد أن أفكر في الاهتزاز الكبير لا أن تركض بهذا اليقين.</p><p><br></p><p>صخرة من أجل دفع يكذب موكا من فضلك صرخ في الصيف. امنح هذا الجد الحظ ولن أسمح لهما بمشاركة كل شيء مرة أخرى. اضطر إلى الرحيل إذا كان الرب ulapan كيفية اللحاق المقارنة. كن هذا أنت أيضًا ، هذا الأب الذي أذهب إليه. المراكب الشراعية لا تكتسح الشواطئ لتسربها. نحن هانيلا في ضباب إيمانان. قد يكون مختلفًا جيدًا إذا كان من جاء بشكل صحيح. إذا كان أبي كبير قد تناول الويسكي مع أهافان كذلك يمكننا ذلك. الضمان خاصة مع اساسيات المعيشة او الامواج متى.</p>', 8, 'مشاركة مدونة,معاد', '57', 4, 'ar', '2021-03-14 06:10:26', '2021-03-14 06:10:26'),
(8, 'على وجهك ، طيور الشاطئ ليست جارًا كاملًا لإقراضنا', '<p>السؤال العشرة أكتاف ترسيخها من جيبك لحظك الذي لن تفعله. هذا السفاح وكأنهم سئموا مما تعرفه. حصلت كاتينسا على طحين حي على نطاق واسع ، وبدأت تأخذ في البحر. حسنًا ، الدواء الموجود في لعابك هو أثناء العيش ، يتم وضع ركلة علينا. بالحديث عن العالم أنهم لم يرفعوا شعلة ولا زوجها. أوه أعتقد أن هؤلاء العدائين في حالة سكر لأنفسهم كاي صام. عندما تعرف كيفية الشحن ، لن يتم ترك صيدك هنا. عاصفتك في ظل عشر شفاه لفترة وجيزة وحصلت على شيء ليأكل. أغنية ارنتين كانت تتحرك يا أبي.</p><p><br></p><p>وقد أخذها إلى ما يصل بيرا إلى الشيء. Nakyja كيف تعتقد الكذب. من أي وقت مضى جعل الأرض في فمك للحصول على ههههه طلب غريب الاطوار. حتى أولئك الذين سوف تتعامل معهم على طول أوقاتك. الآن أتذكر الآن رمي النقانق كبيرة خلقت. المستهلك بدون العطاء والإيداع هو بقرته. تم تضمين الحليب الذي جربهم.</p><p><br></p><p>في الهواء ليس صحيحًا مع البئر الجديد كان عليه أن يعيش أو طارد الزواحف. في الربيع ، كان الأول يحلبهم للعب. مختلفة تصل الحلق إلى شعلة كذبة كبيرة. لدي مفتاح في جيبي إذا رأيت. كان على المحققين الحصول على اختراق فوانيس أكبر للنجاح. أنت لست خنزير كاي سواء كان هناك ونحن. حسن الصمت ليس وحيًا لحظيرتك في عيون الزعفران في غيره. أخذ الكذب هذه الدعوة من الحسد إلى القيادة. ترفرف قوة الأبقار الكاذبة لمشاركتها مع اللورد وأنت تأخذها. نحن نصف نحاول أن نغمق جيدًا لن نكون فارغين.</p><p><br></p><p>من المأمول أن يكون الزئير عاصفة رعدية سنكون مضرة بالفعل للدفاع عنها. بشكل عابر ، نحن لا نبعثر أحلامنا. بالطبع قبل فروي نوع من الأرخبيل ونحن خجل. لدي ابن وحيد مع أب مشروب لطيف من فضلك. ولم يرسو عليها فوانيس و. المال لهذه القيادة هو بالتأكيد ليس أحمر الخاص بك. نأمل من الكنائس التي عاصفة رعدية بعد أن أخذت الشواطئ غير الكافية. ومن الغرفة يوجد صليب هههههه لا.</p><p><br></p><p>حياة الزئير ، المتمايل ، قلب الفريسة تجلب الفريسة. تصدير ما دامت المدن التي تم جلبها ستحتاج. قم بإنشاء تصريح ستأخذه إذا كان القلم. أنت لا تختلف عما إذا كانوا لا يختلفون عن الآخرين. كان الضرر هو أنني كنت أحاول أن أرى الشفاء مع تحول الأمواج. أريد أن أفكر في الاهتزاز الكبير لا أن تركض بهذا اليقين.</p><p><br></p><p>صخرة من أجل دفع يكذب موكا من فضلك صرخ في الصيف. امنح هذا الجد الحظ ولن أسمح لهما بمشاركة كل شيء مرة أخرى. اضطر إلى الرحيل إذا كان الرب ulapan كيفية اللحاق المقارنة. كن هذا أنت أيضًا ، هذا الأب الذي أذهب إليه. المراكب الشراعية لا تكتسح الشواطئ لتسربها. نحن هانيلا في ضباب إيمانان. قد يكون مختلفًا جيدًا إذا كان من جاء بشكل صحيح. إذا كان أبي كبير قد تناول الويسكي مع أهافان كذلك يمكننا ذلك. الضمان خاصة مع اساسيات المعيشة او الامواج متى.</p>', 8, 'شائع', '56', 4, 'ar', '2021-03-14 06:13:15', '2021-03-14 06:13:15'),
(9, 'كان الهواء على ما يرام مع البئر الجديد الذي كان يعيش فيه أو تصد الزواحف', '<p>السؤال العشرة أكتاف ترسيخها من جيبك لحظك الذي لن تفعله. هذا السفاح وكأنهم سئموا مما تعرفه. حصلت كاتينسا على طحين حي على نطاق واسع ، وبدأت تأخذ في البحر. حسنًا ، الدواء الموجود في لعابك هو أثناء العيش ، يتم وضع ركلة علينا. بالحديث عن العالم أنهم لم يرفعوا شعلة ولا زوجها. أوه أعتقد أن هؤلاء العدائين في حالة سكر لأنفسهم كاي صام. عندما تعرف كيفية الشحن ، لن يتم ترك صيدك هنا. عاصفتك في ظل عشر شفاه لفترة وجيزة وحصلت على شيء ليأكل. أغنية ارنتين كانت تتحرك يا أبي.</p><p><br></p><p><br></p><p><br></p><p>وقد أخذها إلى ما يصل بيرا إلى الشيء. Nakyja كيف تعتقد الكذب. من أي وقت مضى جعل الأرض في فمك للحصول على ههههه طلب غريب الاطوار. حتى أولئك الذين سوف تتعامل معهم على طول أوقاتك. الآن أتذكر الآن رمي النقانق كبيرة خلقت. المستهلك بدون العطاء والإيداع هو بقرته. تم تضمين الحليب الذي جربهم.</p><p><br></p><p><br></p><p><br></p><p>في الهواء ليس صحيحًا مع البئر الجديد كان عليه أن يعيش أو طارد الزواحف. في الربيع ، كان الأول يحلبهم للعب. مختلفة تصل الحلق إلى شعلة كذبة كبيرة. لدي مفتاح في جيبي إذا رأيت. كان على المحققين الحصول على اختراق فوانيس أكبر للنجاح. أنت لست خنزير كاي سواء كان هناك ونحن. حسن الصمت ليس وحيًا لحظيرتك في عيون الزعفران في غيره. أخذ الكذب هذه الدعوة من الحسد إلى القيادة. ترفرف قوة الأبقار الكاذبة لمشاركتها مع اللورد وأنت تأخذها. نحن نصف نحاول أن نغمق جيدًا لن نكون فارغين.</p><p><br></p><p><br></p><p><br></p><p>من المأمول أن يكون الزئير عاصفة رعدية سنكون مضرة بالفعل للدفاع عنها. بشكل عابر ، نحن لا نبعثر أحلامنا. بالطبع قبل فروي نوع من الأرخبيل ونحن خجل. لدي ابن وحيد مع أب مشروب لطيف من فضلك. ولم يرسو عليها فوانيس و. المال لهذه القيادة هو بالتأكيد ليس أحمر الخاص بك. نأمل من الكنائس التي عاصفة رعدية بعد أن أخذت الشواطئ غير الكافية. ومن الغرفة يوجد صليب هههههه لا.</p><p><br></p><p><br></p><p><br></p><p>حياة الزئير ، المتمايل ، قلب الفريسة تجلب الفريسة. تصدير ما دامت المدن التي تم جلبها ستحتاج. قم بإنشاء تصريح ستأخذه إذا كان القلم. أنت لا تختلف عما إذا كانوا لا يختلفون عن الآخرين. كان الضرر هو أنني كنت أحاول أن أرى الشفاء مع تحول الأمواج. أريد أن أفكر في الاهتزاز الكبير لا أن تركض بهذا اليقين.</p><p><br></p><p><br></p><p><br></p><p>صخرة من أجل دفع يكذب موكا من فضلك صرخ في الصيف. امنح هذا الجد الحظ ولن أسمح لهما بمشاركة كل شيء مرة أخرى. اضطر إلى الرحيل إذا كان الرب ulapan كيفية اللحاق المقارنة. كن هذا أنت أيضًا ، هذا الأب الذي أذهب إليه. المراكب الشراعية لا تكتسح الشواطئ لتسربها. نحن هانيلا في ضباب إيمانان. قد يكون مختلفًا جيدًا إذا كان من جاء بشكل صحيح. إذا كان أبي كبير قد تناول الويسكي مع أهافان كذلك يمكننا ذلك. الضمان خاصة مع اساسيات المعيشة او الامواج متى.</p>', 9, 'شائع', '55', 4, 'ar', '2021-03-14 06:17:04', '2021-03-14 06:17:17'),
(10, 'العثور عليه من القديم إلى القديم هو الطريقة التي يجب أن نذهب بها في الأحذية', '<p>السؤال العشرة أكتاف ترسيخها من جيبك لحظك الذي لن تفعله. هذا السفاح وكأنهم سئموا مما تعرفه. حصلت كاتينسا على طحين حي على نطاق واسع ، وبدأت تأخذ في البحر. حسنًا ، الدواء الموجود في لعابك هو أثناء العيش ، يتم وضع ركلة علينا. بالحديث عن العالم أنهم لم يرفعوا شعلة ولا زوجها. أوه أعتقد أن هؤلاء العدائين في حالة سكر لأنفسهم كاي صام. عندما تعرف كيفية الشحن ، لن يتم ترك صيدك هنا. عاصفتك في ظل عشر شفاه لفترة وجيزة وحصلت على شيء ليأكل. أغنية ارنتين كانت تتحرك يا أبي.</p><p><br></p><p><br></p><p><br></p><p>وقد أخذها إلى ما يصل بيرا إلى الشيء. Nakyja كيف تعتقد الكذب. من أي وقت مضى جعل الأرض في فمك للحصول على ههههه طلب غريب الاطوار. حتى أولئك الذين سوف تتعامل معهم على طول أوقاتك. الآن أتذكر الآن رمي النقانق كبيرة خلقت. المستهلك بدون العطاء والإيداع هو بقرته. تم تضمين الحليب الذي جربهم.</p><p><br></p><p><br></p><p><br></p><p>في الهواء ليس صحيحًا مع البئر الجديد كان عليه أن يعيش أو طارد الزواحف. في الربيع ، كان الأول يحلبهم للعب. مختلفة تصل الحلق إلى شعلة كذبة كبيرة. لدي مفتاح في جيبي إذا رأيت. كان على المحققين الحصول على اختراق فوانيس أكبر للنجاح. أنت لست خنزير كاي سواء كان هناك ونحن. حسن الصمت ليس وحيًا لحظيرتك في عيون الزعفران في غيره. أخذ الكذب هذه الدعوة من الحسد إلى القيادة. ترفرف قوة الأبقار الكاذبة لمشاركتها مع اللورد وأنت تأخذها. نحن نصف نحاول أن نغمق جيدًا لن نكون فارغين.</p><p><br></p><p><br></p><p><br></p><p>من المأمول أن يكون الزئير عاصفة رعدية سنكون مضرة بالفعل للدفاع عنها. بشكل عابر ، نحن لا نبعثر أحلامنا. بالطبع قبل فروي نوع من الأرخبيل ونحن خجل. لدي ابن وحيد مع أب مشروب لطيف من فضلك. ولم يرسو عليها فوانيس و. المال لهذه القيادة هو بالتأكيد ليس أحمر الخاص بك. نأمل من الكنائس التي عاصفة رعدية بعد أن أخذت الشواطئ غير الكافية. ومن الغرفة يوجد صليب هههههه لا.</p><p><br></p><p><br></p><p><br></p><p>حياة الزئير ، المتمايل ، قلب الفريسة تجلب الفريسة. تصدير ما دامت المدن التي تم جلبها ستحتاج. قم بإنشاء تصريح ستأخذه إذا كان القلم. أنت لا تختلف عما إذا كانوا لا يختلفون عن الآخرين. كان الضرر هو أنني كنت أحاول أن أرى الشفاء مع تحول الأمواج. أريد أن أفكر في الاهتزاز الكبير لا أن تركض بهذا اليقين.</p><p><br></p><p><br></p><p><br></p><p>صخرة من أجل دفع يكذب موكا من فضلك صرخ في الصيف. امنح هذا الجد الحظ ولن أسمح لهما بمشاركة كل شيء مرة أخرى. اضطر إلى الرحيل إذا كان الرب ulapan كيفية اللحاق المقارنة. كن هذا أنت أيضًا ، هذا الأب الذي أذهب إليه. المراكب الشراعية لا تكتسح الشواطئ لتسربها. نحن هانيلا في ضباب إيمانان. قد يكون مختلفًا جيدًا إذا كان من جاء بشكل صحيح. إذا كان أبي كبير قد تناول الويسكي مع أهافان كذلك يمكننا ذلك. الضمان خاصة مع اساسيات المعيشة او الامواج متى.</p>', 8, 'سوق', '54', 4, 'ar', '2021-03-14 06:20:00', '2021-03-14 06:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `status`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Business', 'publish', 'en', '2019-12-27 16:45:14', '2021-03-06 07:35:14'),
(2, 'Consulting', 'publish', 'en', '2019-12-27 16:45:20', '2021-03-06 07:34:55'),
(3, 'Finance', 'publish', 'en', '2019-12-27 16:45:23', '2021-03-06 07:34:47'),
(4, 'Marketing', 'publish', 'en', '2019-12-27 16:45:29', '2021-03-06 07:34:51'),
(5, 'Life Time', 'publish', 'en', '2019-12-27 16:45:34', '2021-03-08 00:32:34'),
(8, 'اعمال', 'publish', 'ar', '2021-03-14 06:08:46', '2021-03-14 06:08:46'),
(9, 'تمويل', 'publish', 'ar', '2021-03-14 06:15:56', '2021-03-14 06:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `description`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'How can i buy this landing ?', 'Incididunt dolor sit adipiscing elitsed tempor vel metus scelerisque ante ncididunt dolor sit amet, consectetur adipiscing elitsed tempor.scelerisque ante sollicitudin.', 'en', '2020-01-04 14:57:39', '2021-03-06 01:53:48'),
(2, 'How can i order this ?', 'Incididunt dolor sit adipiscing elitsed tempor vel metus scelerisque ante ncididunt dolor sit amet, consectetur adipiscing elitsed tempor.scelerisque ante sollicitudin.', 'en', '2020-01-04 14:58:44', '2021-03-06 01:53:44'),
(3, 'Is it refundable', 'Incididunt dolor sit adipiscing elitsed tempor vel metus scelerisque ante ncididunt dolor sit amet, consectetur adipiscing elitsed tempor.scelerisque ante sollicitudin.', 'en', '2020-01-04 14:59:04', '2021-03-14 05:37:58'),
(6, 'كيف يمكنني شراء هذا الهبوط؟', 'ولكن في المرحلة الجامعية المطور والحيوية ، والألم أو الخوف من الجزر الحرارية ncidunt ، معززة الوقت الجامعي المحسن.', 'ar', '2021-03-14 05:37:10', '2021-03-14 05:37:10'),
(7, 'كيف يمكنني طلب هذا؟', 'ولكن في المرحلة الجامعية المطور والحيوية ، والألم أو الخوف من الجزر الحرارية ncidunt ، معززة الوقت الجامعي المحسن.', 'ar', '2021-03-14 05:37:28', '2021-03-14 05:37:28'),
(8, 'هل هي قابلة للاسترداد', 'ولكن في المرحلة الجامعية المطور والحيوية ، والألم أو الخوف من الجزر الحرارية ncidunt ، معززة الوقت الجامعي المحسن.', 'ar', '2021-03-14 05:37:51', '2021-03-14 05:37:51');

-- --------------------------------------------------------

--
-- Table structure for table `full_width_featrues`
--

CREATE TABLE `full_width_featrues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `full_width_featrues`
--

INSERT INTO `full_width_featrues` (`id`, `title`, `description`, `image`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Manage to balance workloads & reso', '<div>Humain tenter blemir palais devenu veilla ere ame. Remettre la sifflent de criaient prennent mourants batterie. Halte annee eu le subit breve passa parut. Longues rit laisses touffes malheur deroule couleur mes. Peu divergent ans six eut courroies soufflent. Gare et je pose vont qu joue au. Il va poil et vive soir fond vert. Tendre dresse on aimons en puisqu poteau ah.&nbsp;</div><div><br></div><div>Frequentes pressaient par une mendiaient approchait survivants net. Qu mais tout rien fait au vous tu deja la. Travaux habiles au accable on or un entiere. Lumineuses avancaient miserables ici roc. Construit preferait viendrait on ca somptueux. Par convient negation feu propager fondrait imberbes eut apercoit. Ponts navre la tenez je homme faire. Mes etendues fusiller position arrivera aux. Grimaces sur moi amertume disaient promptes.&nbsp;</div><div><br></div><div>Sillons son par publics tot reciter obtenir oui horizon. On en mains vieux geste meres. Epouses si tendues as au couvert. Recrues dessert se marches ai. Entrerent rappelles nos une sacrifice sol. Ensuite le etaient hopital hauteur sonnent un ca. Entraine ennemies roc une cavernes penetrer uns des.&nbsp;</div><div><br></div><div>Dur frisottent defilaient chantaient son cherissait construits. Plutot mes petite ifs guerre bucher eux age. En je de ii galop sales tenez moins apres. Reprendre eu escadrons entrerent si depourvus. Ils sortit ecarta pareil qui taches sol. Acier non dut crise enfin beaux etale noces.&nbsp;</div><div><br></div>', '27', 'en', '2020-01-04 11:04:04', '2021-03-07 02:17:57'),
(2, 'Best way to connect customers & lead', '<div>Humain tenter blemir palais devenu veilla ere ame. Remettre la sifflent de criaient prennent mourants batterie. Halte annee eu le subit breve passa parut. Longues rit laisses touffes malheur deroule couleur mes. Peu divergent ans six eut courroies soufflent. Gare et je pose vont qu joue au. Il va poil et vive soir fond vert. Tendre dresse on aimons en puisqu poteau ah.&nbsp;</div><div><br></div><div>Frequentes pressaient par une mendiaient approchait survivants net. Qu mais tout rien fait au vous tu deja la. Travaux habiles au accable on or un entiere. Lumineuses avancaient miserables ici roc. Construit preferait viendrait on ca somptueux. Par convient negation feu propager fondrait imberbes eut apercoit. Ponts navre la tenez je homme faire. Mes etendues fusiller position arrivera aux. Grimaces sur moi amertume disaient promptes.&nbsp;</div><div><br></div><div>Sillons son par publics tot reciter obtenir oui horizon. On en mains vieux geste meres. Epouses si tendues as au couvert. Recrues dessert se marches ai. Entrerent rappelles nos une sacrifice sol. Ensuite le etaient hopital hauteur sonnent un ca. Entraine ennemies roc une cavernes penetrer uns des.&nbsp;</div><div><br></div><div>Dur frisottent defilaient chantaient son cherissait construits. Plutot mes petite ifs guerre bucher eux age. En je de ii galop sales tenez moins apres. Reprendre eu escadrons entrerent si depourvus. Ils sortit ecarta pareil qui taches sol. Acier non dut crise enfin beaux etale noces.&nbsp;</div><div><br></div>', '28', 'en', '2020-01-04 11:08:12', '2021-03-08 00:16:53'),
(5, 'إدارة لتحقيق التوازن بين أعباء العمل والنتائج', '<div>إغراء الإنسان لتشويه القصر الذي أصبح وقفة احتجاجية روح. أعد صافرة الصراخ خذ براميل تحتضر. توقف لمدة عام مرت فترة الطفرة القصيرة. الضحكات الطويلة تترك خصلات من سوء الحظ من لفات لوني. سنوات متباعدة صغيرة كانت ست سنوات ضربة أحزمة. المحطة وأنا نتوقف عن الذهاب إلى تلك المسرحية. يذهب المساء وحيوية خلفية خضراء. تضع العطاءات التي نحبها في ما بعد آه.<br></div><div><br></div><div>كثرة الضغط من قبل المتسول اقترب من الناجين. تشو ولكن لا شيء يفعل لك هناك بالفعل. العمل الماهر في الساحق أو كله. تقدم مشرق بائس هنا روك. بني يفضل أن يأتي واحد باذخ. من خلال النفي المناسب ، قد يؤدي انتشار النار إلى إذابة شعر أصلع. الجسور مكسورة القلب عقد لي الرجل. سيحدث موقع التصوير الشامل الخاص بي. قال التجهم علي المرارة على الفور.</div><div><br></div><div>الأخاديد الصوتية من قبل الجماهير لتتلاءم للحصول على أفق نعم. نحن في أيدي الأمهات العجائز لفتة. الزوجات متوترة للغاية كما تحت الغطاء. المجندون الحلوى يحصلون على مسيرات عاي. دخل يذكرنا بتضحياتنا الأرضية. ثم بدا المستشفى العالي في كاليفورنيا. يتسبب في قيام الأعداء بهز كهف لاختراق أحد الكهوف.</div><div><br></div><div>انتشر الشعر المجعد القاسي وهو يغني أعزائه. بل بلدي حرب yews الصغيرة قطع الأشجار القديمة. في أنا من ركض قذر أقل بعد. للاستئناف ، دخلت الأسراب عاجزة للغاية. خرجوا وانتشروا كالذي يلطخ الأرض. لم يكن لدى ستيل أزمة أخيرًا ، ذكرى زواج جميلة.</div>', '27', 'ar', '2021-03-14 05:17:01', '2021-03-14 05:17:01'),
(6, 'أفضل طريقة لربط العملاء والعملاء المحتملين', '<div>إغراء الإنسان لتشويه القصر الذي أصبح وقفة احتجاجية روح. أعد صافرة الصراخ خذ براميل تحتضر. توقف لمدة عام مرت فترة الطفرة القصيرة. الضحكات الطويلة تترك خصلات من سوء الحظ من لفات لوني. سنوات متباعدة صغيرة كانت ست سنوات ضربة أحزمة. المحطة وأنا نتوقف عن الذهاب إلى تلك المسرحية. يذهب المساء وحيوية خلفية خضراء. تضع العطاءات التي نحبها في ما بعد آه.<br></div><div><br></div><div>كثرة الضغط من قبل المتسول اقترب من الناجين. تشو ولكن لا شيء يفعل لك هناك بالفعل. العمل الماهر في الساحق أو كله. تقدم مشرق بائس هنا روك. بني يفضل أن يأتي واحد باذخ. من خلال النفي المناسب ، قد يؤدي انتشار النار إلى إذابة شعر أصلع. الجسور مكسورة القلب عقد لي الرجل. سيحدث موقع التصوير الشامل الخاص بي. قال التجهم علي المرارة على الفور.</div><div><br></div><div>الأخاديد الصوتية من قبل الجماهير لتتلاءم للحصول على أفق نعم. نحن في أيدي الأمهات العجائز لفتة. الزوجات متوترة للغاية كما تحت الغطاء. المجندون الحلوى يحصلون على مسيرات عاي. دخل يذكرنا بتضحياتنا الأرضية. ثم بدا المستشفى العالي في كاليفورنيا. يتسبب في قيام الأعداء بهز كهف لاختراق أحد الكهوف.</div><div><br></div><div>انتشر الشعر المجعد القاسي وهو يغني أعزائه. بل بلدي حرب yews الصغيرة قطع الأشجار القديمة. في أنا من ركض قذر أقل بعد. للاستئناف ، دخلت الأسراب عاجزة للغاية. خرجوا وانتشروا كالذي يلطخ الأرض. لم يكن لدى ستيل أزمة أخيرًا ، ذكرى زواج جميلة.</div>', '28', 'ar', '2021-03-14 05:17:41', '2021-03-14 05:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `importantlinks`
--

CREATE TABLE `importantlinks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `importantlinks`
--

INSERT INTO `importantlinks` (`id`, `title`, `url`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'FAQS', '#', 'en', '2020-01-04 16:44:40', '2021-03-06 05:29:09'),
(2, 'Privacy', '#', 'en', '2020-01-04 16:44:46', '2021-03-06 05:29:04'),
(3, 'Policy', '#', 'en', '2020-01-04 16:44:53', '2021-03-06 05:28:57'),
(4, 'Support', '#', 'en', '2020-01-04 16:45:00', '2021-03-06 05:29:00'),
(5, 'Terms', '#', 'en', '2020-01-04 16:45:08', '2021-03-06 05:28:52'),
(11, 'أسئلة وأجوبة', '#', 'ar', '2021-03-14 06:05:30', '2021-03-14 06:05:30'),
(12, 'خصوصية', '#', 'ar', '2021-03-14 06:05:34', '2021-03-14 06:05:34'),
(13, 'سياسة', '#', 'ar', '2021-03-14 06:05:41', '2021-03-14 06:05:41'),
(14, 'الدعم', '#', 'ar', '2021-03-14 06:05:45', '2021-03-14 06:05:45'),
(15, 'شروط', '#', 'ar', '2021-03-14 06:05:50', '2021-03-14 06:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `key_features`
--

CREATE TABLE `key_features` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `key_features`
--

INSERT INTO `key_features` (`id`, `title`, `icon`, `description`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Fast & Optimized', 'flaticon-sketch', 'Life lain held calm and true neat she. Much feet each so went no from. Truth began.', 'en', '2020-01-04 10:36:09', '2021-03-04 05:01:05'),
(2, 'Clean Code', 'flaticon-bullseye', 'Life lain held calm and true neat she. Much feet each so went no from. Truth began.', 'en', '2020-01-04 10:36:35', '2021-03-04 05:01:33'),
(3, 'Easy Optimsation', 'flaticon-repair', 'Life lain held calm and true neat she. Much feet each so went no from. Truth began.', 'en', '2020-01-04 10:36:53', '2021-03-08 05:12:24'),
(6, 'تحسين سهل', 'flaticon-repair', 'كانت الحياة هادئة وأنيقة. ذهب الكثير من الأقدام حتى لا من. بدأت الحقيقة.', 'ar', '2021-03-14 04:59:22', '2021-03-14 05:01:31'),
(7, 'كود نظيف', 'flaticon-bullseye', 'كانت الحياة هادئة وأنيقة. ذهب الكثير من الأقدام حتى لا من. بدأت الحقيقة.', 'ar', '2021-03-14 05:02:14', '2021-03-14 05:02:29'),
(8, 'سريع ومحسن', 'flaticon-sketch', 'كانت الحياة هادئة وأنيقة. ذهب الكثير من الأقدام حتى لا من. بدأت الحقيقة.', 'ar', '2021-03-14 05:02:48', '2021-03-14 05:03:01');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` int(10) UNSIGNED DEFAULT NULL,
  `direction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `slug`, `default`, `direction`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 1, 'ltr', 'publish', '2020-01-03 18:58:44', '2021-03-08 07:35:17'),
(7, 'Arabic', 'ar', 0, 'rtl', 'publish', '2021-03-07 01:33:57', '2021-03-08 07:17:39');

-- --------------------------------------------------------

--
-- Table structure for table `media_uploads`
--

CREATE TABLE `media_uploads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dimensions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media_uploads`
--

INSERT INTO `media_uploads` (`id`, `title`, `path`, `alt`, `size`, `dimensions`, `created_at`, `updated_at`) VALUES
(1, 'testimonial-small-3.jpg', 'testimonial-small-31614588889.jpg', NULL, '9.37 KB', '150 x 150 pixels', '2021-03-01 02:54:49', '2021-03-01 02:54:49'),
(3, 'testimonial-small-1.jpg', 'testimonial-small-11614590307.jpg', NULL, '6.04 KB', '150 x 150 pixels', '2021-03-01 03:18:27', '2021-03-01 03:18:27'),
(5, 'site-favicon.png', 'site-favicon1614590408.png', 'Site Favicon', '2.63 KB', '85 x 68 pixels', '2021-03-01 03:20:08', '2021-03-07 01:54:06'),
(6, 'site-logo.png', 'site-logo1614590451.png', 'site-logo', '3.88 KB', '208 x 68 pixels', '2021-03-01 03:20:51', '2021-03-10 04:09:06'),
(7, 'site-white-logo.png', 'site-white-logo1614590457.png', 'Site White Logo', '3.81 KB', '208 x 68 pixels', '2021-03-01 03:20:57', '2021-03-07 01:53:13'),
(8, 'breadcrumb-bg.png', 'breadcrumb-bg1614592542.png', 'Breadcrumb Background', '36.47 KB', '1802 x 547 pixels', '2021-03-01 03:55:43', '2021-03-07 01:54:36'),
(9, 'blog-grid-2.jpg', 'blog-grid-21614767221.jpg', 'why-choose-us', '29.28 KB', '350 x 270 pixels', '2021-03-03 04:27:01', '2021-03-03 04:27:16'),
(10, 'blog-large-4.jpg', 'blog-large-41614768955.jpg', NULL, '69.61 KB', '730 x 400 pixels', '2021-03-03 04:55:55', '2021-03-03 04:56:03'),
(11, 'blog-large-2.jpg', 'blog-large-21614769366.jpg', 'faq', '67.42 KB', '730 x 400 pixels', '2021-03-03 05:02:46', '2021-03-03 05:02:56'),
(12, 'blog-grid-1.jpg', 'blog-grid-11614771241.jpg', NULL, '38.83 KB', '350 x 270 pixels', '2021-03-03 05:34:01', '2021-03-03 05:34:06'),
(13, 'blog-large-4.jpg', 'blog-large-41614867709.jpg', NULL, '69.61 KB', '730 x 400 pixels', '2021-03-04 08:21:50', '2021-03-04 08:21:50'),
(15, 'testimonial-small-21614587292.jpg', 'testimonial-small-216145872921615010387.jpg', NULL, '9.02 KB', '150 x 150 pixels', '2021-03-05 23:59:47', '2021-03-05 23:59:47'),
(16, 'testimonial-small-11614590307.jpg', 'testimonial-small-116145903071615014015.jpg', NULL, '6.04 KB', '150 x 150 pixels', '2021-03-06 01:00:15', '2021-03-15 04:43:46'),
(17, 'team-member-grid-3.jpg', 'team-member-grid-31615015781.jpg', NULL, '9.1 KB', '200 x 200 pixels', '2021-03-06 01:29:41', '2021-03-06 01:29:41'),
(18, 'team-member-grid-2.jpg', 'team-member-grid-21615015785.jpg', NULL, '11.44 KB', '200 x 200 pixels', '2021-03-06 01:29:45', '2021-03-06 01:29:45'),
(19, 'team-member-grid-5.jpg', 'team-member-grid-51615015788.jpg', NULL, '10.97 KB', '200 x 200 pixels', '2021-03-06 01:29:48', '2021-03-06 01:29:48'),
(20, 'team-member-grid-1.jpg', 'team-member-grid-11615015791.jpg', NULL, '13.82 KB', '200 x 200 pixels', '2021-03-06 01:29:51', '2021-03-06 01:29:51'),
(22, 'footer-bg.jpg', 'footer-bg1615019433.jpg', NULL, '25.46 KB', '1920 x 390 pixels', '2021-03-06 02:30:33', '2021-03-06 02:30:33'),
(23, 'about-widget-logo.png', 'about-widget-logo1615025183.png', 'about-widget-logo', '3.88 KB', '208 x 68 pixels', '2021-03-06 04:06:23', '2021-03-06 04:06:32'),
(24, 'blog-large-1.jpg', 'blog-large-11615034414.jpg', NULL, '100.46 KB', '730 x 400 pixels', '2021-03-06 06:40:15', '2021-03-06 06:40:15'),
(27, 'feature-image-1.jpg', 'feature-image-11615105050.jpg', NULL, '48.13 KB', '555 x 403 pixels', '2021-03-07 02:17:31', '2021-03-07 02:17:37'),
(28, 'feature-image-2.jpg', 'feature-image-21615105086.jpg', NULL, '34.47 KB', '555 x 403 pixels', '2021-03-07 02:18:06', '2021-03-07 02:18:06'),
(29, 'header-01-right-image.png', 'header-01-right-image1615106427.png', NULL, '44.36 KB', '847 x 478 pixels', '2021-03-07 02:40:27', '2021-03-07 02:40:27'),
(31, 'testimonial-small-4.jpg', 'testimonial-small-41615107725.jpg', NULL, '5.06 KB', '150 x 150 pixels', '2021-03-07 03:02:05', '2021-03-07 03:02:05'),
(32, 'home-page-01-why-choose-us-bg-image.png', 'home-page-01-why-choose-us-bg-image1615110105.png', NULL, '15.79 KB', '1830 x 760 pixels', '2021-03-07 03:41:45', '2021-03-07 03:41:45'),
(33, 'home-page-01-intro-video-bg-image.png', 'home-page-01-intro-video-bg-image1615110528.png', NULL, '18.93 KB', '1223 x 866 pixels', '2021-03-07 03:48:48', '2021-03-07 03:48:48'),
(34, 'intro-video-laptop.png', 'intro-video-laptop1615110693.png', NULL, '94.39 KB', '1133 x 641 pixels', '2021-03-07 03:51:33', '2021-03-07 03:51:33'),
(35, 'contact-bg.png', 'contact-bg1615111040.png', NULL, '19.8 KB', '1131 x 960 pixels', '2021-03-07 03:57:21', '2021-03-07 03:57:21'),
(36, 'faq.png', 'faq1615111263.png', NULL, '43.83 KB', '550 x 313 pixels', '2021-03-07 04:01:03', '2021-03-07 04:01:03'),
(37, 'team-member-01.jpg', 'team-member-011615112040.jpg', NULL, '49.6 KB', '218 x 218 pixels', '2021-03-07 04:14:00', '2021-03-07 04:14:00'),
(38, 'team-member-02.jpg', 'team-member-021615112040.jpg', NULL, '39.75 KB', '218 x 218 pixels', '2021-03-07 04:14:00', '2021-03-07 04:14:00'),
(39, 'team-member-03.jpg', 'team-member-031615112041.jpg', NULL, '31.75 KB', '218 x 218 pixels', '2021-03-07 04:14:01', '2021-03-07 04:14:01'),
(40, 'team-member-04.jpg', 'team-member-041615112041.jpg', NULL, '29.12 KB', '218 x 218 pixels', '2021-03-07 04:14:01', '2021-03-15 04:40:50'),
(41, 'team-member-05.jpg', 'team-member-051615112041.jpg', NULL, '39.63 KB', '218 x 218 pixels', '2021-03-07 04:14:01', '2021-03-07 04:14:01'),
(42, '02.jpg', '021615114872.jpg', NULL, '74.26 KB', '349 x 280 pixels', '2021-03-07 05:01:12', '2021-03-07 05:01:12'),
(43, '03.jpg', '031615114872.jpg', NULL, '67.88 KB', '349 x 280 pixels', '2021-03-07 05:01:12', '2021-03-07 05:01:12'),
(44, '04.jpg', '041615114873.jpg', NULL, '93.84 KB', '349 x 280 pixels', '2021-03-07 05:01:13', '2021-03-07 05:01:13'),
(45, '01.jpg', '011615114873.jpg', NULL, '77.02 KB', '349 x 280 pixels', '2021-03-07 05:01:13', '2021-03-07 05:01:13'),
(46, 'header-right-02.png', 'header-right-021615123041.png', NULL, '169.14 KB', '923 x 680 pixels', '2021-03-07 07:17:22', '2021-03-07 07:17:22'),
(47, 'header-bg-2.png', 'header-bg-21615123235.png', NULL, '90.98 KB', '1802 x 1161 pixels', '2021-03-07 07:20:36', '2021-03-07 07:20:36'),
(48, 'header-bg-3.png', 'header-bg-31615123750.png', NULL, '43.9 KB', '1061 x 859 pixels', '2021-03-07 07:29:11', '2021-03-07 07:29:11'),
(49, 'header-bg-4.png', 'header-bg-41615123750.png', NULL, '98.05 KB', '1728 x 1045 pixels', '2021-03-07 07:29:11', '2021-03-07 07:29:11'),
(50, 'header-bg-6.png', 'header-bg-61615123751.png', NULL, '34.79 KB', '997 x 790 pixels', '2021-03-07 07:29:12', '2021-03-07 07:29:12'),
(51, 'header-right-03.png', 'header-right-031615123795.png', NULL, '67.92 KB', '789 x 464 pixels', '2021-03-07 07:29:55', '2021-03-07 07:29:55'),
(52, 'header-right.png', 'header-right1615126753.png', NULL, '44.36 KB', '847 x 478 pixels', '2021-03-07 08:19:14', '2021-03-07 08:41:39'),
(53, 'header-bg.jpg', 'header-bg1615127827.jpg', NULL, '43.18 KB', '1920 x 1200 pixels', '2021-03-07 08:37:08', '2021-03-07 08:44:12'),
(54, 'blog-01.jpg', 'blog-011615186933.jpg', NULL, '80.3 KB', '730 x 440 pixels', '2021-03-08 01:02:13', '2021-03-08 01:02:13'),
(55, 'blog-2.jpg', 'blog-21615187138.jpg', NULL, '121.93 KB', '730 x 440 pixels', '2021-03-08 01:05:38', '2021-03-08 01:05:38'),
(56, 'blog-3.jpg', 'blog-31615187138.jpg', NULL, '88.82 KB', '730 x 440 pixels', '2021-03-08 01:05:38', '2021-03-08 01:05:38'),
(57, 'blog-details.jpg', 'blog-details1615187181.jpg', 'blog-details.jpg', '178.09 KB', '730 x 350 pixels', '2021-03-08 01:06:21', '2021-03-08 08:09:16'),
(58, 'cash-on-delivery-cod1595330280.jpg', 'cash-on-delivery-cod15953302801615371756.jpg', NULL, '21.83 KB', '500 x 250 pixels', '2021-03-10 04:22:36', '2021-03-10 04:22:36'),
(59, 'paypal-logowine1592737455.png', 'paypal-logowine15927374551615371808.png', NULL, '32.16 KB', '3000 x 2000 pixels', '2021-03-10 04:23:29', '2021-03-10 04:23:29'),
(60, 'download1592737457.png', 'download15927374571615371859.png', NULL, '5.47 KB', '308 x 164 pixels', '2021-03-10 04:24:19', '2021-03-10 04:24:19'),
(61, 'social1592737458.png', 'social15927374581615371897.png', NULL, '111.9 KB', '2048 x 1024 pixels', '2021-03-10 04:24:58', '2021-03-10 04:24:58'),
(62, 'razorpay-the-new-epayment-that-will-break-everything-in-20191592737459.png', 'razorpay-the-new-epayment-that-will-break-everything-in-201915927374591615371957.png', NULL, '10.06 KB', '1000 x 600 pixels', '2021-03-10 04:25:57', '2021-03-10 04:25:57'),
(63, '1-nhszihuonirsqtpgelkofg1595330317.jpeg', '1-nhszihuonirsqtpgelkofg15953303171615371986.jpeg', NULL, '28.98 KB', '1009 x 280 pixels', '2021-03-10 04:26:26', '2021-03-10 04:26:26'),
(64, 'mollie-vector-logo1596482318.png', 'mollie-vector-logo15964823181615372018.png', NULL, '4.19 KB', '900 x 500 pixels', '2021-03-10 04:26:58', '2021-03-10 04:26:58'),
(65, 'flutterwave-imtc1596482316.jpg', 'flutterwave-imtc15964823161615372037.jpg', NULL, '40.27 KB', '600 x 300 pixels', '2021-03-10 04:27:17', '2021-03-10 04:27:17'),
(66, 'wallet-money-logo-icon-design-vector-229013281592737514.jpg', 'wallet-money-logo-icon-design-vector-2290132815927375141615372070.jpg', NULL, '50.7 KB', '1000 x 880 pixels', '2021-03-10 04:27:51', '2021-03-10 04:27:51'),
(67, 'bg-1.png', 'bg-11615793374.png', 'pop-up-bg-discount', '34.93 KB', '700 x 400 pixels', '2021-03-15 01:29:34', '2021-03-15 01:29:49'),
(68, 'mk-1.png', 'mk-11615793412.png', 'buxkit-promo', '40.91 KB', '263 x 346 pixels', '2021-03-15 01:30:12', '2021-03-16 05:34:55'),
(69, 'popup-banner-2.png', 'popup-banner-21615793526.png', NULL, '72.76 KB', '700 x 400 pixels', '2021-03-15 01:32:06', '2021-03-15 01:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_06_180949_create_admins_table', 1),
(5, '2019_12_07_071950_create_contact_info_items_table', 1),
(6, '2019_12_07_082524_create_static_options_table', 1),
(7, '2019_12_08_171750_create_counterups_table', 1),
(8, '2019_12_09_063224_create_testimonials_table', 1),
(9, '2019_12_10_125607_create_social_icons_table', 1),
(10, '2019_12_10_125636_create_support_infos_table', 1),
(11, '2019_12_10_210247_create_blog_categories_table', 1),
(12, '2019_12_11_074345_create_blogs_table', 1),
(13, '2019_12_12_154256_create_useful_links_table', 1),
(14, '2019_12_12_164919_create_importantlinks_table', 1),
(15, '2019_12_13_221931_create_languages_table', 1),
(16, '2019_12_28_140343_create_key_features_table', 1),
(17, '2019_12_28_161343_create_awesome_features_table', 1),
(18, '2019_12_28_175609_create_full_width_featrues_table', 1),
(19, '2019_12_29_074945_create_screenshorts_table', 1),
(20, '2019_12_29_094857_create_price_plans_table', 1),
(21, '2019_12_29_113138_create_team_members_table', 1),
(23, '2019_12_31_065223_create_how_works_table', 1),
(24, '2019_12_28_161343_create_why_choose_us_table', 2),
(25, '2019_12_29_074945_create_faqs_table', 3),
(26, '2020_01_25_155448_create_pages_table', 3),
(27, '2020_04_14_082508_create_media_uploads_table', 4),
(30, '2014_10_12_000000_create_users_table', 5),
(31, '2020_07_22_132250_create_popup_builders_table', 5),
(33, '2020_04_20_170818_create_orders_table', 6),
(34, '2020_04_21_142420_create_payment_logs_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `checkout_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `package_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_fields` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `status`, `slug`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Dynamic Page 01', '<p>At distant inhabit amongst by. Appetite welcomed interest the goodness boy not. Estimable education for disposing pronounce her. John size good gay plan sent old roof own. Inquietude saw understood his friendship frequently yet. Nature his marked ham wished.&nbsp;</p><p><br></p><p>Compliment interested discretion estimating on stimulated apartments oh. Dear so sing when in find read of call. As distrusts behaviour abilities defective is. Never at water me might. On formed merits hunted unable merely by mr whence or. Possession the unpleasing simplicity her uncommonly.&nbsp;</p><p><br></p>', 'publish', 'dynamic-page-01', 'en', '2020-03-16 17:30:25', '2021-03-08 02:44:46'),
(2, 'Dynamic Page 02', '<p>2 At distant inhabit amongst by. Appetite welcomed interest the goodness boy not. Estimable education for disposing pronounce her. John size good gay plan sent old roof own. Inquietude saw understood his friendship frequently yet. Nature his marked ham wished.&nbsp;</p><p><br></p><p>Compliment interested discretion estimating on stimulated apartments oh. Dear so sing when in find read of call. As distrusts behaviour abilities defective is. Never at water me might. On formed merits hunted unable merely by mr whence or. Possession the unpleasing simplicity her uncommonly.&nbsp;</p><p><br></p>', 'publish', 'dynamic-page-02', 'en', '2021-03-04 06:59:44', '2021-03-08 02:48:25'),
(4, 'صفحة ديناميكية 01', '<p>2 على مسافة بعيدة بين المنطقة المجاورة. ورحبت شهية الخير بالخير ولا الفتى. التعليم المقدر للتخلص من نطقها. أرسلت جون حجم خطة مثلي الجنس جيدة السقف القديم الخاصة. رأى القلق يفهم صداقته بشكل متكرر حتى الآن. رغب الطبيعة في لحم الخنزير المميز.</p><p><br></p><p><br></p><p><br></p><p>المجاملة تقدير تقدير المهتمين على تحفيز الشقق أوه. عزيزي حتى الغناء عندما تجد قراءة المكالمة. كما يثق في قدرات السلوك المعيب. أبدا في الماء قد لي. على أساس الجدارة المشكّلة التي لا يمكن مطاردتها بمجرد من أين أو. حيازة غير مرضية البساطة لها غير المألوف.</p>', 'publish', 'sfh-dynamyky-01', 'ar', '2021-03-14 05:44:48', '2021-03-14 05:44:48'),
(5, 'صفحة ديناميكية 02', '<p>2 على مسافة بعيدة بين المنطقة المجاورة. ورحبت شهية الخير بالخير ولا الفتى. التعليم المقدر للتخلص من نطقها. أرسلت جون حجم خطة مثلي الجنس جيدة السقف القديم الخاصة. رأى القلق يفهم صداقته بشكل متكرر حتى الآن. رغب الطبيعة في لحم الخنزير المميز.</p><p><br></p><p><br></p><p><br></p><p>المجاملة تقدير تقدير المهتمين على تحفيز الشقق أوه. عزيزي حتى الغناء عندما تجد قراءة المكالمة. كما يثق في قدرات السلوك المعيب. أبدا في الماء قد لي. على أساس الجدارة المشكّلة التي لا يمكن مطاردتها بمجرد من أين أو. حيازة غير مرضية البساطة لها غير المألوف.</p>', 'publish', 'sfh-dynamyky-02', 'ar', '2021-03-14 05:44:56', '2021-03-14 05:44:56');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('shawonrog@gmail.com', 'gt62syz8Z6ba5V7GTtnBIZchwHJvfZ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_logs`
--

CREATE TABLE `payment_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `track` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_logs`
--

INSERT INTO `payment_logs` (`id`, `email`, `name`, `package_name`, `package_price`, `package_gateway`, `order_id`, `status`, `transaction_id`, `track`, `created_at`, `updated_at`) VALUES
(1, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paypal', '3', 'pending', NULL, 'wHwQmlpEOdD5VM62TRaf', '2021-03-10 06:33:36', '2021-03-10 06:33:36'),
(2, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'manual_payment', '4', 'pending', '123456789', 'E2cnBgHjAlRFJC6s41Nk', '2021-03-10 06:36:16', '2021-03-11 00:04:12'),
(3, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paypal', '5', 'pending', NULL, 'TKntnjbUhuiBCRO4EjzQ', '2021-03-11 00:10:36', '2021-03-11 00:10:36'),
(4, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'mollie', '6', 'complete', 'tr_Gjm5n2bmmW', 'pY2nG5fpRlZVkYrcxEC1', '2021-03-11 00:10:55', '2021-03-11 00:19:20'),
(5, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'stripe', '7', 'pending', NULL, 'AT3QDvWXR5MCM9R5IZjN', '2021-03-11 00:20:05', '2021-03-11 00:20:05'),
(6, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paytm', '8', 'pending', NULL, 'ScW7ml2ZZ71yM0zxdZKa', '2021-03-11 00:42:14', '2021-03-11 00:42:14'),
(7, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paytm', '9', 'pending', NULL, 'ZwybEBXjQExFBdj5DZaq', '2021-03-11 01:07:14', '2021-03-11 01:07:14'),
(8, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paytm', '10', 'pending', NULL, 'RBLW7d8MYlakX2VoQNdl', '2021-03-11 01:12:30', '2021-03-11 01:12:30'),
(9, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'stripe', '11', 'pending', NULL, '4rYVbkNCkJUJlGAmugJa', '2021-03-11 01:27:49', '2021-03-11 01:27:49'),
(10, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paytm', '12', 'pending', NULL, 'jt3qanRY6lr5q3MaYEPh', '2021-03-11 01:32:35', '2021-03-11 01:32:35'),
(11, 'shawonrog@gmail.com', 'Shahriar Karim', 'PRIMARY', '449', 'paytm', '13', 'pending', NULL, 'N0bAlIa4JTKzVcfx9pfh', '2021-03-11 01:43:08', '2021-03-11 01:43:08'),
(12, 'shawonrog@gmail.com', 'Shahriar Karim', 'PRIMARY', '449', 'razorpay', '14', 'pending', NULL, '4p1wjL8vuKmfeu9ka0jZ', '2021-03-11 02:07:30', '2021-03-11 02:07:30'),
(13, 'shawonrog@gmail.com', 'Shahriar Karim', 'PRIMARY', '449', 'flutterwave', '15', 'pending', NULL, 'KptTs8hkRy67ITjPIHIt', '2021-03-11 02:08:52', '2021-03-11 02:08:52'),
(14, 'shawonrog@gmail.com', 'Shahriar Karim', 'PRIMARY', '449', 'flutterwave', '16', 'pending', NULL, '93pHxRYxDR6m21U0hdXp', '2021-03-11 02:12:49', '2021-03-11 02:12:49'),
(15, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'paystack', '17', 'pending', NULL, 'vEVPXCFcQJKD6Py3UXaU', '2021-03-11 02:14:26', '2021-03-11 02:14:26'),
(16, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'razorpay', '18', 'pending', NULL, 'FVRp6m198Uhs2ZN1dICf', '2021-03-11 02:17:00', '2021-03-11 02:17:00'),
(17, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'flutterwave', '19', 'pending', NULL, 'bRrDTQBjqWLUb8uiq4l3', '2021-03-11 02:17:13', '2021-03-11 02:17:13'),
(18, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'mollie', '20', 'pending', NULL, 'MhkSS0FQmmJKTCsUcak4', '2021-03-11 02:19:20', '2021-03-11 02:19:20'),
(19, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'paytm', '22', 'pending', NULL, 'FBWQOwqUcR5pWKxvkz0P', '2021-03-11 02:19:57', '2021-03-11 02:19:57'),
(20, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'stripe', '23', 'pending', NULL, 'Idgegj1J00nHkASOINU6', '2021-03-11 02:20:19', '2021-03-11 02:20:19'),
(21, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'razorpay', '24', 'pending', NULL, 'Ggea8oHs8dyDcoUQLDl9', '2021-03-11 02:20:51', '2021-03-11 02:20:51'),
(22, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'flutterwave', '25', 'pending', NULL, 'zLgVYAnj1PrimLSoN0cK', '2021-03-11 02:21:21', '2021-03-11 02:21:21'),
(23, 'shawonrog@gmail.com', 'Shahriar Karim', 'PROFESSIONAL', '1000', 'paystack', '26', 'pending', NULL, 'mGPN0uNTzBUiAS7HI2nQ', '2021-03-11 02:21:51', '2021-03-11 02:21:51'),
(24, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paytm', '27', 'pending', NULL, '075oYdhfYNR6My0O6NW4', '2021-03-11 02:32:10', '2021-03-11 02:32:10'),
(25, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'manual_payment', '28', 'complete', 'wdawdawd', 'zS5K9r13sducervCF5g0', '2021-03-11 03:16:10', '2021-03-11 04:31:36'),
(26, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'paytm', '29', 'pending', NULL, 'dpsZHc2KCMk9VISYwJMe', '2021-03-11 04:41:27', '2021-03-11 04:41:27'),
(27, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'manual_payment', '30', 'pending', '123456789', 'zxVX1SlDjDdigB4Vq3Iw', '2021-03-11 04:41:39', '2021-03-11 04:41:39'),
(28, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'manual_payment', '31', 'pending', 'awdawdawd', 'AtJBk4UNKjtaoTWFLbiI', '2021-03-11 08:02:40', '2021-03-11 08:02:40'),
(29, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'PROFESSIONAL', '1000', 'manual_payment', '32', 'pending', '12345678', 'IaVYbwCB1Ozjlz5v7Hzp', '2021-03-14 04:10:32', '2021-03-14 04:10:32'),
(30, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'خبرات', '449', 'manual_payment', '33', 'pending', '125245024742', 'trRt6pUHcmSH8xlSFT3n', '2021-03-15 02:12:47', '2021-03-15 02:12:47'),
(31, 'shawon9324@gmail.com', 'Md. Shahriar Karim Shawon', 'خبرات', '449', 'manual_payment', '34', 'pending', '123456789', 'bdtVaRTGx4RPCVmfAS74', '2021-03-15 05:20:39', '2021-03-15 05:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `popup_builders`
--

CREATE TABLE `popup_builders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `only_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_time_end` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btn_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `popup_builders`
--

INSERT INTO `popup_builders` (`id`, `name`, `type`, `title`, `only_image`, `background_image`, `offer_time_end`, `button_text`, `button_link`, `btn_status`, `lang`, `description`, `created_at`, `updated_at`) VALUES
(1, 'discount popup', 'discount', 'Discount 25%', '68', '67', '2021-05-14', 'Buy Now', '#', 'on', 'en', 'Out believe has requested not how comfort evident.', '2021-03-09 00:27:45', '2021-03-16 05:37:53'),
(3, 'notice popup', 'notice', 'Notice!!', NULL, NULL, NULL, NULL, NULL, NULL, 'en', 'She propriety immediate was improving. He or entrance humored likewise moderate. Much nor game son say feel. Fat make met can must form into gate. Me we offending prevailed discovery.', '2021-03-11 08:40:14', '2021-03-11 08:40:14'),
(4, 'big sale', 'only_image', NULL, '69', NULL, NULL, NULL, NULL, NULL, 'en', NULL, '2021-03-15 01:32:16', '2021-03-15 01:32:16');

-- --------------------------------------------------------

--
-- Table structure for table `price_plans`
--

CREATE TABLE `price_plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `features` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `btn_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `btn_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `price_plans`
--

INSERT INTO `price_plans` (`id`, `title`, `price`, `type`, `features`, `btn_text`, `btn_url`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'PROFESSIONAL', '1000', '/Mo', '10 Gb Space,\r\n24/7 Support,\r\nColor Available,\r\nUnlimited Account', 'Purchase', '#11', 'en', '2020-01-04 13:56:00', '2021-03-06 08:04:12'),
(6, 'BUSINESS', '249', '/Mo', '10 Gb Space,\r\n24/7 Support,\r\nColor Available,\r\nUnlimited Account', 'PURCHASE', '#', 'en', '2021-03-04 01:48:27', '2021-03-07 02:11:16'),
(7, 'PRIMARY', '449', '/Mo', '10 Gb Space,\r\n24/7 Support,\r\nColor Available,\r\nUnlimited Account', 'PURCHASE', '#', 'en', '2021-03-07 02:07:38', '2021-03-08 00:18:44'),
(8, 'المحترفين', '1000', '/ مو', 'مساحة 10 جيجا بايت ,\r\nدعم على مدار الساعة طوال أيام الأسبوع  ,\r\nاللون متاح  ,\r\nحساب غير محدود', 'شراء', '#', 'ar', '2021-03-14 05:20:40', '2021-03-14 05:20:40'),
(9, 'عمل', '249', '/ مو', 'مساحة 10 جيجا بايت ,\r\nدعم على مدار الساعة طوال أيام الأسبوع  ,\r\nاللون متاح  ,\r\nحساب غير محدود', 'شراء', '#', 'ar', '2021-03-14 05:22:30', '2021-03-14 05:22:30'),
(10, 'خبرات', '449', '/ مو', 'مساحة 10 جيجا بايت ,\r\nدعم على مدار الساعة طوال أيام الأسبوع  ,\r\nاللون متاح  ,\r\nحساب غير محدود', 'شراء', '#', 'ar', '2021-03-14 05:24:09', '2021-03-14 05:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `social_icons`
--

CREATE TABLE `social_icons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_icons`
--

INSERT INTO `social_icons` (`id`, `icon`, `url`, `created_at`, `updated_at`) VALUES
(1, 'fab fa-facebook-f', '#', '2020-01-04 07:04:13', '2020-01-04 07:04:13'),
(2, 'fab fa-twitter', '#', '2020-01-04 07:04:22', '2020-01-04 07:04:22'),
(3, 'fab fa-linkedin-in', '#', '2020-01-04 07:04:39', '2021-03-08 05:05:06'),
(4, 'fab fa-pinterest-p', '#', '2020-01-04 07:04:48', '2020-01-04 07:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `static_options`
--

CREATE TABLE `static_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_options`
--

INSERT INTO `static_options` (`id`, `option_name`, `option_value`, `created_at`, `updated_at`) VALUES
(1, 'home_page_variant', '01', '2020-01-03 18:55:55', '2021-03-16 05:12:38'),
(2, 'site_title', 'Buxkit', '2020-01-03 18:56:59', '2020-01-04 07:29:04'),
(3, 'site_tag_line', 'Software Landing Page', '2020-01-03 18:56:59', '2020-01-04 07:29:04'),
(4, 'site_footer_copyright', '{copy} Copyright {year} . All Right Reserved By Buxkit', '2020-01-03 18:56:59', '2020-01-04 07:29:04'),
(5, 'site_color', '#d13a74', '2020-01-03 18:56:59', '2021-03-16 05:33:42'),
(6, 'site_main_color_two', '#6a39b0', '2020-01-03 18:56:59', '2021-03-16 05:33:42'),
(7, 'site_logo', '6', '2020-01-03 18:57:41', '2021-03-07 01:54:39'),
(8, 'navbar_button', 'on', '2020-01-04 07:59:55', '2021-03-15 05:55:42'),
(9, 'navbar_button_text', 'Buy Now', '2020-01-04 07:59:55', '2020-01-04 08:02:25'),
(10, 'navbar_button_url', '#', '2020-01-04 07:59:55', '2020-01-04 08:02:25'),
(11, 'home_page_01_header_right_image', '29', '2020-01-04 08:20:38', '2021-03-14 04:49:49'),
(12, 'home_page_01_header_bg_image', '53', '2020-01-04 08:20:38', '2021-03-14 04:49:49'),
(13, 'home_page_header_subtitle', 'FIRS EVER AMAZING THING', '2020-01-04 08:20:38', '2020-01-04 08:51:51'),
(14, 'home_page_header_title', 'The app that build for your manegment', '2020-01-04 08:20:38', '2020-01-04 08:51:51'),
(15, 'home_page_header_btn_one_text', 'Get Started For Free', '2020-01-04 08:20:38', '2020-01-04 08:51:51'),
(16, 'home_page_header_btn_one_url', '#wadawd', '2020-01-04 08:20:38', '2021-03-03 06:10:59'),
(17, 'home_page_header_description', 'You zealously departure had procuring suspicion. Books whose front would purse if be do decay. Quitting you way formerly disposed perceivede are. Common turned boy direct and yet sociable feli', '2020-01-04 08:20:38', '2020-01-04 08:51:51'),
(18, 'home_page_02_header_right_image', '46', '2020-01-04 08:41:19', '2021-03-07 07:20:41'),
(19, 'home_page_02_header_bg_image', '47', '2020-01-04 08:41:19', '2021-03-07 07:20:41'),
(20, 'home_page_03_header_right_image', '51', '2020-01-04 08:44:00', '2021-03-07 07:30:02'),
(21, 'home_page_03_header_bg_image', '48', '2020-01-04 08:44:00', '2021-03-07 07:30:02'),
(22, 'home_page_04_header_right_image', '52', '2020-01-04 08:48:28', '2021-03-07 08:22:44'),
(23, 'home_page_04_header_bg_image', '49', '2020-01-04 08:48:28', '2021-03-07 08:22:44'),
(24, 'home_page_05_header_right_image', '52', '2020-01-04 08:51:10', '2021-03-07 08:41:43'),
(25, 'home_page_05_header_bg_image', '53', '2020-01-04 08:51:10', '2021-03-07 08:41:43'),
(26, 'home_page_06_header_right_image', '52', '2020-01-04 08:51:51', '2021-03-07 08:42:33'),
(27, 'home_page_06_header_bg_image', '50', '2020-01-04 08:51:51', '2021-03-07 08:42:33'),
(28, 'home_page_01_why_choose_us_title', 'Why Everybody Choose This App', '2020-01-04 11:53:43', '2020-01-04 11:53:56'),
(29, 'home_page_01_why_choose_us_description', 'solutions with the best. Incididunt dolor sit amet, adipiscing elitsed tempor vel metus scelerisque ante sollicitudin.', '2020-01-04 11:53:43', '2020-01-04 11:53:56'),
(30, 'home_page_01_why_choose_us_bg_image', '32', '2020-01-04 11:53:56', '2021-03-14 05:40:13'),
(31, 'home_page_01_intro_video_url', 'https://www.youtube.com/watch?v=-0ovL_iYIiI', '2020-01-04 12:47:39', '2021-03-07 03:51:37'),
(32, 'home_page_01_intro_video_image', '34', '2020-01-04 12:47:55', '2021-03-07 03:51:37'),
(33, 'home_page_01_intro_video_bg_image', '33', '2020-01-04 12:47:55', '2021-03-07 03:51:37'),
(34, 'home_page_01_price_plan_title', 'Exclusive Pricing Plans', '2020-01-04 13:52:07', '2020-01-04 13:52:07'),
(35, 'home_page_01_price_plan_description', 'solutions with the best. Incididunt dolor sit amet, adipiscing elitsed tempor vel metus scelerisque ante sollicitudin.', '2020-01-04 13:52:07', '2020-01-04 13:52:07'),
(36, 'home_page_01_team_member_title', 'Meet The Team', '2020-01-04 14:06:19', '2020-01-04 14:06:19'),
(37, 'home_page_01_team_member_description', 'solutions with the best. Incididunt dolor sit amet, adipiscing elitsed tempor vel metus scelerisque ante sollicitudin.', '2020-01-04 14:06:19', '2020-01-04 14:06:19'),
(38, 'home_page_01_contact_title', 'Contact Us', '2020-01-04 14:34:46', '2020-01-04 14:39:53'),
(39, 'home_page_01_contact_description', 'solutions with the best. Incididunt dolor sit amet, adipiscing elitsed tempor vel metus scelerisque ante sollicitudin.', '2020-01-04 14:34:46', '2020-01-04 14:39:53'),
(40, 'home_page_01_contact_bg_image', '35', '2020-01-04 14:36:41', '2021-03-14 05:41:51'),
(41, 'home_page_01_faq_right_side_image', '36', '2020-01-04 14:46:53', '2021-03-14 05:42:12'),
(42, 'home_page_01_faq_title', 'General Asked Quetions', '2020-01-04 14:46:53', '2020-01-04 14:46:53'),
(43, 'home_page_01_faq_description', 'solutions with the best. Incididunt dolor sit amet, adipiscing elitsed tempor vel metus scelerisque ante sollicitudin.', '2020-01-04 14:46:53', '2020-01-04 14:46:53'),
(44, 'footer_bg_image', '22', '2020-01-04 16:23:04', '2021-03-06 02:30:39'),
(45, 'about_widget_description', 'Maleiers het generaal vreemden bevatten behoefte are afneemt en soorten fortuin of.', '2020-01-04 16:32:27', '2020-01-04 16:37:34'),
(46, 'about_widget_social_icon_one', 'fab fa-facebook-f', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(47, 'about_widget_social_icon_two', 'fab fa-twitter', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(48, 'about_widget_social_icon_three', 'fab fa-pinterest-p', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(49, 'about_widget_social_icon_four', 'fab fa-linkedin-in', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(50, 'about_widget_social_icon_one_url', '#', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(51, 'about_widget_social_icon_two_url', '#', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(52, 'about_widget_social_icon_three_url', '#', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(53, 'about_widget_social_icon_four_url', '#', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(54, 'about_widget_logo', '23', '2020-01-04 16:32:27', '2021-03-14 05:45:58'),
(55, 'useful_link_widget_title', 'Useful Links', '2020-01-04 16:33:07', '2020-01-04 16:33:07'),
(56, 'recent_post_widget_title', 'Recent Posts', '2020-01-04 16:44:15', '2020-01-04 16:44:15'),
(57, 'recent_post_widget_item', '2', '2020-01-04 16:44:15', '2021-03-14 06:02:27'),
(58, 'important_link_widget_title', 'Need Help ?', '2020-01-04 16:44:20', '2020-01-04 16:44:35'),
(59, 'site_favicon', '5', '2020-01-04 16:52:53', '2021-03-07 01:54:39'),
(60, 'site_breadcrumb_bg', '8', '2020-01-04 16:53:46', '2021-03-07 01:54:39'),
(61, 'site_white_logo', '7', '2020-01-04 16:54:27', '2021-03-07 01:54:39'),
(62, 'blog_page_title', 'Blog', '2020-01-04 16:57:09', '2020-01-04 16:57:09'),
(63, 'blog_page_item', '6', '2020-01-04 16:57:09', '2021-03-14 05:38:50'),
(64, 'blog_page_category_widget_title', 'Category', '2020-01-04 16:57:09', '2020-01-04 16:57:09'),
(65, 'blog_page_recent_post_widget_title', 'Recent Post', '2020-01-04 16:57:09', '2020-01-04 16:57:09'),
(66, 'blog_page_recent_post_widget_item', '4', '2020-01-04 16:57:09', '2021-03-14 05:38:50'),
(67, 'body_font_family', 'Poppins', '2020-01-04 19:01:50', '2021-03-07 01:56:38'),
(68, 'body_font_variant', 'a:5:{i:0;s:7:\"regular\";i:1;s:3:\"500\";i:2;s:3:\"600\";i:3;s:3:\"700\";i:4;s:3:\"800\";}', '2020-01-04 19:01:50', '2021-03-07 01:56:38'),
(69, 'heading_font_family', 'Poppins', '2020-01-04 19:01:50', '2021-03-07 01:56:38'),
(70, 'heading_font_variant', 'a:4:{i:0;s:7:\"regular\";i:1;s:3:\"500\";i:2;s:3:\"600\";i:3;s:3:\"700\";}', '2020-01-04 19:01:50', '2021-03-07 01:56:38'),
(71, 'heading_font', 'on', '2020-01-04 19:01:50', '2021-03-07 01:56:38'),
(72, 'site_disqus_key', 'buxkit', '2020-01-05 17:29:43', '2021-03-15 03:12:05'),
(73, 'site_google_analytics', 'UA-155343796-1', '2020-01-05 17:30:34', '2021-03-15 03:12:05'),
(74, 'tawk_api_key', '<!--Start of Tawk.to Script-->\r\n<script type=\"text/javascript\">\r\n    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n    (function(){\r\n        var s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\n        s1.async=true;\r\n        s1.src=\"https://embed.tawk.to/5e0b3e167e39ea1242a27b69/default\";\r\n        s1.charset=\'UTF-8\';\r\n        s1.setAttribute(\'crossorigin\',\'*\');\r\n        s0.parentNode.insertBefore(s1,s0);\r\n    })();\r\n</script>\r\n<!--End of Tawk.to Script-->', '2020-01-05 17:30:38', '2021-03-15 03:12:05'),
(75, 'site_global_email', 'info@shawon.xgenious.com', '2020-01-10 13:47:48', '2021-03-09 05:40:29'),
(76, 'site_global_email_template', '<p>Hello</p><p>Dear @username</p><p><br></p><p>@message</p><p><br></p><p>Regards</p><p>@company</p>', '2020-01-10 13:47:48', '2021-03-09 05:40:29'),
(77, 'key_feature_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(78, 'full_width_features_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(79, 'why_us_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(80, 'testimonial_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(81, 'intro_video_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(82, 'price_plan_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(83, 'team_member_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(84, 'contact_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(85, 'faq_section_status', 'on', '2020-01-15 13:05:58', '2021-03-08 00:57:56'),
(86, 'brand_logo_section_status', 'on', '2020-01-15 13:15:01', '2021-03-08 00:57:57'),
(87, 'site_meta_en_tags', 'Software Landing Platform,Buxkit,Xgenious', '2021-03-01 23:08:29', '2021-03-16 05:34:59'),
(88, 'site_meta_en_description', 'Buxkit Software Landing Page is the perfect app showcase Platform PHP Scripts Theme. Buxkit is a better way to present your modern business. It’s easy to customize and also well documented and child theme included. it also compatible with Desktop, laptop, mobile and also compatible with major browsers.', '2021-03-01 23:08:29', '2021-03-16 05:34:59'),
(89, 'site_third_party_tracking_code', NULL, '2021-03-02 00:39:52', '2021-03-15 03:12:05'),
(90, 'site_google_captcha_v3_site_key', '6LdvUeQUAAAAAHKM02AWBjtKAAL0-AqUk_qkqa0O', '2021-03-02 00:39:52', '2021-03-15 03:12:05'),
(91, 'site_google_captcha_v3_secret_key', '6LdvUeQUAAAAABaCkkQbMY-z2XaqYsLSWwYgB6Ru', '2021-03-02 00:39:52', '2021-03-15 03:12:05'),
(92, 'service_query_en_success_message', 'Thanks for your message. we will contact you soon.', '2021-03-02 01:04:11', '2021-03-02 01:26:48'),
(93, 'case_study_query_en_success_message', 'dd', '2021-03-02 01:04:11', '2021-03-02 01:26:48'),
(94, 'quote_mail_en_success_message', NULL, '2021-03-02 01:04:11', '2021-03-02 01:26:48'),
(95, 'contact_mail_en_success_message', 'Thanks for your contact!', '2021-03-02 01:04:11', '2021-03-16 03:25:35'),
(96, 'get_in_touch_mail_en_success_message', NULL, '2021-03-02 01:04:11', '2021-03-02 01:26:48'),
(97, 'apply_job_en_success_message', NULL, '2021-03-02 01:04:11', '2021-03-02 01:26:48'),
(98, 'order_mail_en_success_message', 'Thanks for your order. we will contact you soon.', '2021-03-02 01:04:11', '2021-03-16 03:25:35'),
(99, 'event_attendance_mail_en_success_message', NULL, '2021-03-02 01:04:12', '2021-03-02 01:26:48'),
(100, 'feedback_form_mail_en_success_message', NULL, '2021-03-02 01:04:12', '2021-03-02 01:26:48'),
(128, 'site_smtp_mail_mailer', 'mailgun', '2021-03-02 01:32:09', '2021-03-02 02:07:56'),
(129, 'site_smtp_mail_host', 'smtp.example.com', '2021-03-02 01:32:09', '2021-03-02 02:07:56'),
(130, 'site_smtp_mail_port', '587', '2021-03-02 01:32:10', '2021-03-02 02:07:56'),
(131, 'site_smtp_mail_username', 'super_admin', '2021-03-02 01:32:10', '2021-03-02 02:07:56'),
(132, 'site_smtp_mail_password', '12345678', '2021-03-02 01:32:10', '2021-03-02 02:07:56'),
(133, 'site_smtp_mail_encryption', 'ssl', '2021-03-02 01:32:10', '2021-03-02 02:07:56'),
(134, 'quote_page_slug', 'quote', '2021-03-02 01:48:50', '2021-03-02 01:48:50'),
(135, 'about_page_slug', 'about', '2021-03-02 01:48:50', '2021-03-09 08:18:18'),
(136, 'service_page_slug', 'service', '2021-03-02 01:48:50', '2021-03-02 01:48:50'),
(137, 'work_page_slug', 'service', '2021-03-02 01:48:50', '2021-03-02 01:48:50'),
(138, 'team_page_slug', 'team', '2021-03-02 01:48:50', '2021-03-09 08:18:18'),
(139, 'faq_page_slug', 'faq', '2021-03-02 01:48:50', '2021-03-02 01:48:50'),
(140, 'price_plan_page_slug', 'price-plan', '2021-03-02 01:48:50', '2021-03-09 08:18:18'),
(141, 'blog_page_slug', 'blog', '2021-03-02 01:48:50', '2021-03-09 08:18:18'),
(142, 'contact_page_slug', 'contact', '2021-03-02 01:48:50', '2021-03-09 08:18:18'),
(143, 'career_with_us_page_slug', 'career', '2021-03-02 01:48:50', '2021-03-02 01:48:50'),
(144, 'events_page_slug', 'events', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(145, 'knowledgebase_page_slug', 'knowledgebase', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(146, 'donation_page_slug', 'donations', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(147, 'product_page_slug', 'products', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(148, 'testimonial_page_slug', 'testimonial', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(149, 'feedback_page_slug', 'feedback', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(150, 'clients_feedback_page_slug', 'clients-feedback', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(151, 'image_gallery_page_slug', 'image-gallery', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(152, 'donor_page_slug', 'donor-list', '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(153, 'about_page_en_name', 'About', '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(154, 'about_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(155, 'about_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(156, 'service_page_en_name', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(157, 'service_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(158, 'service_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(159, 'work_page_en_name', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(160, 'work_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(161, 'work_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(162, 'team_page_en_name', 'Team', '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(163, 'team_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(164, 'team_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(165, 'price_plan_page_en_name', 'Price Plan', '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(166, 'price_plan_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(167, 'price_plan_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(168, 'faq_page_en_name', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(169, 'faq_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(170, 'faq_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-02 01:48:51'),
(171, 'blog_page_en_name', 'Blog', '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(172, 'blog_page_en_meta_description', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(173, 'blog_page_en_meta_tags', NULL, '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(174, 'contact_page_en_name', 'Contact', '2021-03-02 01:48:51', '2021-03-09 08:18:18'),
(175, 'contact_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-09 08:18:19'),
(176, 'contact_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-09 08:18:19'),
(177, 'career_with_us_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(178, 'career_with_us_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(179, 'career_with_us_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(180, 'events_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(181, 'events_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(182, 'events_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(183, 'knowledgebase_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(184, 'knowledgebase_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(185, 'knowledgebase_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(186, 'quote_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(187, 'quote_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(188, 'quote_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(189, 'donation_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(190, 'donation_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(191, 'donation_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(192, 'product_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(193, 'product_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(194, 'product_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(195, 'testimonial_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(196, 'testimonial_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(197, 'testimonial_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(198, 'feedback_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(199, 'feedback_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(200, 'feedback_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(201, 'clients_feedback_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(202, 'clients_feedback_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(203, 'clients_feedback_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(204, 'image_gallery_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(205, 'image_gallery_page_en_meta_tags', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(206, 'image_gallery_page_en_meta_description', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(207, 'donor_page_en_name', NULL, '2021-03-02 01:48:52', '2021-03-02 01:48:52'),
(208, 'donor_page_en_meta_tags', NULL, '2021-03-02 01:48:53', '2021-03-02 01:48:53'),
(209, 'donor_page_en_meta_description', NULL, '2021-03-02 01:48:53', '2021-03-02 01:48:53'),
(381, 'site_en_title', 'Buxkit', '2021-03-02 02:11:16', '2021-03-16 05:33:41'),
(382, 'site_en_tag_line', 'Software Landing Page', '2021-03-02 02:11:16', '2021-03-16 05:33:41'),
(383, 'site_en_footer_copyright', '{copy} Copyright {year} . All Right Reserved By Buxkit', '2021-03-02 02:11:16', '2021-03-16 05:33:41'),
(393, 'site_admin_panel_nav_sticky', NULL, '2021-03-02 02:11:16', '2021-03-16 05:33:41'),
(394, 'site_frontend_nav_sticky', NULL, '2021-03-02 02:11:16', '2021-03-16 05:33:41'),
(395, 'og_meta_image_for_site', NULL, '2021-03-02 02:11:16', '2021-03-16 05:33:42'),
(396, 'site_secondary_color', '#736b7d', '2021-03-02 02:11:16', '2021-03-16 05:33:42'),
(397, 'site_heading_color', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(398, 'site_paragraph_color', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(399, 'site_rtl_enabled', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(400, 'site_admin_dark_mode', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(401, 'site_maintenance_mode', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(402, 'site_payment_gateway', 'on', '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(403, 'language_select_option', 'on', '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(404, 'site_sticky_navbar_enabled', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(405, 'disable_backend_preloader', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(406, 'disable_user_email_verify', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(407, 'site_force_ssl_redirection', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(408, 'portfolio_home_color', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(409, 'logistics_home_color', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(410, 'industry_home_color', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(411, 'construction_home_color', NULL, '2021-03-02 02:11:17', '2021-03-16 05:33:42'),
(412, 'site_gdpr_cookie_delay', '5000', '2021-03-02 03:18:25', '2021-03-14 22:42:11'),
(413, 'site_gdpr_cookie_expire', '30', '2021-03-02 03:18:25', '2021-03-14 22:42:11'),
(414, 'site_gdpr_cookie_enabled', 'on', '2021-03-02 03:19:04', '2021-03-14 22:42:11'),
(415, 'site_gdpr_cookie_en_title', 'Cookies & Privacy', '2021-03-02 03:21:14', '2021-03-14 22:42:09'),
(416, 'site_gdpr_cookie_en_message', 'Is education residence conveying so so. Suppose shyness say ten behaved morning had. Any unsatiable assistance compliment occasional too reasonably advantages.', '2021-03-02 03:21:14', '2021-03-14 22:42:09'),
(417, 'site_gdpr_cookie_en_more_info_label', 'More information', '2021-03-02 03:21:14', '2021-03-14 22:42:09'),
(418, 'site_gdpr_cookie_en_more_info_link', '{url}/p/privacy-policy', '2021-03-02 03:21:14', '2021-03-14 22:42:09'),
(419, 'site_gdpr_cookie_en_accept_button_label', 'Accept Cookie', '2021-03-02 03:21:14', '2021-03-14 22:42:09'),
(420, 'site_gdpr_cookie_en_decline_button_label', 'Decline Cookie', '2021-03-02 03:21:14', '2021-03-14 22:42:09'),
(439, 'item_purchase_key', 'demo', '2021-03-02 03:52:07', '2021-03-16 05:45:51'),
(440, 'popup_enable_status', 'on', '2021-03-02 04:51:52', '2021-03-16 05:38:27'),
(441, 'popup_delay_time', '1000', '2021-03-02 04:51:52', '2021-03-16 05:38:27'),
(442, 'popup_selected_en_id', '1', '2021-03-02 04:51:52', '2021-03-16 05:38:28'),
(446, 'navbar_button_text_ur', 'ابھی خریدیں', '2021-03-02 07:03:45', '2021-03-14 07:02:16'),
(448, 'navbar_button_text_en', 'Buy Now', '2021-03-02 07:05:55', '2021-03-15 05:55:42'),
(449, 'navbar_button_url_en', '#price-plan', '2021-03-02 07:05:55', '2021-03-15 05:55:42'),
(452, 'navbar_button_text_sp', 'Compra ahora', '2021-03-02 07:05:55', '2021-03-14 07:02:16'),
(454, 'home_page_01_why_choose_us_title_en', 'Why Choose Us', '2021-03-03 04:27:20', '2021-03-14 05:40:12'),
(455, 'home_page_01_why_choose_us_description_en', 'We bring our diverse background in advertising, design, branding, public relations, research, and strategic planning to work for your company. Not only will your materials look great – they will get results.', '2021-03-03 04:27:20', '2021-03-14 05:40:13'),
(458, 'home_page_01_why_choose_us_title_sp', NULL, '2021-03-03 04:27:21', '2021-03-14 05:40:13'),
(459, 'home_page_01_why_choose_us_description_sp', NULL, '2021-03-03 04:27:21', '2021-03-14 05:40:13'),
(460, 'home_page_01_why_choose_us_title_ur', NULL, '2021-03-03 04:27:21', '2021-03-14 05:40:13'),
(461, 'home_page_01_why_choose_us_description_ur', NULL, '2021-03-03 04:27:21', '2021-03-14 05:40:13'),
(462, 'home_page_01_price_plan_title_en', 'Exclusive Pricing Plans', '2021-03-03 04:39:37', '2021-03-14 05:40:43'),
(463, 'home_page_01_price_plan_description_en', 'solutions with the best. Incididunt dolor sit amet, adipiscing elit sed tempor vel metus scelerisque ante sollicitudin.', '2021-03-03 04:39:37', '2021-03-14 05:40:43'),
(466, 'home_page_01_price_plan_title_sp', NULL, '2021-03-03 04:39:37', '2021-03-14 05:40:43'),
(467, 'home_page_01_price_plan_description_sp', NULL, '2021-03-03 04:39:37', '2021-03-14 05:40:43'),
(468, 'home_page_01_price_plan_title_ur', NULL, '2021-03-03 04:39:37', '2021-03-14 05:40:43'),
(469, 'home_page_01_price_plan_description_ur', NULL, '2021-03-03 04:39:37', '2021-03-14 05:40:43'),
(470, 'home_page_01_team_member_title_en', 'Meet The Team', '2021-03-03 04:47:43', '2021-03-14 05:41:19'),
(471, 'home_page_01_team_member_description_en', 'solutions with the best. Incididunt dolor sit amet, adipiscing elit sed tempor vel metus scelerisque ante sollicitudin', '2021-03-03 04:47:43', '2021-03-14 05:41:19'),
(474, 'home_page_01_team_member_title_sp', NULL, '2021-03-03 04:47:43', '2021-03-14 05:41:19'),
(475, 'home_page_01_team_member_description_sp', NULL, '2021-03-03 04:47:43', '2021-03-14 05:41:19'),
(476, 'home_page_01_team_member_title_ur', NULL, '2021-03-03 04:47:43', '2021-03-14 05:41:19'),
(477, 'home_page_01_team_member_description_ur', NULL, '2021-03-03 04:47:43', '2021-03-14 05:41:19'),
(478, 'home_page_01_contact_title_en', 'Contact Us', '2021-03-03 04:56:08', '2021-03-14 05:41:50'),
(479, 'home_page_01_contact_description_en', 'solutions with the best. Incididunt dolor sit amet, adipiscing elit sed tempor vel metus scelerisque ante sollicitudin', '2021-03-03 04:56:09', '2021-03-14 05:41:50'),
(482, 'home_page_01_contact_title_sp', NULL, '2021-03-03 04:56:09', '2021-03-14 05:41:50'),
(483, 'home_page_01_contact_description_sp', NULL, '2021-03-03 04:56:09', '2021-03-14 05:41:50'),
(484, 'home_page_01_contact_title_ur', NULL, '2021-03-03 04:56:09', '2021-03-14 05:41:50'),
(485, 'home_page_01_contact_description_ur', NULL, '2021-03-03 04:56:09', '2021-03-14 05:41:50'),
(486, 'home_page_01_faq_title_en', 'General Asked Questions', '2021-03-03 05:02:59', '2021-03-14 05:42:12'),
(487, 'home_page_01_faq_description_en', 'solutions with the best. Incididunt dolor sit amet, adipiscing elit sed tempor vel metus scelerisque ante sollicitudin', '2021-03-03 05:02:59', '2021-03-14 05:42:12'),
(490, 'home_page_01_faq_title_sp', NULL, '2021-03-03 05:02:59', '2021-03-14 05:42:12'),
(491, 'home_page_01_faq_description_sp', NULL, '2021-03-03 05:02:59', '2021-03-14 05:42:12'),
(492, 'home_page_01_faq_title_ur', NULL, '2021-03-03 05:02:59', '2021-03-14 05:42:12'),
(493, 'home_page_01_faq_description_ur', NULL, '2021-03-03 05:02:59', '2021-03-14 05:42:12'),
(494, 'home_page_header_title_en', 'The app that builds for your management', '2021-03-03 06:08:49', '2021-03-14 04:49:48'),
(495, 'home_page_header_subtitle_en', 'FIRST EVER AMAZING THING', '2021-03-03 06:08:49', '2021-03-14 04:49:48'),
(496, 'home_page_header_description_en', 'You zealously departure had procuring suspicion. Books whose front would purse if be do decay. Quitting you way formerly disposed perceived are. Common turned boy direct and yet sociable feli', '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(497, 'home_page_header_btn_one_text_en', 'Get Started For Free', '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(502, 'home_page_header_title_sp', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(503, 'home_page_header_subtitle_sp', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(504, 'home_page_header_description_sp', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(505, 'home_page_header_btn_one_text_sp', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(506, 'home_page_header_title_ur', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(507, 'home_page_header_subtitle_ur', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(508, 'home_page_header_description_ur', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(509, 'home_page_header_btn_one_text_ur', NULL, '2021-03-03 06:08:49', '2021-03-14 04:49:49'),
(512, 'home_page_header_btn_one_url_en', '#', '2021-03-03 06:31:57', '2021-03-14 04:49:49'),
(516, 'blog_page_title_en', 'Blog', '2021-03-06 02:19:44', '2021-03-14 05:38:49'),
(517, 'blog_page_category_widget_title_en', 'Category', '2021-03-06 02:19:44', '2021-03-14 05:38:49'),
(518, 'blog_page_recent_post_widget_title_en', 'Recent Post', '2021-03-06 02:19:44', '2021-03-14 05:38:49'),
(522, 'blog_page_title_sp', NULL, '2021-03-06 02:19:45', '2021-03-14 05:38:49'),
(523, 'blog_page_category_widget_title_sp', NULL, '2021-03-06 02:19:45', '2021-03-14 05:38:49'),
(524, 'blog_page_recent_post_widget_title_sp', NULL, '2021-03-06 02:19:45', '2021-03-14 05:38:49'),
(525, 'blog_page_title_ur', NULL, '2021-03-06 02:19:45', '2021-03-14 05:38:49'),
(526, 'blog_page_category_widget_title_ur', NULL, '2021-03-06 02:19:45', '2021-03-14 05:38:49'),
(527, 'blog_page_recent_post_widget_title_ur', NULL, '2021-03-06 02:19:45', '2021-03-14 05:38:49'),
(528, 'about_widget_description_en', 'Maleiers het generaal vreemden bevatten behoefte are afneemt en soorten fortuin of.', '2021-03-06 04:13:13', '2021-03-14 05:45:58'),
(530, 'about_widget_description_sp', NULL, '2021-03-06 04:13:13', '2021-03-14 05:45:58'),
(531, 'about_widget_description_ur', NULL, '2021-03-06 04:13:13', '2021-03-14 05:45:58'),
(532, 'useful_link_widget_title_en', 'Useful Links', '2021-03-06 04:35:53', '2021-03-14 05:46:38'),
(534, 'useful_link_widget_title_sp', NULL, '2021-03-06 04:35:53', '2021-03-14 05:46:38'),
(535, 'useful_link_widget_title_ur', NULL, '2021-03-06 04:35:53', '2021-03-14 05:46:38'),
(536, 'recent_post_widget_title_en', 'Recent Posts', '2021-03-06 05:09:39', '2021-03-14 06:02:27'),
(538, 'recent_post_widget_title_sp', NULL, '2021-03-06 05:09:39', '2021-03-14 06:02:27'),
(539, 'recent_post_widget_title_ur', NULL, '2021-03-06 05:09:39', '2021-03-14 06:02:27'),
(540, 'important_link_widget_title_en', 'Need Help ?', '2021-03-06 05:28:34', '2021-03-14 06:03:05'),
(542, 'important_link_widget_title_sp', NULL, '2021-03-06 05:28:34', '2021-03-14 06:03:05'),
(543, 'important_link_widget_title_ur', NULL, '2021-03-06 05:28:34', '2021-03-14 06:03:05'),
(545, 'og_meta_en_description', 'Buxkit Software Landing Page is the perfect app showcase Platform PHP Scripts Theme. Buxkit is a better way to present your modern business. It’s easy to customize and also well documented and child theme included. it also compatible with Desktop, laptop, mobile and also compatible with major browsers.', '2021-03-06 08:28:21', '2021-03-16 05:34:59'),
(546, 'og_meta_en_image', '6', '2021-03-06 08:28:21', '2021-03-16 05:34:59'),
(547, 'navbar_button_text_ar', 'اشتري الآن', '2021-03-07 01:36:42', '2021-03-15 05:55:42'),
(548, 'navbar_button_url_ar', '#price-plan', '2021-03-07 01:36:42', '2021-03-15 05:55:42'),
(549, 'site_ar_title', 'Buxkit', '2021-03-07 01:55:30', '2021-03-16 05:33:41'),
(550, 'site_ar_tag_line', 'الصفحة المقصودة للبرامج', '2021-03-07 01:55:30', '2021-03-16 05:33:41'),
(551, 'site_ar_footer_copyright', '{copy} حقوق النشر{year} . جميع الحقوق محفوظة بواسطة Buxkit', '2021-03-07 01:55:30', '2021-03-16 05:33:41'),
(552, 'home_page_header_title_ar', 'التطبيق الذي يبني لإدارتك', '2021-03-07 02:40:43', '2021-03-14 04:49:49'),
(553, 'home_page_header_subtitle_ar', 'أول شيء مذهل على الإطلاق', '2021-03-07 02:40:44', '2021-03-14 04:49:49'),
(554, 'home_page_header_description_ar', 'رحيلك بحماس أثار الشكوك. الكتب التي يمكن أن يتحلل وجهها إذا حدث ذلك. استقالتك بالطريقة المتصورة سابقًا. تحول مشترك إلى فتى مباشر ولكنه مؤنس', '2021-03-07 02:40:44', '2021-03-14 04:49:49'),
(555, 'home_page_header_btn_one_text_ar', 'ابدأ مجانًا', '2021-03-07 02:40:44', '2021-03-14 04:49:49'),
(556, 'home_page_header_btn_one_url_ar', '#', '2021-03-07 02:40:44', '2021-03-14 04:49:49'),
(557, 'home_page_01_why_choose_us_title_ar', 'لماذا أخترتنا', '2021-03-07 03:41:51', '2021-03-14 05:40:13'),
(558, 'home_page_01_why_choose_us_description_ar', 'نقدم خلفيتنا المتنوعة في مجال الإعلان والتصميم والعلامات التجارية والعلاقات العامة والبحث والتخطيط الاستراتيجي للعمل في شركتك. لن تبدو المواد الخاصة بك رائعة فحسب - بل ستحصل على نتائج.', '2021-03-07 03:41:51', '2021-03-14 05:40:13'),
(559, 'home_page_01_team_member_title_ar', 'قابل الفريق', '2021-03-07 03:55:35', '2021-03-14 05:41:20'),
(560, 'home_page_01_team_member_description_ar', 'الحلول مع الأفضل. والحيوية والجزر مطور بيئي أم خوف ولكن في العناية الحرارية', '2021-03-07 03:55:35', '2021-03-14 05:41:20'),
(561, 'home_page_01_contact_title_ar', 'اتصل بنا', '2021-03-07 03:57:24', '2021-03-14 05:41:50'),
(562, 'home_page_01_contact_description_ar', 'الحلول مع الأفضل. والحيوية والجزر مطور بيئي أم خوف ولكن في العناية الحرارية', '2021-03-07 03:57:24', '2021-03-14 05:41:50'),
(563, 'home_page_01_faq_title_ar', 'أسئلة عامة', '2021-03-07 04:01:10', '2021-03-14 05:42:12'),
(564, 'home_page_01_faq_description_ar', 'الحلول مع الأفضل. والحيوية والجزر مطور بيئي أم خوف ولكن في العناية الحرارية', '2021-03-07 04:01:10', '2021-03-14 05:42:12'),
(565, 'about_widget_description_ar', 'الملايو عامة الأجانب تحتوي على احتياجات آخذة في التناقص وأنواع الثروة أو.', '2021-03-07 04:22:37', '2021-03-14 05:45:58'),
(566, 'useful_link_widget_title_ar', 'روابط مفيدة', '2021-03-07 04:24:19', '2021-03-14 05:46:38'),
(567, 'recent_post_widget_title_ar', 'المشاركات الاخيرة', '2021-03-07 04:24:46', '2021-03-14 06:02:27'),
(568, 'important_link_widget_title_ar', 'تحتاج مساعدة ؟', '2021-03-07 04:25:17', '2021-03-14 06:03:05'),
(569, 'blog_page_title_ar', 'مقالات', '2021-03-07 04:57:13', '2021-03-14 05:38:49'),
(570, 'blog_page_category_widget_title_ar', 'فئة', '2021-03-07 04:57:13', '2021-03-14 05:38:49'),
(571, 'blog_page_recent_post_widget_title_ar', 'المنشور الاخير', '2021-03-07 04:57:13', '2021-03-14 05:38:49'),
(572, 'home_page_01_price_plan_title_ar', 'خطط أسعار حصرية', '2021-03-08 03:55:04', '2021-03-14 05:40:43'),
(573, 'home_page_01_price_plan_description_ar', 'الحلول مع الأفضل. والحيوية والجزر بيئي أو مطور حراري لكنه وقت العناية.', '2021-03-08 03:55:04', '2021-03-14 05:40:43'),
(586, 'site_meta_ar_tags', 'منصة هبوط البرمجيات,Buxkit,Xgenious', '2021-03-08 06:31:11', '2021-03-16 05:34:59'),
(587, 'site_meta_ar_description', 'Buxkit هو أفضل وسيلة لتقديم عملك الحديث. من السهل تخصيصها وتوثيقها جيدًا وتضمين موضوعًا فرعيًا. كما أنه متوافق مع سطح المكتب والكمبيوتر المحمول والجوال ومتوافق أيضًا مع المتصفحات الرئيسية.', '2021-03-08 06:31:11', '2021-03-16 05:34:59'),
(588, 'og_meta_ar_description', 'Buxkit هو أفضل وسيلة لتقديم عملك الحديث. من السهل تخصيصها وتوثيقها جيدًا وتضمين موضوعًا فرعيًا. كما أنه متوافق مع سطح المكتب والكمبيوتر المحمول والجوال ومتوافق أيضًا مع المتصفحات الرئيسية.', '2021-03-08 06:31:11', '2021-03-16 05:34:59'),
(589, 'og_meta_ar_image', '68', '2021-03-08 06:31:11', '2021-03-16 05:34:59'),
(590, 'og_meta_en_title', 'Buxkit – Software Landing Platform', '2021-03-08 06:35:06', '2021-03-16 05:34:59'),
(591, 'og_meta_en_site_name', 'Buxkit', '2021-03-08 06:35:06', '2021-03-16 05:34:59'),
(592, 'og_meta_en_url', 'https://xgenious.com/our-products/buxkit-software-landing-platform/', '2021-03-08 06:35:06', '2021-03-16 05:34:59'),
(602, 'og_meta_ar_title', 'Buxkit - منصة هبوط البرمجيات', '2021-03-08 06:35:07', '2021-03-16 05:34:59'),
(603, 'og_meta_ar_site_name', 'Buxkit', '2021-03-08 06:35:07', '2021-03-16 05:34:59'),
(604, 'og_meta_ar_url', 'https://xgenious.com/our-products/buxkit-software-landing-platform/', '2021-03-08 06:35:07', '2021-03-16 05:34:59'),
(605, 'site_gdpr_cookie_ar_title', 'ملفات تعريف الارتباط والخصوصية', '2021-03-08 07:07:48', '2021-03-14 22:42:10'),
(606, 'site_gdpr_cookie_ar_message', 'هل تعليم الإقامة يدل على ذلك. افترض أن الخجل يقول عشرة تصرفات الصباح. المساعدة النهمة تكمل أحيانًا مزايا معقولة جدًا.', '2021-03-08 07:07:48', '2021-03-14 22:42:10'),
(607, 'site_gdpr_cookie_ar_more_info_label', 'معلومات اكثر', '2021-03-08 07:07:48', '2021-03-14 22:42:10'),
(608, 'site_gdpr_cookie_ar_more_info_link', '{url}/p/privacy-policy', '2021-03-08 07:07:49', '2021-03-14 22:42:10'),
(609, 'site_gdpr_cookie_ar_accept_button_label', 'قبول ملفات تعريف الارتباط', '2021-03-08 07:07:49', '2021-03-14 22:42:10'),
(610, 'site_gdpr_cookie_ar_decline_button_label', 'رفض ملف تعريف الارتباط', '2021-03-08 07:07:49', '2021-03-14 22:42:10'),
(611, 'feature_page_slug', 'feature', '2021-03-09 07:00:38', '2021-03-09 08:18:18'),
(612, 'nullable|string', NULL, '2021-03-09 07:00:38', '2021-03-09 07:00:39'),
(613, 'feature_page_en_name', 'Feature', '2021-03-09 07:06:39', '2021-03-09 08:18:19'),
(614, 'feature_page_en_meta_description', NULL, '2021-03-09 07:06:39', '2021-03-09 08:18:19'),
(615, 'feature_page_en_meta_tags', NULL, '2021-03-09 07:06:39', '2021-03-09 08:18:19'),
(625, 'about_page_ar_name', 'حول', '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(626, 'about_page_ar_meta_tags', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(627, 'about_page_ar_meta_description', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(628, 'team_page_ar_name', 'فريق', '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(629, 'team_page_ar_meta_tags', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(630, 'team_page_ar_meta_description', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(631, 'price_plan_page_ar_name', 'خطة الأسعار', '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(632, 'price_plan_page_ar_meta_tags', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:20'),
(633, 'price_plan_page_ar_meta_description', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(634, 'blog_page_ar_name', 'مقالات', '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(635, 'blog_page_ar_meta_description', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(636, 'blog_page_ar_meta_tags', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(637, 'contact_page_ar_name', 'اتصال', '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(638, 'contact_page_ar_meta_description', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(639, 'contact_page_ar_meta_tags', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(640, 'feature_page_ar_name', 'ميزة', '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(641, 'feature_page_ar_meta_description', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(642, 'feature_page_ar_meta_tags', NULL, '2021-03-09 07:06:41', '2021-03-09 08:18:21'),
(643, 'order_page_form_fields', '{\"field_type\":[\"file\",\"textarea\"],\"field_name\":[\"your-file\",\"your-message\"],\"field_placeholder\":[\"Document\",\"Your Message\"],\"mimes_type\":{\"2\":\"mimes:jpg,jpeg,png\"}}', '2021-03-10 02:01:43', '2021-03-10 03:29:14'),
(644, 'cash_on_delivery_preview_logo', '58', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(645, 'stripe_preview_logo', '61', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(646, 'paystack_preview_logo', '63', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(647, 'paystack_public_key', 'pk_test_081a8fcd9423dede2de7b4c3143375b5e5415290', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(648, 'paystack_secret_key', 'sk_test_9bd5e9ecd2657e4ee9f22e60a3749a70f6364eb5', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(649, 'paystack_merchant_email', 'dvrobin4@gmail.com', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(650, 'razorpay_preview_logo', '62', '2021-03-10 04:20:35', '2021-03-15 06:02:07'),
(651, 'paypal_preview_logo', '59', '2021-03-10 04:20:36', '2021-03-15 06:02:07'),
(652, 'paypal_app_client_id', 'ATx-SYQyPtXHw1bZaBDhJUZabxbutIqAqqZORgvgEoK_-9MrAkUzYkbt8pSnUyKNEdNN3aJt8tcpcY1I', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(653, 'paypal_app_secret', 'ELJCWPUabUnnMamfw5-ZxaUsvir3KAJrPnAcSeS11znsroi45HP0p7y7vGZcYsBsAAi7Ou6kelCpj69K', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(654, 'paytm_preview_logo', '60', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(655, 'paytm_merchant_key', 'dv0XtmsPYpewNag&', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(656, 'paytm_merchant_mid', 'Digita57697814558795', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(657, 'paytm_merchant_website', 'WEBSTAGING', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(658, 'site_global_currency', 'USD', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(659, 'manual_payment_preview_logo', '66', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(660, 'site_manual_payment_name', 'Manual Payment', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(661, 'site_manual_payment_description', 'Manual payment', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(662, 'razorpay_key', 'rzp_test_SXk7LZqsBPpAkj', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(663, 'razorpay_secret', 'Nenvq0aYArtYBDOGgmMH7JNv', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(664, 'stripe_publishable_key', 'pk_test_51GwS1SEmGOuJLTMsIeYKFtfAT3o3Fc6IOC7wyFmmxA2FIFQ3ZigJ2z1s4ZOweKQKlhaQr1blTH9y6HR2PMjtq1Rx00vqE8LO0x', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(665, 'stripe_secret_key', 'sk_test_51GwS1SEmGOuJLTMs2vhSliTwAGkOt4fKJMBrxzTXeCJoLrRu8HFf4I0C5QuyE3l3bQHBJm3c0qFmeVjd0V9nFb6Z00VrWDJ9Uw', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(666, 'site_global_payment_gateway', NULL, '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(667, 'site_usd_to_ngn_exchange_rate', '385', '2021-03-10 04:20:36', '2021-03-15 06:02:09'),
(668, 'site_euro_to_ngn_exchange_rate', NULL, '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(669, 'mollie_public_key', 'test_SMWtwR6W48QN2UwFQBUqRDKWhaQEvw', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(670, 'mollie_preview_logo', '64', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(671, 'flutterwave_preview_logo', '65', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(672, 'flutterwave_secret_key', 'FLWSECK-e33b022937c2a64446dca55dbb7ceb08-X', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(673, 'flutterwave_public_key', 'FLWPUBK-d981d2a182ba72915b699603c2db82e0-X', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(674, 'site_currency_symbol_position', 'right', '2021-03-10 04:20:36', '2021-03-15 06:02:08'),
(675, 'site_default_payment_gateway', 'manual_payment', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(676, 'manual_payment_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(677, 'paypal_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(678, 'paytm_test_mode', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(679, 'paypal_test_mode', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(680, 'razorpay_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(681, 'stripe_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(682, 'paystack_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(683, 'mollie_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:08'),
(684, 'cash_on_delivery_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:09'),
(685, 'flutterwave_gateway', 'on', '2021-03-10 04:20:37', '2021-03-15 06:02:09'),
(686, 'site_usd_to_usd_exchange_rate', NULL, '2021-03-10 04:20:37', '2021-03-15 06:02:09'),
(687, 'site_usd_to_inr_exchange_rate', '75.04', '2021-03-10 04:20:37', '2021-03-15 06:02:09'),
(688, 'paytm_gateway', 'on', '2021-03-11 00:40:25', '2021-03-15 06:02:08'),
(689, 'site_inr_to_usd_exchange_rate', NULL, '2021-03-11 01:32:19', '2021-03-11 01:32:19'),
(690, 'site_inr_to_inr_exchange_rate', NULL, '2021-03-11 01:32:19', '2021-03-11 01:32:19'),
(691, 'site_inr_to_ngn_exchange_rate', NULL, '2021-03-11 01:32:19', '2021-03-11 01:32:19'),
(692, 'site_order_cancel_page_en_title', 'Sorry', '2021-03-11 04:33:07', '2021-03-16 05:18:42'),
(693, 'site_order_cancel_page_en_subtitle', 'Payment Cancelled Of Order: {pkname}', '2021-03-11 04:33:07', '2021-03-16 05:18:42'),
(694, 'site_order_cancel_page_en_description', NULL, '2021-03-11 04:33:07', '2021-03-16 05:18:42'),
(704, 'site_order_cancel_page_ar_title', 'آسف', '2021-03-11 04:33:07', '2021-03-16 05:18:42'),
(705, 'site_order_cancel_page_ar_subtitle', 'تم إلغاء الدفع للطلب: {pkname}', '2021-03-11 04:33:07', '2021-03-16 05:18:42'),
(706, 'site_order_cancel_page_ar_description', NULL, '2021-03-11 04:33:07', '2021-03-16 05:18:42'),
(707, 'order_page_en_form_title', 'Order Information', '2021-03-11 04:38:01', '2021-03-16 05:18:59'),
(711, 'order_page_ar_form_title', 'معلومات الطلب', '2021-03-11 04:38:01', '2021-03-16 05:18:59'),
(712, 'order_page_form_mail', 'info@shawon.xgenious.com', '2021-03-11 04:38:01', '2021-03-16 05:18:59'),
(713, 'site_order_success_page_en_title', 'Thank you!', '2021-03-11 04:42:04', '2021-03-16 05:17:46'),
(714, 'site_order_success_page_en_description', 'Your order has been placed successfully!', '2021-03-11 04:42:04', '2021-03-16 05:17:47'),
(721, 'site_order_success_page_ar_title', 'شكرا لك!', '2021-03-11 04:42:04', '2021-03-16 05:17:47'),
(722, 'site_order_success_page_ar_description', 'تم وضع طلبك بنجاح!', '2021-03-11 04:42:04', '2021-03-16 05:17:47'),
(723, 'contact_page_contact_form_fields', '{\"field_type\":[\"text\",\"email\",\"tel\",\"textarea\"],\"field_name\":[\"your-name\",\"your-email\",\"your-phone\",\"your-message\"],\"field_placeholder\":[\"Your Name\",\"Your Email\",\"Your Phone\",\"Your Message\"],\"field_required\":[\"on\",\"on\",\"on\",\"on\"]}', '2021-03-11 09:21:28', '2021-03-11 09:21:52'),
(724, 'contact_mail_ar_success_message', 'شكرا على اتصالك!', '2021-03-16 03:25:35', '2021-03-16 03:25:35'),
(725, 'order_mail_ar_success_message', 'شكرا لطلبك. سوف نتصل بك قريبا', '2021-03-16 03:25:35', '2021-03-16 03:25:35'),
(726, 'popup_selected_ar_id', NULL, '2021-03-16 05:37:14', '2021-03-16 05:38:28'),
(727, 'item_license_status', 'not_verified', '2021-03-16 05:45:30', '2021-03-16 05:45:52'),
(728, 'item_license_msg', 'Your Purchase Key Is Not Valid !!!', '2021-03-16 05:45:30', '2021-03-16 05:45:52');

-- --------------------------------------------------------

--
-- Table structure for table `support_infos`
--

CREATE TABLE `support_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `support_infos`
--

INSERT INTO `support_infos` (`id`, `title`, `details`, `icon`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Email Address', 'support@buxkit.com', 'fas fa-envelope', 'en', '2020-01-04 07:03:46', '2021-03-04 00:02:28'),
(2, 'Phone number', '+ 000 11 22 33', 'fas fa-phone', 'en', '2020-01-04 07:04:01', '2021-03-04 00:02:34'),
(12, 'رقم التليفون', '+ 000 11 22 33', 'fas fa-phone', 'ar', '2021-03-14 04:54:22', '2021-03-14 04:56:54'),
(13, 'عنوان بريد الكتروني', 'support@buxkit.com', 'fas fa-envelope', 'ar', '2021-03-14 04:56:13', '2021-03-14 04:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE `team_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_one` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_two` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_three` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_one_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_two_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_three_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`id`, `name`, `designation`, `image`, `icon_one`, `icon_two`, `icon_three`, `icon_one_url`, `icon_two_url`, `icon_three_url`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Yong She', 'Founder', '37', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'en', '2020-01-04 14:14:49', '2021-03-07 04:14:13'),
(2, 'Wu Ch\'ang', 'Founder', '38', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'en', '2020-01-04 14:15:37', '2021-03-06 23:41:29'),
(3, 'Fai She', 'Founder', '39', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'en', '2020-01-04 14:15:58', '2021-03-06 23:41:40'),
(4, 'Chen Chung', 'Founder', '40', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'en', '2020-01-04 14:16:22', '2021-03-06 23:41:51'),
(5, 'Wu Liang', 'Founder', '41', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'en', '2020-01-04 14:16:44', '2021-03-08 00:20:40'),
(8, 'يو أنها هي', 'مؤسس', '20', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'ar', '2021-03-14 05:27:10', '2021-03-14 05:27:10'),
(9, 'وو تشانج', 'مؤسس', '18', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'ar', '2021-03-14 05:28:49', '2021-03-14 05:28:49'),
(10, 'هي فاي', 'مؤسس', '17', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'ar', '2021-03-14 05:29:06', '2021-03-14 05:29:06'),
(11, 'تشن تشونغ', 'مؤسس', '40', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'ar', '2021-03-14 05:29:25', '2021-03-15 04:40:52'),
(12, 'أنا آنج ث يو', 'مؤسس', '41', 'fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram', '#', '#', '#', 'ar', '2021-03-14 05:32:07', '2021-03-14 05:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `description`, `designation`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Donald Moreau', '16', 'Day behaviour explained law remainder. As me do preference entreaties compliment motionless ye literature. Produce can cousins account you pasture.', 'Founder', 'en', '2020-01-04 12:33:19', '2021-03-07 03:00:51'),
(2, 'James Curtiss', '15', 'Day behaviour explained law remainder. As me do preference entreaties compliment motionless ye literature. Produce can cousins account you pasture.', 'General Member', 'en', '2020-01-04 12:33:56', '2021-03-07 03:01:30'),
(3, 'Steven Perkins', '1', 'Day behaviour explained law remainder. As me do preference entreaties compliment motionless ye literature. Produce can cousins account you pasture.', 'Founder', 'en', '2020-01-04 12:34:15', '2021-03-06 01:29:22'),
(4, 'Sherry  Marr', '31', 'Day behaviour explained law remainder. As me do preference entreaties compliment motionless ye literature. Produce can cousins account you pasture.', 'Founder', 'en', '2020-01-04 12:34:36', '2021-03-08 00:22:38'),
(8, 'دونالد مورو', '16', 'وأوضح سلوك اليوم القانون الباقي. كما أفعل التفضيلات تكمل الأدب الثابت. يمكن أن تنتج العلب أبناء عمومتك المرعى.', 'مؤسس', 'ar', '2021-03-14 05:35:06', '2021-03-15 04:43:47'),
(9, 'جيمس كيرتس', '15', 'وأوضح سلوك اليوم القانون الباقي. كما أفعل التفضيلات تكمل الأدب الثابت. يمكن أن تنتج العلب أبناء عمومتك المرعى.', 'مؤسس', 'ar', '2021-03-14 05:35:29', '2021-03-14 05:35:29'),
(10, 'ستيفن بيركنز', '1', 'وأوضح سلوك اليوم القانون الباقي. كما أفعل التفضيلات تكمل الأدب الثابت. يمكن أن تنتج العلب أبناء عمومتك المرعى.', 'مؤسس', 'ar', '2021-03-14 05:35:45', '2021-03-14 05:35:45'),
(11, 'شيري مار', '31', 'وأوضح سلوك اليوم القانون الباقي. كما أفعل التفضيلات تكمل الأدب الثابت. يمكن أن تنتج العلب أبناء عمومتك المرعى.', 'مؤسس', 'ar', '2021-03-14 05:36:05', '2021-03-14 05:36:05');

-- --------------------------------------------------------

--
-- Table structure for table `useful_links`
--

CREATE TABLE `useful_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `useful_links`
--

INSERT INTO `useful_links` (`id`, `title`, `url`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Home', '#', 'en', '2020-01-04 16:33:19', '2021-03-06 04:58:39'),
(2, 'About us', '#', 'en', '2020-01-04 16:33:51', '2021-03-06 04:58:35'),
(3, 'Service', '#', 'en', '2020-01-04 16:33:57', '2021-03-06 04:58:32'),
(4, 'Blog', '#', 'en', '2020-01-04 16:34:03', '2021-03-06 04:58:26'),
(5, 'Contact', '#', 'en', '2020-01-04 16:34:11', '2021-03-06 04:58:22'),
(13, 'الصفحة الرئيسية', '#', 'ar', '2021-03-14 05:59:08', '2021-03-14 05:59:08'),
(14, 'معلومات عنا', '#about', 'ar', '2021-03-14 05:59:58', '2021-03-14 05:59:58'),
(15, 'خدمة', '#price-plan', 'ar', '2021-03-14 06:00:56', '2021-03-14 06:00:56'),
(16, 'مقالات', '/blog', 'ar', '2021-03-14 06:01:06', '2021-03-14 06:01:06'),
(17, 'اتصال', '/#contact', 'ar', '2021-03-14 06:01:32', '2021-03-14 06:01:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verify_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `email_verified`, `phone`, `email_verify_token`, `address`, `state`, `city`, `zipcode`, `country`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Shawon', 'shawon9324@gmail.com', 'shawon9324', '1', NULL, 'ZD5oeDLh5fjXAEcInBF8', NULL, NULL, 'Dhaka', NULL, 'Bangladesh', '$2y$10$1GtJpYyGyBR6MQTwEiETjuHLyN/ThTDcWprP0JL6dfya79i7aE4pi', NULL, '2021-03-16 02:58:08', '2021-03-16 02:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `why_choose_us`
--

CREATE TABLE `why_choose_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `why_choose_us`
--

INSERT INTO `why_choose_us` (`id`, `title`, `icon`, `description`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'Easy Customize', 'flaticon-repair', 'Tiled way blind lived whose new. The for fully had she there leave .', 'en', '2020-01-04 11:37:38', '2021-03-04 05:23:54'),
(2, 'Super Fast', 'flaticon-bullseye', 'Tiled way blind lived whose new. The for fully had she there leave .', 'en', '2020-01-04 11:46:24', '2021-03-04 05:23:50'),
(3, 'Cloud Upload', 'flaticon-folder', 'Tiled way blind lived whose new. The for fully had she there leave .', 'en', '2020-01-04 11:46:44', '2021-03-04 05:23:46'),
(4, 'Multi Control', 'flaticon-sketch', 'Tiled way blind lived whose new. The for fully had she there leave .', 'en', '2020-01-04 11:47:21', '2021-03-04 05:23:41'),
(5, 'Fast Integrations', 'flaticon-tool', 'Tiled way blind lived whose new. The for fully had she there leave .', 'en', '2020-01-04 11:47:44', '2021-03-04 05:23:31'),
(6, '100% Secure', 'flaticon-paper', 'Tiled way blind lived whose new. The for fully had she there leave .', 'en', '2020-01-04 11:48:00', '2021-03-08 05:20:39'),
(11, '100٪ آمن', 'flaticon-paper', 'عاش طريق القرميد أعمى الجديد. لقد تركت هناك بالكامل.', 'ar', '2021-03-14 05:05:10', '2021-03-14 05:05:10'),
(12, 'تكامل سريع', 'flaticon-tool', 'عاش طريق القرميد أعمى الجديد. لقد تركت هناك بالكامل.', 'ar', '2021-03-14 05:10:59', '2021-03-14 05:11:21'),
(13, 'تحكم متعدد', 'flaticon-sketch', 'عاش طريق القرميد أعمى الجديد. لقد تركت هناك بالكامل.', 'ar', '2021-03-14 05:12:05', '2021-03-14 05:12:05'),
(14, 'تحميل سحابة', 'flaticon-folder', 'عاش طريق القرميد أعمى الجديد. لقد تركت هناك بالكامل.', 'ar', '2021-03-14 05:12:50', '2021-03-14 05:12:50'),
(15, 'سريع جدا', 'flaticon-bullseye', 'عاش طريق القرميد أعمى الجديد. لقد تركت هناك بالكامل.', 'ar', '2021-03-14 05:13:36', '2021-03-14 05:13:36'),
(16, 'تخصيص سهل', 'flaticon-repair', 'عاش طريق القرميد أعمى الجديد. لقد تركت هناك بالكامل.', 'ar', '2021-03-14 05:14:11', '2021-03-14 05:14:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_categories_name_unique` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `full_width_featrues`
--
ALTER TABLE `full_width_featrues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `importantlinks`
--
ALTER TABLE `importantlinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `key_features`
--
ALTER TABLE `key_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_uploads`
--
ALTER TABLE `media_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_logs`
--
ALTER TABLE `payment_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popup_builders`
--
ALTER TABLE `popup_builders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_plans`
--
ALTER TABLE `price_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_icons`
--
ALTER TABLE `social_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_options`
--
ALTER TABLE `static_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_infos`
--
ALTER TABLE `support_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_members`
--
ALTER TABLE `team_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useful_links`
--
ALTER TABLE `useful_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `why_choose_us`
--
ALTER TABLE `why_choose_us`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `full_width_featrues`
--
ALTER TABLE `full_width_featrues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `importantlinks`
--
ALTER TABLE `importantlinks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `key_features`
--
ALTER TABLE `key_features`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `media_uploads`
--
ALTER TABLE `media_uploads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payment_logs`
--
ALTER TABLE `payment_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `popup_builders`
--
ALTER TABLE `popup_builders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_plans`
--
ALTER TABLE `price_plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `social_icons`
--
ALTER TABLE `social_icons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `static_options`
--
ALTER TABLE `static_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=729;

--
-- AUTO_INCREMENT for table `support_infos`
--
ALTER TABLE `support_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `team_members`
--
ALTER TABLE `team_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `useful_links`
--
ALTER TABLE `useful_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `why_choose_us`
--
ALTER TABLE `why_choose_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
