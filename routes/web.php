<?php
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' =>[ 'setlang','globalVariable']],function (){
    $blog_page_slug = get_static_option('blog_page_slug') ?? 'blog';
    $feature_page_slug = get_static_option('feature_page_slug') ?? 'feature';
    $feature_page_slug = get_static_option('feature_page_slug') ?? 'price-plan';
    $price_plan_page_slug = get_static_option('price_plan_page_slug') ?? 'price-plan';
    //frontend routes
    Route::get('/','FrontendController@index')->name('homepage');
    Route::get('/p/{any}','FrontendController@dynamic_single_page')->name('frontend.dynamic.page');
    Route::get('/'.$blog_page_slug,'FrontendController@blog_page')->name('frontend.blog');
    Route::get('/'.$blog_page_slug.'/{id}/{any}','FrontendController@blog_single_page')->name('frontend.blog.single');
    Route::get('/'.$blog_page_slug.'/search','FrontendController@blog_search_page')->name('frontend.blog.search');
    Route::get('/'.$blog_page_slug.'/category/{id}/{any}','FrontendController@category_wise_blog_page')->name('frontend.blog.category');
    Route::get('/'.$feature_page_slug.'/{id}/{any}','FrontendController@full_width_feature_details')->name('frontend.full.feature.details');
    Route::post('/place-order','FrontendController@send_order_message')->name('frontend.order.message');
    Route::get('/email-test','FrontendController@email_test')->name('frontend.email.test');
    //product invoice for user
    Route::post('/package-user/generate-invoice','FrontendController@generate_package_invoice')->name('frontend.package.invoice.generate');
    Route::get('/order-details/{id}','FrontendController@order_details')->name('frontend.order.view');
    //payment status route
    Route::get('/order-success/{id}','FrontendController@order_payment_success')->name('frontend.order.payment.success');
    Route::get('/order-cancel/{id}','FrontendController@order_payment_cancel')->name('frontend.order.payment.cancel');
    Route::get('/order-confirm/{id}','FrontendController@order_confirm')->name('frontend.order.confirm');
    Route::post('/order-confirm','PaymentLogController@order_payment_form')->name('frontend.order.payment.form');
    //contact-message
    Route::post('/contact-message','FrontendFormController@send_contact_message')->name('frontend.contact.message');
    Route::post('/place-order','FrontendFormController@send_order_message')->name('frontend.order.message');
    //ipn route
    Route::get('/paypal-ipn','PaymentLogController@paypal_ipn')->name('frontend.paypal.ipn');
    Route::post('/paytm-ipn','PaymentLogController@paytm_ipn')->name('frontend.paytm.ipn');
    Route::post('/stripe','PaymentLogController@stripe_ipn')->name('frontend.stripe.ipn');
    Route::get('/stripe-pay','PaymentLogController@stripe_success')->name('frontend.stripe.success');
    Route::post('/razorpay','PaymentLogController@razorpay_ipn')->name('frontend.razorpay.ipn');
    Route::post('/paystack/pay','PaymentLogController@paystack_pay')->name('frontend.paystack.pay');
    Route::get('/paystack/callback','PaymentLogController@paystack_callback')->name('frontend.paystack.callback');
    Route::get('/flutterwave/pay','FrontendController@flutterwave_pay_get')->name('frontend.flutterwave.pay');
    Route::post('/flutterwave/pay','PaymentLogController@flutterwave_pay');
    Route::get('/flutterwave/callback','PaymentLogController@flutterwave_callback')->name('frontend.flutterwave.callback');
    Route::get('/mollie/callback','PaymentLogController@mollie_webhook')->name('frontend.mollie.webhook');
    //place order form
    Route::get('/'.$price_plan_page_slug,'FrontendController@price_plan_page')->name('frontend.price.plan');
    Route::get('/'.$price_plan_page_slug.'/{id}','FrontendController@plan_order')->name('frontend.plan.order');
    //user dashboard
    Route::prefix('user-home')->middleware(['userEmailVerify','setlang','globalVariable'])->group(function (){
        Route::get('/', 'UserDashboardController@user_index')->name('user.home');
        Route::get('/download/file/{id}', 'UserDashboardController@download_file')->name('user.dashboard.download.file');
        Route::post('/profile-update','UserDashboardController@user_profile_update')->name('user.profile.update');
        Route::post('/password-change','UserDashboardController@user_password_change')->name('user.password.change');
        Route::post('/package-order/cancel','UserDashboardController@package_order_cancel')->name('user.dashboard.package.order.cancel');
        Route::post('/product-order/cancel','UserDashboardController@product_order_cancel')->name('user.dashboard.product.order.cancel');
        Route::post('/event-order/cancel','UserDashboardController@event_order_cancel')->name('user.dashboard.event.order.cancel');
        Route::post('/donation-order/cancel','UserDashboardController@donation_order_cancel')->name('user.dashboard.donation.order.cancel');
        Route::get('/product-order/view/{id}','UserDashboardController@product_order_view')->name('user.dashboard.product.order.view');
    });
    //user login
    Route::get('/login','Auth\LoginController@showLoginForm')->name('user.login');
    Route::post('/ajax-login','FrontendController@ajax_login')->name('user.ajax.login');
    Route::post('/login','Auth\LoginController@login');
    Route::get('/register','Auth\RegisterController@showRegistrationForm')->name('user.register');
    Route::post('/register','Auth\RegisterController@register');
    Route::get('/login/forget-password','FrontendController@showUserForgetPasswordForm')->name('user.forget.password');
    Route::get('/login/reset-password/{user}/{token}','FrontendController@showUserResetPasswordForm')->name('user.reset.password');
    Route::post('/login/reset-password','FrontendController@UserResetPassword')->name('user.reset.password.change');
    Route::post('/login/forget-password','FrontendController@sendUserForgetPasswordMail');
    Route::post('/logout','Auth\LoginController@logout')->name('user.logout');
    //user email verify
    Route::get('/user/email-verify','UserDashboardController@user_email_verify_index')->name('user.email.verify');
    Route::get('/user/resend-verify-code','UserDashboardController@reset_user_email_verify_code')->name('user.resend.verify.mail');
    Route::post('/user/email-verify','UserDashboardController@user_email_verify');
    //language change
Route::get('/lang','FrontendController@lang_change')->name('frontend.langchange');
Route::get('/home/{id}','FrontendController@home_page_change');

});

//admin login
Route::get('/login/admin','Auth\LoginController@showAdminLoginForm')->name('admin.login');
Route::get('/login/admin/forget-password','FrontendController@showAdminForgetPasswordForm')->name('admin.forget.password');
Route::get('/login/admin/reset-password/{user}/{token}','FrontendController@showAdminResetPasswordForm')->name('admin.reset.password');
Route::post('/login/admin/reset-password','FrontendController@AdminResetPassword')->name('admin.reset.password.change');
Route::post('/login/admin/forget-password','FrontendController@sendAdminForgetPasswordMail');
Route::post('/logout/admin','AdminDashboardController@adminLogout')->name('admin.logout');
Route::post('/login/admin','Auth\LoginController@adminLogin');
//admin dashboard routes
Route::prefix('admin-home')->group(function (){
    //general settings
    Route::prefix('general-settings')->group(function (){
        //site-identity
        Route::get('/site-identity','GeneralSettingsController@site_identity')->name('admin.general.site.identity');
        Route::post('/site-identity','GeneralSettingsController@update_site_identity');
        //basic-settings
        Route::get('/basic-settings','GeneralSettingsController@basic_settings')->name('admin.general.basic.settings');
        Route::post('/basic-settings','GeneralSettingsController@update_basic_settings');
        //seo settings
        Route::get('/seo-settings','GeneralSettingsController@seo_settings')->name('admin.general.seo.settings');
        Route::post('/seo-settings','GeneralSettingsController@update_seo_settings');
        //scripts settings
        Route::get('/scripts','GeneralSettingsController@scripts_settings')->name('admin.general.scripts.settings');
        Route::post('/scripts','GeneralSettingsController@update_scripts_settings');
        //email template settings
        Route::get('/email-template','GeneralSettingsController@email_template_settings')->name('admin.general.email.template');
        Route::post('/email-template','GeneralSettingsController@update_email_template_settings');
        //email settings
        Route::get('/email-settings','GeneralSettingsController@email_settings')->name('admin.general.email.settings');
        Route::post('/email-settings','GeneralSettingsController@update_email_settings');
        //typography settings
        Route::get('/typography-settings','GeneralSettingsController@typography_settings')->name('admin.general.typography.settings');
        Route::post('/typography-settings','GeneralSettingsController@update_typography_settings');
        Route::post('typography-settings/single','GeneralSettingsController@get_single_font_variant')->name('admin.general.typography.single');
        //smtp settings
        Route::get('/smtp-settings','GeneralSettingsController@smtp_settings')->name('admin.general.smtp.settings');
        Route::post('/smtp-settings','GeneralSettingsController@update_smtp_settings');
        //page settings
        Route::get('/page-settings','GeneralSettingsController@page_settings')->name('admin.general.page.settings');
        Route::post('/page-settings','GeneralSettingsController@update_page_settings');
        //payment gateway
        Route::get('/payment-settings','GeneralSettingsController@payment_settings')->name('admin.general.payment.settings');
        Route::post('/payment-settings','GeneralSettingsController@update_payment_settings');
        //custom css
        Route::get('/custom-css','GeneralSettingsController@custom_css_settings')->name('admin.general.custom.css');
        Route::post('/custom-css','GeneralSettingsController@update_custom_css_settings');
        //custom js
        Route::get('/custom-js','GeneralSettingsController@custom_js_settings')->name('admin.general.custom.js');
        Route::post('/custom-js','GeneralSettingsController@update_custom_js_settings');
        //gdpr-settings
        Route::get('/gdpr-settings','GeneralSettingsController@gdpr_settings')->name('admin.general.gdpr.settings');
        Route::post('/gdpr-settings','GeneralSettingsController@update_gdpr_cookie_settings');
        //sitemap
        Route::get('/sitemap-settings','GeneralSettingsController@sitemap_settings')->name('admin.general.sitemap.settings');
        Route::post('/sitemap-settings','GeneralSettingsController@update_sitemap_settings');
        Route::post('/sitemap-settings/delete','GeneralSettingsController@delete_sitemap_settings')->name('admin.general.sitemap.settings.delete');
        //license-setting
        Route::get('/license-setting','GeneralSettingsController@license_settings')->name('admin.general.license.settings');
        Route::post('/license-setting','GeneralSettingsController@update_license_settings');
        //popup-setting
        Route::get('/popup-settings','GeneralSettingsController@popup_settings')->name('admin.general.popup.settings');
        Route::post('/popup-settings','GeneralSettingsController@update_popup_settings');
        //new settings
        Route::get('/cache-settings','GeneralSettingsController@cache_settings')->name('admin.general.cache.settings');
        Route::post('/cache-settings','GeneralSettingsController@update_cache_settings');

    });
    Route::get('/', 'AdminDashboardController@adminIndex')->name('admin.home');
    //home page variant select
    Route::get('/home-variant',"AdminDashboardController@home_variant")->name('admin.home.variant');
    Route::post('/home-variant',"AdminDashboardController@update_home_variant");
    //navbar settings
    Route::get('/navbar-settings',"AdminDashboardController@navbar_settings")->name('admin.navbar.settings');
    Route::post('/navbar-settings',"AdminDashboardController@update_navbar_settings");
    //page settings
    Route::get('/contact','AdminDashboardController@contact')->name('admin.contact');
    Route::post('/contact','AdminDashboardController@update_contact');
    //blog page
    Route::get('/blog-page','AdminDashboardController@blog_page')->name('admin.blog.page');
    Route::post('/blog-page','AdminDashboardController@blog_page_update');
    //testimonial
    Route::get('/testimonial','TestimonialController@index')->name('admin.testimonial');
    Route::post('/testimonial','TestimonialController@store');
    Route::post('/update-testimonial','TestimonialController@update')->name('admin.testimonial.update');
    Route::post('/delete-testimonial/{id}','TestimonialController@delete')->name('admin.testimonial.delete');
    Route::post('/testimonial-bulk-action','TestimonialController@bulk_action')->name('admin.testimonial.bulk.action');
    //key features
    Route::get('/keyfeatures','KeyFeaturesController@index')->name('admin.keyfeatures');
    Route::post('/keyfeatures','KeyFeaturesController@store');
    Route::post('/update-keyfeatures','KeyFeaturesController@update')->name('admin.keyfeatures.update');
    Route::post('/delete-keyfeatures/{id}','KeyFeaturesController@delete')->name('admin.keyfeatures.delete');
    Route::post('/keyfeatures-bulk-action','KeyFeaturesController@bulk_action')->name('admin.keyfeatures.bulk.action');
    //why choose us
    Route::get('/why-choose','WhyChooseUsController@index')->name('admin.why.choose.us');
    Route::post('/why-choose','WhyChooseUsController@store');
    Route::post('/update-why-choose','WhyChooseUsController@update')->name('admin.why.choose.us.update');
    Route::post('/delete-why-choose/{id}','WhyChooseUsController@delete')->name('admin.why.choose.us.delete');
    Route::post('/why-choose-bulk-action','WhyChooseUsController@bulk_action')->name('admin.why.choose.bulk.action');
    //full width features
    Route::get('/full-width-features','FullWidthFeatruesController@index')->name('admin.full.width.features');
    Route::post('/full-width-features','FullWidthFeatruesController@store');
    Route::post('/update-full-width-features','FullWidthFeatruesController@update')->name('admin.full.width.features.update');
    Route::post('/delete-full-width-features/{id}','FullWidthFeatruesController@delete')->name('admin.full.width.features.delete');
    Route::post('/full-width-features-bulk-action','FullWidthFeatruesController@bulk_action')->name('admin.full.width.bulk.action');
    //faq
    Route::get('/faq','FaqController@index')->name('admin.faq');
    Route::post('/faq','FaqController@store');
    Route::post('/update-faq','FaqController@update')->name('admin.faq.update');
    Route::post('/delete-faq/{id}','FaqController@delete')->name('admin.faq.delete');
    Route::post('/faq-bulk-action','FaqController@bulk_action')->name('admin.faq.bulk.action');
    //price plan
    Route::get('/price-plan','PricePlanController@index')->name('admin.price.plan');
    Route::post('/price-plan','PricePlanController@store');
    Route::post('/update-price-plan','PricePlanController@update')->name('admin.price.plan.update');
    Route::post('/delete-price-plan/{id}','PricePlanController@delete')->name('admin.price.plan.delete');
    Route::post('/price-plan/bulk-action','PricePlanController@bulk_action')->name('admin.price.plan.bulk.action');
    //team member
    Route::get('/team-member','TeamMemberController@index')->name('admin.team.member');
    Route::post('/team-member','TeamMemberController@store');
    Route::post('/update-team-member','TeamMemberController@update')->name('admin.team.member.update');
    Route::post('/delete-team-member/{id}','TeamMemberController@delete')->name('admin.team.member.delete');
    Route::post('/team-member-bulk-action','TeamMemberController@bulk_action')->name('admin.team.member.bulk.action');
    //pages
    Route::get('/page','PagesController@index')->name('admin.page');
    Route::get('/new-page','PagesController@new_page')->name('admin.page.new');
    Route::post('/new-page','PagesController@store_new_page');
    Route::get('/page-edit/{id}','PagesController@edit_page')->name('admin.page.edit');
    Route::post('/page-update/{id}','PagesController@update_page')->name('admin.page.update');
    Route::post('/page-delete/{id}','PagesController@delete_page')->name('admin.page.delete');
    Route::post('/page-bulk-action','PagesController@bulk_action')->name('admin.page.bulk.action');
    //homepage
    Route::get('/home-page/header','HomePageController@header_area')->name('admin.home.header');
    Route::post('/home-page/header','HomePageController@header_area_update');
    Route::get('/home-page-01/intro-video','HomePageController@home_01_intro_video')->name('admin.homeone.intro.video');
    Route::post('/home-page-01/intro-video','HomePageController@home_01_update_intro_video');
    Route::get('/home-page-01/faq','HomePageController@home_01_faq')->name('admin.homeone.faq');
    Route::post('/home-page-01/faq','HomePageController@home_01_update_faq');
    Route::get('/home-page-01/price-plan','HomePageController@home_01_price_plan')->name('admin.homeone.price.plan');
    Route::post('/home-page-01/price-plan','HomePageController@home_01_update_price_plan');
    Route::get('/home-page-01/team-member','HomePageController@home_01_team_member')->name('admin.homeone.team.member');
    Route::post('/home-page-01/team-member','HomePageController@home_01_update_team_member');
    Route::get('/home-page-01/contact','HomePageController@home_01_contact')->name('admin.homeone.contact');
    Route::post('/home-page-01/contact','HomePageController@home_01_update_contact');
    Route::get('/home-page-01/key-features','HomePageController@home_01_key_features')->name('admin.homeone.key.features');
    Route::post('/home-page-01/key-features','HomePageController@home_01_update_key_features');
    Route::get('/home-page-01/why-choose','HomePageController@home_01_why_choose_us')->name('admin.homeone.why.choose.us');
    Route::post('/home-page-01/why-choose','HomePageController@home_01_update_why_choose_us');
    Route::get('/home-page-01/section-manage','HomePageController@home_01_section_manage')->name('admin.homeone.section.manage');
    Route::post('/home-page-01/section-manage','HomePageController@home_01_update_section_manage');
    //footer
    Route::get('/footer/general','FooterController@general_settings')->name('admin.footer.general');
    Route::post('/footer/general','FooterController@update_general_settings');
    Route::get('/footer/about','FooterController@about_widget')->name('admin.footer.about');
    Route::post('/footer/about','FooterController@update_about_widget');
    Route::get('/footer/useful-links','FooterController@useful_links_widget')->name('admin.footer.useful.link');
    Route::post('/footer/useful-links/widget','FooterController@update_widget_useful_links')->name('admin.footer.useful.link.widget');
    Route::post('/footer/useful-links','FooterController@new_useful_links_widget');
    Route::post('/footer/useful-links/update','FooterController@update_useful_links_widget')->name('admin.footer.useful.link.update');
    Route::post('/footer/useful-links/update/{delete}','FooterController@delete_useful_links_widget')->name('admin.footer.useful.link.delete');
    Route::post('/footer/useful-links/bulk-action','FooterController@useful_link_bulk_action')->name('admin.footer.useful.link.bulk.action');
    Route::get('/footer/recent-post','FooterController@recent_post_widget')->name('admin.footer.recent.post');
    Route::post('/footer/recent-post','FooterController@update_recent_post_widget');
    Route::get('/footer/important-links','FooterController@important_links_widget')->name('admin.footer.important.link');
    Route::post('/footer/important-links/widget','FooterController@update_widget_important_links')->name('admin.footer.important.link.widget');
    Route::post('/footer/important-links','FooterController@new_important_links_widget');
    Route::post('/footer/important-links/update','FooterController@update_important_links_widget')->name('admin.footer.important.link.update');
    Route::post('/footer/important-links/update/{delete}','FooterController@delete_important_links_widget')->name('admin.footer.important.link.delete');
    Route::post('/footer/important-links/bulk-action','FooterController@important_link_bulk_action')->name('admin.footer.important.link.bulk.action');
    //topbar area
    Route::get('/topbar','TopBarController@index')->name('admin.topbar');
    Route::post('/topbar/new-support-info','TopBarController@new_support_info')->name('admin.new.support.info');
    Route::post('/topbar/update-support-info','TopBarController@update_support_info')->name('admin.update.support.info');
    Route::post('/topbar/delete-support-info/{id}','TopBarController@delete_support_info')->name('admin.delete.support.info');
    Route::post('/topbar/new-social-item','TopBarController@new_social_item')->name('admin.new.social.item');
    Route::post('/topbar/update-social-item','TopBarController@update_social_item')->name('admin.update.social.item');
    Route::post('/topbar/delete-social-item/{id}','TopBarController@delete_social_item')->name('admin.delete.social.item');
    Route::post('/topbar/bulk-action','TopBarController@bulk_action')->name('admin.support.info.bulk.action');
    //blog
    Route::get('/blog','BlogController@index')->name('admin.blog');
    Route::get('/new-blog','BlogController@new_blog')->name('admin.blog.new');
    Route::post('/new-blog','BlogController@store_new_blog');
    Route::get('/blog-edit/{id}','BlogController@edit_blog')->name('admin.blog.edit');
    Route::post('/blog-update/{id}','BlogController@update_blog')->name('admin.blog.update');
    Route::post('/blog-delete/{id}','BlogController@delete_blog')->name('admin.blog.delete');
    Route::post('/blog-bulk-action','BlogController@bulk_action_blog')->name('admin.blog.bulk.action');
    Route::get('/blog-category','BlogController@category')->name('admin.blog.category');
    Route::post('/blog-category','BlogController@new_category');
    Route::post('/delete-blog-category/{id}','BlogController@delete_category')->name('admin.blog.category.delete');
    Route::post('/update-blog-category','BlogController@update_category')->name('admin.blog.category.update');
    Route::post('/blog-category-bulk-action','BlogController@bulk_action_blog_category')->name('admin.blog.category.bulk.action');
    //admin user role management
    Route::get('/new-user','UserRoleManageController@new_user')->name('admin.new.user');
    Route::post('/new-user','UserRoleManageController@new_user_add');
    Route::post('/user-update','UserRoleManageController@user_update')->name('admin.user.update');
    Route::post('/user-password-chnage','UserRoleManageController@user_password_change')->name('admin.user.password.change');
    Route::post('/delete-user/{id}','UserRoleManageController@new_user_delete')->name('admin.delete.user');
    Route::get('/all-user','UserRoleManageController@all_user')->name('admin.all.user');
    //user role management
    Route::get('/frontend/new-user','FrontendUserManageController@new_user')->name('admin.frontend.new.user');
    Route::post('/frontend/new-user','FrontendUserManageController@new_user_add');
    Route::post('/frontend/user-update','FrontendUserManageController@user_update')->name('admin.frontend.user.update');
    Route::post('/frontend/user-password-chnage','FrontendUserManageController@user_password_change')->name('admin.frontend.user.password.change');
    Route::post('/frontend/delete-user/{id}','FrontendUserManageController@new_user_delete')->name('admin.frontend.delete.user');
    Route::get('/frontend/all-user','FrontendUserManageController@all_user')->name('admin.all.frontend.user');
    Route::post('/frontend/all-user/bulk-action','FrontendUserManageController@bulk_action')->name('admin.all.frontend.user.bulk.action');
    Route::post('/frontend/all-user/email-status','FrontendUserManageController@email_status')->name('admin.all.frontend.user.email.status');
    //admin settings
    Route::get('/settings','AdminDashboardController@admin_settings')->name('admin.profile.settings');
    Route::get('/profile-update','AdminDashboardController@admin_profile')->name('admin.profile.update');
    Route::post('/profile-update','AdminDashboardController@admin_profile_update');
    Route::get('/password-change','AdminDashboardController@admin_password')->name('admin.password.change');
    Route::post('/password-change','AdminDashboardController@admin_password_chagne');
    //language
    Route::get('/languages','LanguageController@index')->name('admin.languages');
    Route::get('/languages/words/edit/{id}','LanguageController@edit_words')->name('admin.languages.words.edit');
    Route::post('/languages/words/new','LanguageController@add_new_words')->name('admin.languages.add.new.word');
    Route::post('/languages/words/update/{id}','LanguageController@update_words')->name('admin.languages.words.update');
    Route::post('/languages/new','LanguageController@store')->name('admin.languages.new');
    Route::post('/languages/update','LanguageController@update')->name('admin.languages.update');
    Route::post('/languages/delete/{id}','LanguageController@delete')->name('admin.languages.delete');
    Route::post('/languages/clone','LanguageController@clone_languages')->name('admin.languages.clone');
    Route::post('/languages/default/{id}','LanguageController@make_default')->name('admin.languages.default');
    //popup page
    Route::get('/popup-builder/all','GeneralSettingsController@all_popup')->name('admin.popup.builder.all');
    Route::get('/popup-builder/new','GeneralSettingsController@new_popup')->name('admin.popup.builder.new');
    Route::post('/popup-builder/new','GeneralSettingsController@store_popup');
    Route::get('/popup-builder/edit/{id}','GeneralSettingsController@edit_popup')->name('admin.popup.builder.edit');
    Route::post('/popup-builder/update/{id}','GeneralSettingsController@update_popup')->name('admin.popup.builder.update');
    Route::post('/popup-builder/delete/{id}','GeneralSettingsController@delete_popup')->name('admin.popup.builder.delete');
    Route::post('/popup-builder/clone/{id}','GeneralSettingsController@clone_popup')->name('admin.popup.builder.clone');
    Route::post('/popup-builder/bulk-action','GeneralSettingsController@bulk_action')->name('admin.popup.builder.bulk.action');
    // media upload routes end
    Route::post('/media-upload/all','MediaUploadController@all_upload_media_file')->name('admin.upload.media.file.all');
    Route::post('/media-upload','MediaUploadController@upload_media_file')->name('admin.upload.media.file');
    Route::get('/media-upload/page','MediaUploadController@all_upload_media_images_for_page')->name('admin.upload.media.images.page');
    Route::post('/media-upload/delete','MediaUploadController@delete_upload_media_file')->name('admin.upload.media.file.delete');
    Route::post('/media-upload/alt','MediaUploadController@alt_change_upload_media_file')->name('admin.upload.media.file.alt.change');
    //form builder route
    Route::get('/form-builder/order-form','FormBuilderController@order_form_index')->name('admin.form.builder.order');
    Route::post('/form-builder/order-form','FormBuilderController@update_order_form');
    Route::get('/form-builder/contact-form','FormBuilderController@contact_form_index')->name('admin.form.builder.contact');
    Route::post('/form-builder/contact-form','FormBuilderController@update_contact_form');
    //payment log route
    Route::get('/payment-logs','OrderManageController@all_payment_logs')->name('admin.payment.logs');
    Route::post('/payment-logs/delete/{id}','OrderManageController@payment_logs_delete')->name('admin.payment.delete');
    Route::post('/payment-logs/approve/{id}','OrderManageController@payment_logs_approve')->name('admin.payment.approve');
    Route::post('/payment-logs/bulk-action','OrderManageController@payment_log_bulk_action')->name('admin.payment.bulk.action');
    Route::get('/payment-logs/report','OrderManageController@payment_report')->name('admin.payment.report');
    //order manage route
    Route::prefix('package')->group(function (){
    Route::get('/order-manage/all','OrderManageController@all_orders')->name('admin.package.order.manage.all');
    Route::get('/order-manage/pending','OrderManageController@pending_orders')->name('admin.package.order.manage.pending');
    Route::get('/order-manage/completed','OrderManageController@completed_orders')->name('admin.package.order.manage.completed');
    Route::get('/order-manage/in-progress','OrderManageController@in_progress_orders')->name('admin.package.order.manage.in.progress');
    Route::post('/order-manage/change-status','OrderManageController@change_status')->name('admin.package.order.manage.change.status');
    Route::post('/order-manage/send-mail','OrderManageController@send_mail')->name('admin.package.order.manage.send.mail');
    Route::post('/order-manage/delete/{id}','OrderManageController@order_delete')->name('admin.package.order.manage.delete');
    //thank you page
    Route::get('/order-manage/success-page','OrderManageController@order_success_payment')->name('admin.package.order.success.page');
    Route::post('/order-manage/success-page','OrderManageController@update_order_success_payment');
    //cancel page
    Route::get('/order-manage/cancel-page','OrderManageController@order_cancel_payment')->name('admin.package.order.cancel.page');
    Route::post('/order-manage/cancel-page','OrderManageController@update_order_cancel_payment');
    Route::get('/order-page','OrderPageController@index')->name('admin.package.order.page');
    Route::post('/order-page','OrderPageController@udpate');
    Route::post('/order-manage/bulk-action','OrderManageController@bulk_action')->name('admin.package.order.bulk.action');
    Route::post('/order-manage/reminder','OrderManageController@order_reminder')->name('admin.package.order.reminder');
    Route::get('/order-report','OrderManageController@order_report')->name('admin.package.order.report');
});
});
