<div class="topbar-area black home-{{get_static_option('home_page_variant')}} ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="topbar-inner">
                    <div class="left-content-area">
                        <ul class="info-items">
                            @foreach($all_support_item as $data)
                            @if($user_select_lang_slug == $data->lang)
                                <li><i class="{{$data->icon}}"></i> {{$data->details}}</li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="right-content-area">
                        <div class="right-inner">
                            <ul class="social-icons">
                                @foreach($all_social_item as $data)
                                    <li><a href="{{$data->url}}"><i class="{{$data->icon}}"></i></a></li>
                                @endforeach
                                @if(auth()->check())
                                    @php
                                        $route = auth()->guest() == 'admin' ? route('admin.home') : route('user.home');
                                    @endphp
                                    <li><a href="{{$route}}">{{__('Dashboard')}}</a>  <span>/</span>
                                        <a href="{{ route('user.logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('userlogout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="userlogout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                @else
                                    <li><a href="{{route('user.login')}}">{{__('Login')}}</a> <span>/</span> <a href="{{route('user.register')}}">{{__('Register')}}</a></li>
                                @endif
                            </ul>
                            @if(!empty(get_static_option('language_select_option')))
                            <select id="langchange">
                                @foreach($all_language as $lang)
                                    <option @if(session()->get('lang') == $lang->slug) selected @endif value="{{$lang->slug}}">{{$lang->name}}</option>
                                @endforeach
                            </select>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>