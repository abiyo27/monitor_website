@php $home_variant = isset($home_variant) ?? ''; @endphp 
<nav class="navbar navbar-area navbar-expand-lg absolute">
    <div class="container-fluid nav-container">
        <div class="logo-wrapper navbar-brand">
            <a href="{{url('/')}}" class="logo">
              @if(!empty($home_variant))
                 {!! render_image_markup_by_attachment_id(get_static_option('site_logo')) !!}
              @else 
                @if(request()->is('/') )
                    {!! render_image_markup_by_attachment_id(get_static_option('site_logo')) !!}
                @else
                    {!! render_image_markup_by_attachment_id(get_static_option('site_white_logo')) !!}
                @endif
              @endif
            </a>
        </div>
        <div class="collapse navbar-collapse" id="cgency">
            <!-- navbar collapse start -->
            <ul class="navbar-nav" id="primary-menu">
                <!-- navbar- nav -->
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">{{__('Home')}}</a>
                </li>
                @if(!empty(get_static_option('full_width_features_section_status')))
                <li class="nav-item">
                    <a class="nav-link" @if(request()->path() == "/")
                                        href="#{{ get_static_option('about_page_slug') }}"
                                        @else
                                        href="{{ url('/#'.get_static_option('about_page_slug')) }}"
                                        @endif >{{get_static_option('about_page_'.$user_select_lang_slug.'_name')}}</a>
                </li>
                @endif
                @if(!empty(get_static_option('why_us_section_status')))
                <li class="nav-item dropdown">
                    <a class="nav-link" @if(request()->path() == "/")
                                        href="#{{ get_static_option('feature_page_slug') }}"
                                        @else
                                        href="{{ url('/#'.get_static_option('feature_page_slug')) }}"
                                        @endif >{{get_static_option('feature_page_'.$user_select_lang_slug.'_name')}}</a>
                </li>
                @endif
                @if(!empty(get_static_option('price_plan_section_status')))
                <li class="nav-item">
                    <a class="nav-link" @if(request()->path() == "/")
                                        href="#{{ get_static_option('price_plan_page_slug') }}"
                                        @else
                                        href="{{ url('/#'.get_static_option('price_plan_page_slug')) }}"
                                        @endif >{{get_static_option('price_plan_page_'.$user_select_lang_slug.'_name')}}</a>
                </li>
                @endif
                @if(!empty(get_static_option('team_member_section_status')))
                <li class="nav-item">
                    <a class="nav-link" @if(request()->path() == "/")
                                        href="#{{ get_static_option('team_page_slug') }}"
                                        @else
                                        href="{{ url('/#'.get_static_option('team_page_slug')) }}"
                                        @endif >{{get_static_option('team_page_'.$user_select_lang_slug.'_name')}}</a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="{{route('frontend.blog')}}">{{get_static_option('blog_page_'.$user_select_lang_slug.'_name')}}</a>
                </li>
                @if(!empty(get_static_option('contact_section_status')))
                <li class="nav-item">
                    <a class="nav-link" @if(request()->path() == "/")
                                        href="#{{ get_static_option('contact_page_slug') }}"
                                        @else
                                        href="{{ url('/#'.get_static_option('contact_page_slug')) }}"
                                        @endif >{{get_static_option('contact_page_'.$user_select_lang_slug.'_name')}}</a>
                </li>
                @endif
            </ul>
            <!-- /.navbar-nav -->
        </div>
        <!-- /.navbar btn wrapper -->
        <div class="responsive-mobile-menu">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#cgency" aria-controls="cgency"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <!-- navbar collapse end -->
        @if(!empty(get_static_option('navbar_button')))
        <div class="nav-right-content">
            <ul>
                <li class="nav-btn">
                    <a href="{{get_static_option('navbar_button_url_'.$user_select_lang_slug)}}" class="boxed-btn @if(!empty(filter_static_option_value('site_rtl_enabled',$global_static_field_data)) || get_user_lang_direction() == 'rtl')gd-bg @else blank  @endif">{{get_static_option('navbar_button_text_'.$user_select_lang_slug)}}</a>
                </li>
                
            </ul>
        </div>
        @endif
    </div>
</nav>
<!-- navbar area end -->