<!DOCTYPE html>
<html lang="{{$user_select_lang_slug}}"  dir="{{get_user_lang_direction()}}">
<head>
    @if(!empty(get_static_option('site_google_analytics')))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{get_static_option('site_google_analytics')}}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', "{{get_static_option('site_google_analytics')}}");
    </script>
    @endif
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="{{filter_static_option_value('site_meta_'.$user_select_lang_slug.'_description',$global_static_field_data)}}">
        <meta name="tags" content="{{filter_static_option_value('site_meta_'.$user_select_lang_slug.'_tags',$global_static_field_data)}}">
        {!! render_favicon_by_id(filter_static_option_value('site_favicon',$global_static_field_data)) !!}
        <meta name="og:title" content="{{get_static_option('og_meta_'.$user_select_lang_slug.'_title')}}"/>
        <meta name="og:description" content="{{get_static_option('og_meta_'.$user_select_lang_slug.'_description')}}"/>
        <meta name="og:site_name" content="{{get_static_option('og_meta_'.$user_select_lang_slug.'_site_name')}}"/>
        <meta name="og:url" content="{{get_static_option('og_meta_'.$user_select_lang_slug.'_url')}}"/>
        {!! render_og_meta_image_by_attachment_id(get_static_option('og_meta_'.$user_select_lang_slug.'_image')) !!}
        <!-- load fonts dynamically -->
    {!! load_google_fonts() !!}
    <!-- all stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/dynamic-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/custom-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/root-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/common/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/jquery.ihavecookies.css')}}">
    <script src="{{asset('assets/frontend/js/jquery-3.4.1.min.js')}}"></script>
    @include('frontend.partials.root-style')
    @yield('style')
        @if(!empty(filter_static_option_value('site_rtl_enabled',$global_static_field_data)) || get_user_lang_direction() == 'rtl')
            <link rel="stylesheet" href="{{asset('assets/frontend/css/rtl.css')}}">
        @endif
    <title>{{get_static_option('site_title')}} - {{get_static_option('site_tag_line')}}</title>

    {!! filter_static_option_value('site_third_party_tracking_code') !!}
</head>
<body class="current-page-{{(request()->path() == "/")? '/' :'blog'}}">
@include('frontend.partials.support')
@include('frontend.partials.navbar')
