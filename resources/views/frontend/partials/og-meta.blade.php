@if(request()->path() == '/')
    <meta property="og:title"  content="{{filter_static_option_value('site_'.$user_select_lang_slug.'_title',$global_static_field_data)}}" />
    {!! render_og_meta_image_by_attachment_id(filter_static_option_value('og_meta_image_for_site',$global_static_field_data)) !!}
@endif

@if(request()->is([
filter_static_option_value('blog_page_slug',$global_static_field_data).'/*',
]))
    @yield('og-meta')
    <title>@yield('site-title')</title>
@elseif(request()->is([
        'p/*',
       
        filter_static_option_value('blog_page_slug',$global_static_field_data),
        filter_static_option_value('blog_page_slug',$global_static_field_data).'/*',
        
    ]))
    <title>@yield('site-title') - {{filter_static_option_value('site_'.$user_select_lang_slug.'_title',$global_static_field_data)}} </title>
@else
    <title>{{filter_static_option_value('site_'.$user_select_lang_slug.'_title',$global_static_field_data)}} - {{filter_static_option_value('site_'.$user_select_lang_slug.'_tag_line',$global_static_field_data)}}</title>
@endif