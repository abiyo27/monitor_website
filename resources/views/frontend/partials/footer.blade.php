
<footer class="footer-area footer-bg"
{!! render_background_image_markup_by_attachment_id(get_static_option('footer_bg_image')) !!}
>
    <div class="footer-top-area padding-bottom-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget about_widget">
                        <a href="{{url('/')}}" class="footer-logo">
                            {!! render_image_markup_by_attachment_id(get_static_option('about_widget_logo')) !!}
                        </a>
                        <div class="description">
                            <p>{{get_static_option('about_widget_description_'.$user_select_lang_slug)}}</p>
                        </div>
                        <ul class="social-icon">
                            @if(!empty(get_static_option('about_widget_social_icon_one')) && !empty(get_static_option('about_widget_social_icon_one_url')))
                                <li><a href="{{get_static_option('about_widget_social_icon_one_url')}}"><i class="{{get_static_option('about_widget_social_icon_one')}}"></i></a></li>
                            @endif
                            @if(!empty(get_static_option('about_widget_social_icon_two')) && !empty(get_static_option('about_widget_social_icon_two_url')))
                                <li><a href="{{get_static_option('about_widget_social_icon_two_url')}}"><i class="{{get_static_option('about_widget_social_icon_two')}}"></i></a></li>
                            @endif
                            @if(!empty(get_static_option('about_widget_social_icon_three')) && !empty(get_static_option('about_widget_social_icon_three_url')))
                                <li><a href="{{get_static_option('about_widget_social_icon_three_url')}}"><i class="{{get_static_option('about_widget_social_icon_three')}}"></i></a></li>
                            @endif
                            @if(!empty(get_static_option('about_widget_social_icon_four')) && !empty(get_static_option('about_widget_social_icon_four_url')))
                                <li><a href="{{get_static_option('about_widget_social_icon_four_url')}}"><i class="{{get_static_option('about_widget_social_icon_four')}}"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">{{get_static_option('useful_link_widget_title_'.$user_select_lang_slug)}}</h4>
                        <ul>
                            @foreach($all_usefull_links as $data)
                            @if($user_select_lang_slug == $data->lang)
                                <li><a href="{{$data->url}}"><i class="@if(!empty(filter_static_option_value('site_rtl_enabled',$global_static_field_data)) || get_user_lang_direction() == 'rtl')fas fa-chevron-left @else
                                    fas fa-chevron-right @endif"></i> {{$data->title}}</a></li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget widget widget_popular_posts">
                        <h4 class="widget-title">{{get_static_option('recent_post_widget_title_'.$user_select_lang_slug)}}</h4>
                        <ul>
                            @foreach($all_recent_post as $data)
                                <li class="single-popular-post-item">
                                    <div class="thumb">
                                        {!! render_image_markup_by_attachment_id($data->image) !!}
                                    </div>
                                    <div class="content">
                                        <span class="time"><i class="fa fa-calendar"></i> {{$data->created_at->diffForHumans()}}</span>
                                        <h4 class="title"><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{$data->title}}</a></h4>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">{{get_static_option('important_link_widget_title_'.$user_select_lang_slug)}}</h4>
                        <ul>
                            @foreach($all_important_links as $data)
                            @if($user_select_lang_slug == $data->lang)
                                <li><a href="{{$data->url}}"><i class="@if(!empty(filter_static_option_value('site_rtl_enabled',$global_static_field_data)) || get_user_lang_direction() == 'rtl')fas fa-chevron-left @else
                                    fas fa-chevron-right @endif"></i> {{$data->title}}</a></li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="copyright-inner">
                        @php
                            $footer_text = get_static_option('site_'.$user_select_lang_slug.'_footer_copyright');
                            $footer_text = str_replace('{copy}','&copy;',$footer_text);
                            $footer_text = str_replace('{year}',date('Y'),$footer_text);
                        @endphp
                        {!! $footer_text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<div class="back-to-top">
    <i class="fas fa-rocket"></i>
</div>

@include('frontend.partials.popup-structure')
<!-- jquery -->

<script src="{{asset('assets/frontend/js/jquery-migrate-3.1.0.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.rcounter.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.waypoints.js')}}"></script>
<script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/wow.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/TweenMax.js')}}"></script>
<script src="{{asset('assets/frontend/js/mousemoveparallax.js')}}"></script>
<script src="{{asset('assets/frontend/js/slick.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>
<script src="{{asset('assets/frontend/js/dynamic-script.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.counterup.min.js')}}"></script>
<script>
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

</script>
@include('frontend.partials.popup-jspart')
@include('frontend.partials.gdpr-cookie')
<script>
    (function($){
        "use strict";
        $(document).ready(function(){
            $(document).on('change','#langchange',function(e){
                $.ajax({
                    url : "{{route('frontend.langchange')}}",
                    type: "GET",
                    data:{
                        'lang' : $(this).val()
                    },
                    success:function (data) {
                        location.reload();
                    }
                })
            });
        });
    }(jQuery));
</script>
@yield('scripts')
{!! get_static_option('tawk_api_key') !!}
</body>
