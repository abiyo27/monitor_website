<div class="widget-area">
    <div class="widget widget_search">
        <form action="{{route('frontend.blog.search')}}" method="get" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" name="search" placeholder="{{__("Search")}}">
            </div>
            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <div class="widget widget_nav_menu">
        <h3 class="widget-title">{{get_static_option('blog_page_category_widget_title_'.$user_select_lang_slug)}}</h3>
        <ul>
            @foreach($all_categories as $data)
            @if($user_select_lang_slug == $data->lang)
                <li><a href="{{route('frontend.blog.category',['id' => $data->id,'any'=> Str::slug($data->name,'-')])}}">{{ucfirst($data->name)}}</a></li>
            @endif
            @endforeach
        </ul>
    </div>
    <div class="widget widget_popular_posts">
        <h4 class="widget-title">{{get_static_option('blog_page_recent_post_widget_title_'.$user_select_lang_slug)}}</h4>
        <ul>
            @foreach($all_recent_blogs as $data)
            @if($user_select_lang_slug == $data->lang)
                <li class="single-popular-post-item">
                    <div class="thumb">
                        {!! render_image_markup_by_attachment_id($data->image) !!}
                    </div>
                    <div class="content">
                        <span class="time"><i class="fa fa-calendar"></i> {{$data->created_at->diffForHumans()}}</span>
                        <h4 class="title"><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{$data->title}}</a></h4>
                    </div>
                </li>
            @endif
            @endforeach
        </ul>
    </div>
</div>
