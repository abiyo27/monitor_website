<div class="team-member-area padding-bottom-115" id="{{ get_static_option('team_page_slug') }}">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title center-aligned">
                    <h2 class="title">{{get_static_option('home_page_01_team_member_title_'.$user_select_lang_slug)}}</h2>
                    <p>{{get_static_option('home_page_01_team_member_description_'.$user_select_lang_slug)}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="team-slider">
                    @foreach($all_team_members as $data)
                    @if($user_select_lang_slug == $data->lang)
                        <div class="single-team-member-01">
                            <div class="thumb">
                                {!! render_image_markup_by_attachment_id($data->image) !!}
                                <div class="hover">
                                    <ul class="social-icon">
                                        @if(!empty($data->icon_one) && !empty($data->icon_one_url))
                                            <li><a href="{{$data->icon_one_url}}"><i class="{{$data->icon_one}}"></i></a></li>
                                        @endif
                                        @if(!empty($data->icon_two) && !empty($data->icon_two_url))
                                            <li><a href="{{$data->icon_two_url}}"><i class="{{$data->icon_two}}"></i></a></li>
                                        @endif
                                        @if(!empty($data->icon_three) && !empty($data->icon_three_url))
                                            <li><a href="{{$data->icon_three_url}}"><i class="{{$data->icon_three}}"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="description">
                                <h4 class="name">{{$data->name}}</h4>
                                <span class="designation">{{$data->designation}}</span>
                            </div>
                        </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
