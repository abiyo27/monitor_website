<div class="testimonial-area padding-bottom-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="testimonial-carousel-wrapper">

                    <ul class="slider-nav">
                        @foreach($all_testimonial as $data)
                        @if($user_select_lang_slug == $data->lang)
                            <li class="single-nav-item">
                                <div class="img-wrap">
                                    {!! render_image_markup_by_attachment_id($data->image) !!}
                                </div>
                            </li>
                        @endif
                        @endforeach
                    </ul>
                    <div class="testimonial-carousel slider-for">
                        @foreach($all_testimonial as $data)
                        @if($user_select_lang_slug == $data->lang)
                            <div class="single-testimonial-item">
                                <div class="icon">
                                    <i class="fas fa-quote-left"></i>
                                </div>
                                <div class="description">
                                    <p>{{$data->description}}</p>
                                    <div class="author-meta">
                                        <h5 class="name">{{$data->name}}</h5>
                                        <span class="designation">{{$data->designation}}</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
