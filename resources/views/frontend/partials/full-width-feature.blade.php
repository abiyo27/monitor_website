@foreach($all_full_width_features as $key => $data)
@if($user_select_lang_slug == $data->lang)
    <div class="block-feature-area padding-top-120" id="{{ get_static_option('about_page_slug') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-feature-item">
                        <div class="row">
                            <div class="col-lg-6 @if($key % 2) order-lg-2 @endif">
                                <div class="img-wrapper box-shadow-90">
                                    {!! render_image_markup_by_attachment_id($data->image) !!}
                                </div>
                            </div>
                            <div class="col-lg-6 @if($key % 2) order-lg-1 @endif">
                                <div class="content-block-area  @if(!$key % 2) padding-left-50 @endif">
                                    <a href="{{route('frontend.full.feature.details',['id' => $data->id, 'any' => Str::slug($data->title)])}}"><h4 class="title wow fadeInUp">{{$data->title}}</h4></a>
                                    <div class="content-area">
                                        {!! Str::words($data->description,35) !!}
                                    </div>
                                    <div class="btn-wrapper margin-top-20 wow fadeInDown">
                                        <a href="{{route('frontend.full.feature.details',['id' => $data->id, 'any' => Str::slug($data->title)])}}" class="boxed-btn gd-bg br-5 w180px">{{__('Read More')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endforeach