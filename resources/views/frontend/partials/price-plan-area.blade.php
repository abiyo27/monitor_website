<div class="price-plan-area padding-top-110 padding-bottom-100" id="{{ get_static_option('price_plan_page_slug') }}">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title center-aligned">
                    <h2 class="title">{{get_static_option('home_page_01_price_plan_title_'.$user_select_lang_slug)}}</h2>
                    <p>{{get_static_option('home_page_01_price_plan_description_'.$user_select_lang_slug)}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($all_price_plan as $key => $data)
            @if($user_select_lang_slug == $data->lang)
                <div class="col-lg-4 col-md-6 @if($key == 1) remove-col-padding @endif">
                    <div class="single-price-table-01  wow zoomI @if($key == 1) active @endif ">
                        <div class="price-header">
                            <span class="name">{{$data->title}}</span>
                            <div class="price-wrap">{{amount_with_currency_symbol($data->price)}}<span class="month">{{$data->type}}</span></div>
                        </div>
                        <div class="price-body">
                            <ul>
                                @foreach(explode(',',$data->features) as $item)
                                    <li>{{$item}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="price-footer">
                            
                            <a href="{{route('frontend.plan.order',['id' => $data->id])}}" class="boxed-btn blank bordered btn-rounded w180px">{{$data->btn_text}}</a>
                        </div>
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    </div>
</div>