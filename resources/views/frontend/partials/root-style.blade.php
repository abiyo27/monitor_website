<style>
    :root {
        --main-color-one: {{get_static_option('site_color')}};
        --secondary-color: {{get_static_option('site_main_color_two')}};
        --heading-font: "{{get_static_option('heading_font_family')}}",sans-serif;
        --body-font:"{{get_static_option('body_font_family')}}",sans-serif;
    }
</style>