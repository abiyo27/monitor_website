<div class="faq-area padding-top-110 padding-bottom-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title center-aligned">
                    <h2 class="title">{{get_static_option('home_page_01_faq_title_'.$user_select_lang_slug)}}</h2>
                    <p>{{get_static_option('home_page_01_faq_description_'.$user_select_lang_slug)}}</p>
                </div>
            </div>
        </div>
        <div class="row reorder-xs">
            <div class="col-lg-6">
                <div class="accordion-wrapper">
                    <div id="accordion">
                        @foreach($all_faqs as $key => $data)
                        @if($user_select_lang_slug == $data->lang)
                            @php
                            $aria_expanded = 'false';
                            if($key == 0){ $aria_expanded = 'true'; }
                            @endphp
                            <div class="card">
                                <div class="card-header" id="headingOne_{{$key}}">
                                    <h5 class="mb-0">
                                        <a  data-toggle="collapse" data-target="#collapseOne_{{$key}}" role="button" aria-expanded="{{$aria_expanded}}" aria-controls="collapseOne_{{$key}}">
                                            {{$data->title}}
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne_{{$key}}" class="collapse @if($key == 0) show @endif " aria-labelledby="headingOne_{{$key}}" data-parent="#accordion">
                                    <div class="card-body">
                                        {{$data->description}}
                                    </div>
                                </div>
                            </div>
                        @endif
                        @endforeach
                    </div>
                </div><!-- //. accordion wrapper -->
            </div>
            <div class="col-lg-6">
                <div class="img-wrapper wow zoomIn">
                    {!! render_image_markup_by_attachment_id(get_static_option('home_page_01_faq_right_side_image')) !!}
                </div>
            </div>
        </div>
    </div>
</div>