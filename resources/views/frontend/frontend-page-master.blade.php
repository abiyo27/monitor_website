
@include('frontend.partials.header')
<div class="breadcrumb-area"
     {!! render_background_image_markup_by_attachment_id(get_static_option('site_breadcrumb_bg')) !!}
    >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h2 class="page-title">@yield('page-title')</h2>
                    <ul class="page-list">
                        <li><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li><a href="#">@yield('page-title')</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@yield('content')

@include('frontend.partials.footer')