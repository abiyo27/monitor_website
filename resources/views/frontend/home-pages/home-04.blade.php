
<div class="header-area header-bg-4 style-04" id="home"
     {!! render_background_image_markup_by_attachment_id(get_static_option('home_page_04_header_bg_image')) !!}>
    <div class="header-area-inner">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <div class="header-inner">
                        <h1 class="title wow FadeInDown">{{get_static_option('home_page_header_title_'.$user_select_lang_slug)}}</h1>
                        <div class="btn-wrapper">
                            <a href="{{get_static_option('home_page_header_btn_one_url_'.$user_select_lang_slug)}}" class="boxed-btn gd-bg">{{get_static_option('home_page_header_btn_one_text_'.$user_select_lang_slug)}}</a>
                        </div>
                    </div>
                    <div class="bottom-image margin-top-80 wow fadeIndown">
                        {!! render_image_markup_by_attachment_id(get_static_option('home_page_04_header_right_image')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!empty(get_static_option('key_feature_section_status')))
    <div class="header-bottom-area padding-top-130">
        <div class="container">
            <div class="row">
                @foreach($all_key_features as $data)
                @if($user_select_lang_slug == $data->lang)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-feature-item-02  wow zoomIn">
                            <div class="icon">
                                <i class="{{$data->icon}}"></i>
                            </div>
                            <div class="content">
                                <h4 class="title">{{$data->title}}</h4>
                                <p>{{$data->description}}</p>
                            </div>
                        </div>
                    </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endif
</div>


@if(!empty(get_static_option('full_width_features_section_status')))
@include('frontend.partials.full-width-feature')
@endif
@if(!empty(get_static_option('why_us_section_status')))
@include('frontend.partials.why-us-area')
@endif
@if(!empty(get_static_option('testimonial_section_status')))
@include('frontend.partials.testimonial')
@endif
@if(!empty(get_static_option('intro_video_section_status')))
@include('frontend.partials.intro-video')
@endif
@if(!empty(get_static_option('price_plan_section_status')))
@include('frontend.partials.price-plan-area')
@endif
@if(!empty(get_static_option('team_member_section_status')))
@include('frontend.partials.team-member-area')
@endif
@if(!empty(get_static_option('contact_section_status')))
@include('frontend.partials.contact-area')
@endif
@if(!empty(get_static_option('faq_section_status')))
@include('frontend.partials.faq-area')
@endif