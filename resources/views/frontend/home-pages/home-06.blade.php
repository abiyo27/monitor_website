<div class="header-area header-bg-6 style-06" id="home"
     {!! render_background_image_markup_by_attachment_id(get_static_option('home_page_06_header_bg_image')) !!}>
    <div class="header-area-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="header-inner">
                        <div class="subwrap"><span class="subtitle">{{get_static_option('home_page_header_subtitle_'.$user_select_lang_slug)}}</span></div>
                        <h1 class="title wow FadeInDown">{{get_static_option('home_page_header_title_'.$user_select_lang_slug)}}</h1>
                        <p>{{get_static_option('home_page_header_description_'.$user_select_lang_slug)}}</p>
                        <div class="btn-wrapper">
                            <a href="{{get_static_option('home_page_header_btn_one_url_'.$user_select_lang_slug)}}" class="boxed-btn gd-bg">{{get_static_option('home_page_header_btn_one_text_'.$user_select_lang_slug)}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-right-image">
        <div class="header-right-image-animation">
            {!! render_image_markup_by_attachment_id(get_static_option('home_page_06_header_right_image')) !!}
        </div>
    </div>

</div>
@if(!empty(get_static_option('full_width_features_section_status')))
@include('frontend.partials.full-width-feature')
@endif
@if(!empty(get_static_option('key_feature_section_status')))
<div class="header-bottom-area section-bg padding-120 margin-top-120">
    <div class="container">
        <div class="row">
            @foreach($all_key_features as $data)
            @if($user_select_lang_slug == $data->lang)
                <div class="col-lg-4 col-md-6">
                    <div class="single-feature-item-02  wow zoomIn">
                        <div class="icon">
                            <i class="{{$data->icon}}"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">{{$data->title}}</h4>
                            <p>{{$data->description}}</p>
                        </div>
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
@endif
@if(!empty(get_static_option('why_us_section_status')))
<section class="why-chose-us why-choose-us-bg padding-top-110 padding-bottom-55" id="feature">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title center-aligned">
                    <h2 class="title">{{get_static_option('home_page_01_why_choose_us_title')}}</h2>
                    <p>{{get_static_option('home_page_01_why_choose_us_description')}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($why_choose_us as $data)
                <div class="col-lg-4 col-md-6">
                    <div class="why-us-box-01 margin-bottom-30">
                        <div class="icon">
                            <i class="{{$data->icon}}"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">{{$data->title}}</h4>
                            <p>{{$data->description}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@if(!empty(get_static_option('testimonial_section_status')))
@include('frontend.partials.testimonial')
@endif
@if(!empty(get_static_option('intro_video_section_status')))
@include('frontend.partials.intro-video')
@endif
@if(!empty(get_static_option('price_plan_section_status')))
@include('frontend.partials.price-plan-area')
@endif
@if(!empty(get_static_option('team_member_section_status')))
@include('frontend.partials.team-member-area')
@endif
@if(!empty(get_static_option('contact_section_status')))
@include('frontend.partials.contact-area')
@endif
@if(!empty(get_static_option('faq_section_status')))
@include('frontend.partials.faq-area')
@endif

