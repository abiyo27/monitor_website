@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Category: ').$category_name}}
@endsection
@section('content')

    <section class="blog-content-area padding-120 ">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @if(count($all_blogs) < 1)
                            <div class="col-lg-12">
                                <div class="alert alert-danger">
                                    {{__('No Post Available In ').$category_name.__(' Category')}}
                                </div>
                            </div>
                        @endif
                            @foreach($all_blogs as $data)
                            @if($user_select_lang_slug == $data->lang)
                                <div class="col-lg-6 col-md-6">
                                    <div class="single-blog-grid margin-bottom-30">
                                        <div class="thumb">
                                            {!! render_image_markup_by_attachment_id($data->image) !!}
                                        </div>
                                        <div class="content">
                                            <ul class="post-meta">
                                                <li><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}"><i class="far fa-calendar-alt"></i> {{$data->created_at->diffForHumans()}}</a></li>
                                                <li><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{__('By ')}} {{$data->user->name}}</a></li>
                                            </ul>
                                            <h4 class="title"><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{$data->title}}</a></h4>
                                            <div class="description">
                                                {!! Str::words($data->content,55) !!}
                                            </div>
                                            <a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}" class="readmore">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @endforeach
                    </div>
                    <div class="col-lg-12">
                        <nav class="pagination-wrapper" aria-label="Page navigation ">
                           {{$all_blogs->links()}}
                        </nav>
                    </div>
                </div>
                <div class="col-lg-4">
                   @include('frontend.partials.sidebar')
                </div>
            </div>
        </div>
    </section>
@endsection
