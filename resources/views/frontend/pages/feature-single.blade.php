@extends('frontend.frontend-page-master')
@section('page-title')
    {{__("Features Details")}}
@endsection
@section('content')

    <section class="blog-content-area feature-details-page padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="single-feature-details">
                        <div class="thumb">
                            {!! render_image_markup_by_attachment_id($feature->image) !!}
                        </div>
                        <div class="content-area">
                            <h2 class="title">{{$feature->title}}</h2>
                            <div class="feature-description">
                                {!! $feature->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
