@extends('backend.admin-master')
@section('site-title')
    {{__('Important Links Widget Settings')}}
@endsection
@section('style')
    @include('backend.partials.datatable.style-enqueue')
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @include('backend/partials/error')
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Important Link Widget Settings')}}</h4>
                        <form action="{{route('admin.footer.important.link.widget')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('backend.partials.languages-nav')
                            <div class="tab-content margin-top-20" id="nav-tabContent">
                                @foreach($all_languages as $key => $lang)
                                <div class="tab-pane fade @if($key == 0) show active @endif" id="nav-home-{{$lang->slug}}" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="form-group">
                                        <label for="important_link_widget_title_{{$lang->slug}}">{{__('Widget Description')}}</label>
                                        <input type="text" class="form-control"  id="important_link_widget_title_{{$lang->slug}}" name="important_link_widget_title_{{$lang->slug}}" value="{{get_static_option('important_link_widget_title_'.$lang->slug)}}" placeholder="{{__('Widget Description')}}">
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <button type="submit" class="btn btn-primary pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <div class="header-wrapp">
                            <h4 class="header-title">{{__('All Important Link')}}  </h4>
                            <div class="header-title">
                                <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" data-toggle="modal" data-target="#add_new_important_link">{{__('Add New Useful Link')}}</button>
                            </div>
                        </div>
                        <x-bulk-action/>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            @php $a=0; @endphp
                            @foreach($all_important_links as $key => $all_important_link)
                                <li class="nav-$all_important_links">
                                    <a class="nav-link @if($a == 0) active @endif"  data-toggle="tab" href="#slider_tab_{{$key}}" role="tab" aria-controls="home" aria-selected="true">{{get_language_by_slug($key)}}</a>
                                </li>
                                @php $a++; @endphp
                            @endforeach
                        </ul>
                        <div class="tab-content margin-top-20" id="myTabContent">
                            @php $b=0; @endphp
                            @foreach($all_important_links as $key => $all_important_link)
                                <div class="tab-pane fade @if($b == 0) show active @endif" id="slider_tab_{{$key}}" role="tabpanel" >
                                    <div class="table-wrap table-responsive">
                                        <table class="table table-default">
                                        <thead>
                                            <th class="no-sort">
                                                <div class="mark-all-checkbox">
                                                    <input type="checkbox" class="all-checkbox">
                                                </div>
                                            </th>
                                            <th>{{__('ID')}}</th>
                                            <th>{{__('Title')}}</th>
                                            <th>{{__('URL')}}</th>
                                            <th>{{__('Action')}}</th>
                                        </thead>
                                        <tbody>
                                        @foreach($all_important_link as $data)
                                            <tr>
                                                <td>
                                                    <x-bulk-delete-checkbox :id="$data->id"/>
                                                </td>
                                                <td>{{$data->id}}</td>
                                                <td>{{$data->title}}</td>
                                                <td>{{$data->url}}</td>
                                                 <td>
                                                    <x-delete-popover :url="route('admin.footer.important.link.delete',$data->id)"/>
                                                    <a href="#"
                                                       data-toggle="modal"
                                                       data-target="#important_link_item_edit_modal"
                                                       class="btn btn-primary btn-xs mb-3 mr-1 important_link_edit_btn"
                                                       data-id="{{$data->id}}"
                                                       data-title="{{$data->title}}"
                                                       data-url="{{$data->url}}"
                                                       data-lang="{{$data->lang}}"
                                                    >
                                                        <i class="ti-pencil"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                @php $b++; @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Start Add New Important Link  -->
             <!-- Modal -->
             <div class="modal fade" id="add_new_important_link" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header ">
                            <h5 class="modal-title">{{__('New Important Link')}}</h5>
                            <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        </div>
                        <form action="{{route('admin.footer.important.link')}}"  method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                @csrf
                                <div class="form-group">
                                    <label for="lang">{{__('Languages')}}</label>
                                    <select name="lang" id="lang" class="form-control">
                                        @foreach($all_languages as $lang)
                                            <option value="{{$lang->slug}}">{{$lang->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">{{__('Title')}}</label>
                                    <input type="text" class="form-control"  id="title" name="title" >
                                </div>
                                <div class="form-group">
                                    <label for="url">{{__('URL')}}</label>
                                    <input type="text" class="form-control"  id="url"  name="url" >
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
<!-- End Add New Important Link  -->
    <div class="modal fade" id="important_link_item_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Edit Important Link')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <form action="{{route('admin.footer.important.link.update')}}"   method="post">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="important_link_id" value="">
                        <div class="form-group">
                            <label for="edit_language">{{__('Languages')}}</label>
                            <select name="lang" id="edit_language" class="form-control">
                                @foreach($all_languages as $lang)
                                    <option value="{{$lang->slug}}">{{$lang->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="edit_title">{{__('Title')}}</label>
                            <input type="text" class="form-control"  id="edit_title" name="title" >
                        </div>
                        <div class="form-group">
                            <label for="edit_url">{{__('URL')}}</label>
                            <input type="text" class="form-control"  id="edit_url"  name="url" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save Changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
@include('backend.partials.datatable.script-enqueue')
    <script>
        $(document).ready(function () {
            <x-bulk-action-js :url="route('admin.footer.important.link.bulk.action')" />
            $(document).on('click','.important_link_edit_btn',function(){
                var el = $(this);
                var id = el.data('id');
                var title = el.data('title');
                var url = el.data('url');
                var form = $('#important_link_item_edit_modal');
                form.find('#important_link_id').val(id);
                form.find('#edit_title').val(title);
                form.find('#edit_url').val(url);
                form.find('#edit_language option[value='+el.data("lang")+']').attr('selected',true);
            });
        });
    </script>
@endsection
