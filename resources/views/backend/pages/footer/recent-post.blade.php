@extends('backend.admin-master')
@section('site-title')
    {{__('Recent Post Widget Settings')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')m
                @include('backend/partials/error')
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Recent Post Widget Settings')}}</h4>
                        <form action="{{route('admin.footer.recent.post')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('backend.partials.languages-nav')
                            <div class="tab-content margin-top-30" id="nav-tabContent">
                                @foreach($all_languages as $key => $lang)
                                <div class="tab-pane fade @if($key == 0) show active @endif" id="nav-home-{{$lang->slug}}" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="form-group">
                                        <label for="recent_post_widget_title_{{$lang->slug}}">{{__('Widget Description')}}</label>
                                        <input type="text" class="form-control"  id="recent_post_widget_title_{{$lang->slug}}" name="recent_post_widget_title_{{$lang->slug}}" value="{{get_static_option('recent_post_widget_title_'.$lang->slug)}}" placeholder="{{__('Widget Description')}}">
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <div class="form-group">
                                <label for="recent_post_widget_item">{{__('Recent Post Number')}}</label>
                                <input type="text" class="form-control" id="recent_post_widget_item"  value="{{get_static_option('recent_post_widget_item')}}" name="recent_post_widget_item" >
                                <small class="text-danger">{{__('enter how many news you want to show in this widget')}}</small>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
