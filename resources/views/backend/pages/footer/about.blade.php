@extends('backend.admin-master')
@section('site-title')
    {{__('About Widget Settings')}}
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('assets/backend/css/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/css/media-uploader.css')}}">
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @include('backend/partials/error')
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('About Widget Settings')}}</h4>
                        <form action="{{route('admin.footer.about')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('backend.partials.languages-nav')
                            <div class="tab-content margin-top-30" id="nav-tabContent">
                                @foreach($all_languages as $key => $lang)
                                <div class="tab-pane fade @if($key == 0) show active @endif" id="nav-home-{{$lang->slug}}" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="form-group">
                                        <label for="about_widget_description_{{$lang->slug}}">{{__('Widget Description')}}</label>
                                        <input type="text" class="form-control"  id="about_widget_description_{{$lang->slug}}" name="about_widget_description_{{$lang->slug}}" value="{{get_static_option('about_widget_description_'.$lang->slug)}}" placeholder="{{__('Widget Description')}}">
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <div class="form-group">
                                <label for="about_widget_logo">{{__('Widget Logo')}}</label>
                                <div class="media-upload-btn-wrapper">
                                    <div class="img-wrap">
                                        @php
                                            $image = get_attachment_image_by_id(get_static_option('about_widget_logo'),null,true);
                                            $image_btn_label = 'Upload Image';
                                        @endphp
                                        @if (!empty($image))
                                            <div class="attachment-preview">
                                                <div class="thumbnail">
                                                    <div class="centered">
                                                        <img class="avatar user-thumb" src="{{$image['img_url']}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            @php  $image_btn_label = 'Change Image'; @endphp
                                        @endif
                                    </div>
                                    <input type="hidden" id="about_widget_logo" name="about_widget_logo" value="{{get_static_option('about_widget_logo')}}">
                                    <button type="button" class="btn btn-info media_upload_form_btn" data-btntitle="{{__('Select Image')}}" data-modaltitle="{{__('Upload Image')}}" data-toggle="modal" data-target="#media_upload_modal">
                                        {{__($image_btn_label)}}
                                    </button>
                                </div>
                                <small class="form-text text-muted">{{__('allowed image format: jpg,jpeg,png')}}</small>
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_one">{{__('Social Icon One')}}</label>
                                <input type="text" class="form-control fa-icon-picker"  id="about_widget_social_icon_one" value="{{get_static_option('about_widget_social_icon_one')}}" name="about_widget_social_icon_one" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_one_url">{{__('Social Icon One Url')}}</label>
                                <input type="text" class="form-control"  id="about_widget_social_icon_one_url" value="{{get_static_option('about_widget_social_icon_one_url')}}" name="about_widget_social_icon_one_url" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_two">{{__('Social Icon Two')}}</label>
                                <input type="text" class="form-control fa-icon-picker"  id="about_widget_social_icon_two" value="{{get_static_option('about_widget_social_icon_two')}}" name="about_widget_social_icon_two" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_two_url">{{__('Social Icon Two Url')}}</label>
                                <input type="text" class="form-control"  id="about_widget_social_icon_two_url" value="{{get_static_option('about_widget_social_icon_two_url')}}"  name="about_widget_social_icon_two_url" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_three">{{__('Social Icon Three')}}</label>
                                <input type="text" class="form-control fa-icon-picker"  id="about_widget_social_icon_three" value="{{get_static_option('about_widget_social_icon_three')}}"  name="about_widget_social_icon_three" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_three_url">{{__('Social Icon Three Url')}}</label>
                                <input type="text" class="form-control"  id="about_widget_social_icon_three_url" value="{{get_static_option('about_widget_social_icon_three_url')}}" name="about_widget_social_icon_three_url" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_four">{{__('Social Icon Four')}}</label>
                                <input type="text" class="form-control fa-icon-picker"  id="about_widget_social_icon_four" value="{{get_static_option('about_widget_social_icon_four')}}" name="about_widget_social_icon_four" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_social_icon_four_url">{{__('Social Icon Four Url')}}</label>
                                <input type="text" class="form-control"  id="about_widget_social_icon_four_url" value="{{get_static_option('about_widget_social_icon_four_url')}}" name="about_widget_social_icon_four_url" >
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.partials.media-upload.media-upload-markup')
@endsection
@section('script')
    <script src="{{asset('assets/backend/js/dropzone.js')}}"></script>
    @include('backend.partials.media-upload.media-js')
@endsection