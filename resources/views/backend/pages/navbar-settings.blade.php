@extends('backend.admin-master')
@section('site-title')
    {{__('Navbar Settings')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @include('backend/partials/error')
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Navbar Settings')}}</h4>
                        <form action="{{route('admin.navbar.settings')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('backend.partials.languages-nav')
                            <div class="form-group margin-top-30">
                             <label for="navbar_button ">{{__('Button Show/Hide')}}</label>
                            <label class="switch">
                                <input type="checkbox" name="navbar_button"  @if(!empty(get_static_option('navbar_button'))) checked @endif id="navbar_button">
                                <span class="slider"></span>
                            </label>
                            </div>
                            <div class="tab-content " id="nav-tabContent">
                                @foreach($all_languages as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="nav-home-{{$lang->slug}}" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <div class="form-group">
                                            <label for="navbar_button_text_{{$lang->slug}}">{{__('Button Text')}}</label>
                                            <input type="text" name="navbar_button_text_{{$lang->slug}}" class="form-control" value="{{get_static_option('navbar_button_text_'.$lang->slug)}}" id="navbar_button_text_{{$lang->slug}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="navbar_button_url_{{$lang->slug}}">{{__('Button URL')}}</label>
                                            <input type="text" name="navbar_button_url_{{$lang->slug}}" class="form-control" value="{{get_static_option('navbar_button_url_'.$lang->slug)}}" id="navbar_button_url_{{$lang->slug}}">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

