@extends('backend.admin-master')
@section('site-title')
    {{__('Section Manage')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @include('backend/partials/error')
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Section Manage')}}</h4>
                        <form action="{{route('admin.homeone.section.manage')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-4">
                                     <div class="form-group">
                                        <label for="key_feature_section_status"><strong>{{__('Key Feature Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="key_feature_section_status"  @if(!empty(get_static_option('key_feature_section_status'))) checked @endif id="key_feature_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="full_width_features_section_status"><strong>{{__('Full Width Features Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="full_width_features_section_status"  @if(!empty(get_static_option('full_width_features_section_status'))) checked @endif id="full_width_features_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                     <div class="form-group">
                                        <label for="why_us_section_status"><strong>{{__('Why Us Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="why_us_section_status"  @if(!empty(get_static_option('why_us_section_status'))) checked @endif id="why_us_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="testimonial_section_status"><strong>{{__('Testimonial Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="testimonial_section_status"  @if(!empty(get_static_option('testimonial_section_status'))) checked @endif id="testimonial_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="intro_video_section_status"><strong>{{__('Intro video Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="intro_video_section_status"  @if(!empty(get_static_option('intro_video_section_status'))) checked @endif id="intro_video_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="price_plan_section_status"><strong>{{__('Price Plan Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="price_plan_section_status"  @if(!empty(get_static_option('price_plan_section_status'))) checked @endif id="price_plan_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="team_member_section_status"><strong>{{__('Team Member Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="team_member_section_status"  @if(!empty(get_static_option('team_member_section_status'))) checked @endif id="team_member_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="contact_section_status"><strong>{{__('Contact Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="contact_section_status"  @if(!empty(get_static_option('contact_section_status'))) checked @endif id="contact_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="faq_section_status"><strong>{{__('Faq Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="faq_section_status"  @if(!empty(get_static_option('faq_section_status'))) checked @endif id="faq_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

