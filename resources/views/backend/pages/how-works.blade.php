@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/backend/css/summernote-bs4.css')}}">
@endsection
@section('site-title')
    {{__('How Works')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-lg-6 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('How Works Items')}}</h4>
                        <table class="table table-default">
                            <thead>
                            <th>{{__('ID')}}</th>
                            <th>{{__('Title')}}</th>
                            <th>{{__('Icon')}}</th>
                            <th>{{__('Content')}}</th>
                            <th>{{__('image')}}</th>
                            <th>{{__('Action')}}</th>
                            </thead>
                            <tbody>
                            @foreach($all_how_it_works as $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->title}}</td>
                                    <td><i class="{{$data->icon}}"></i></td>
                                    <td>{!! $data->content !!}</td>
                                    <td>
                                        @php $img_url = ''; @endphp
                                        @if(file_exists('assets/uploads/how-works/how-work-image-'.$data->id.'.'.$data->image))
                                            <img style="max-width: 100px;" src="{{asset('assets/uploads/how-works/how-work-image-'.$data->id.'.'.$data->image)}}" alt="">
                                            @php $img_url = asset('assets/uploads/how-works/how-work-image-'.$data->id.'.'.$data->image); @endphp
                                        @endif
                                    </td>
                                    <td>
                                        <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1"
                                           role="button"
                                           data-toggle="popover"
                                           data-trigger="focus"
                                           data-html="true"
                                           title=""
                                           data-content="
                                               <h6>Are you sure to delete this how works item ?</h6>
                                               <form method='post' action='{{route('admin.how.works.delete',$data->id)}}'>
                                               <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                               <br>
                                                <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                </form>
                                                ">
                                            <i class="ti-trash"></i>
                                        </a>
                                        <a href="#"
                                           data-toggle="modal"
                                           data-target="#how_works_item_edit_modal"
                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 how_works_edit_btn"
                                           data-id="{{$data->id}}"
                                           data-title="{{$data->title}}"
                                           data-icon="{{$data->icon}}"
                                           data-content="{{$data->content}}"
                                           data-image="{{$img_url}}"
                                        >
                                            <i class="ti-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('New How Works')}}</h4>
                        <form action="{{route('admin.how.works')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">{{__('Title')}}</label>
                                <input type="text" class="form-control"  id="title"  name="title" placeholder="{{__('Title')}}">
                            </div>
                            <div class="form-group">
                                <label for="icon">{{__('Icon')}}</label>
                                <input type="text" class="form-control fa-icon-picker"  id="icon"  name="icon" placeholder="{{__('Icon')}}">
                            </div>
                            <div class="form-group">
                                <label for="content">{{__('Content')}}</label>
                                <input type="hidden" name="content"  class="form-control">
                                <div class="summernote" data-content=''></div>
                            </div>
                            <div class="form-group">
                                <label for="image">{{__('Image')}}</label>
                                <input type="file" class="form-control"  id="image"  name="image" >
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New How Works')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="how_works_item_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Edit How Works Item')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <form action="{{route('admin.how.works.update')}}" id="how_works_edit_modal_form" enctype="multipart/form-data"  method="post">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="how_works_id" value="">
                        <div class="form-group">
                            <label for="title">{{__('Title')}}</label>
                            <input type="text" class="form-control"  id="edit_title"  name="title" placeholder="{{__('Title')}}">
                        </div>
                        <div class="form-group">
                            <label for="edit_icon">{{__('Icon')}}</label>
                            <input type="text" class="form-control fa-icon-picker"  id="edit_icon"  name="icon" placeholder="{{__('Icon')}}">
                        </div>
                        <div class="form-group">
                            <label for="content">{{__('Content')}}</label>
                            <input type="hidden" name="content"  class="form-control" value="{{get_static_option('site_global_email_template')}}" id="edit_content">
                            <div class="summernote"  id="edit-summernote" data-content=''></div>
                        </div>
                        <div class="form-group">
                            <div class="img-wrapper">
                                <img style="max-width: 100px;margin-bottom: 20px;" src="" id="edit_preview_image" alt="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image">{{__('Image')}}</label>
                            <input type="file" class="form-control"  name="image" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save Changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/backend/js/summernote-bs4.js')}}"></script>
    <script>
        $(document).ready(function () {

            $(document).on('click','.how_works_edit_btn',function(){
                var el = $(this);
                var id = el.data('id');
                var title = el.data('title');
                var form = $('#how_works_edit_modal_form');

                form.find('#how_works_id').val(id);
                form.find('#edit_title').val(title);
                form.find('#edit_icon').val(el.data('icon'));
                form.find('#edit_preview_image').attr('src',el.data('image'));
                form.find('#edit_content').val(el.data('content'));
                $('#edit-summernote').summernote('code', el.data('content'));
            });
            $('.summernote').summernote({
                height: 150,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                },
                callbacks: {
                    onChange: function(contents, $editable) {
                        $(this).prev('input').val(contents);
                    }
                }
            });
        });
    </script>
@endsection