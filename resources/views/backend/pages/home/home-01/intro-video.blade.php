@extends('backend.admin-master')
@section('site-title')
    {{__('Intro Video')}}
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('assets/backend/css/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/css/media-uploader.css')}}">
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @include('backend/partials/message')
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Intro Video Settings')}}</h4>
                        <form action="{{route('admin.homeone.intro.video')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="home_page_01_intro_video_url">{{__('Video URL')}}</label>
                                <input type="text" name="home_page_01_intro_video_url" class="form-control" value="{{get_static_option('home_page_01_intro_video_url')}}" id="home_page_01_intro_video_url">
                            </div>

                            <div class="form-group">
                                <label for="site_favicon">{{__('Video Background Image')}}</label>
                                <div class="media-upload-btn-wrapper">
                                    <div class="img-wrap">
                                        @php
                                            $home_page_01_intro_video_image = get_attachment_image_by_id(get_static_option('home_page_01_intro_video_image'),null,true);
                                            $home_page_01_intro_video_image_btn_label = 'Upload Video Background Image';
                                        @endphp
                                        @if (!empty($home_page_01_intro_video_image))
                                            <div class="attachment-preview">
                                                <div class="thumbnail">
                                                    <div class="centered">
                                                        <img class="avatar user-thumb" src="{{$home_page_01_intro_video_image['img_url']}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            @php  $home_page_01_intro_video_image_btn_label = 'Change Video Background Image'; @endphp
                                        @endif
                                    </div>
                                    <input type="hidden" id="home_page_01_intro_video_image" name="home_page_01_intro_video_image" value="{{get_static_option('home_page_01_intro_video_image')}}">
                                    <button type="button" class="btn btn-info media_upload_form_btn" data-btntitle="{{__('Select Image')}}" data-modaltitle="{{__('Upload Image')}}" data-toggle="modal" data-target="#media_upload_modal">
                                        {{__($home_page_01_intro_video_image_btn_label)}}
                                    </button>
                                </div>
                                <small class="form-text text-muted">{{__('allowed image format: jpg,jpeg,png')}}</small>
                            </div>

                            <div class="form-group">
                                <label for="site_favicon">{{__('Section Background Image')}}</label>
                                <div class="media-upload-btn-wrapper">
                                    <div class="img-wrap">
                                        @php
                                            $home_page_01_intro_video_bg_image = get_attachment_image_by_id(get_static_option('home_page_01_intro_video_bg_image'),null,true);
                                            $home_page_01_intro_video_bg_image_btn_label = 'Upload Section Background Image';
                                        @endphp
                                        @if (!empty($home_page_01_intro_video_bg_image))
                                            <div class="attachment-preview">
                                                <div class="thumbnail">
                                                    <div class="centered">
                                                        <img class="avatar user-thumb" src="{{$home_page_01_intro_video_bg_image['img_url']}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            @php  $home_page_01_intro_video_bg_image_btn_label = 'Change Section Background Image'; @endphp
                                        @endif
                                    </div>
                                    <input type="hidden" id="home_page_01_intro_video_bg_image" name="home_page_01_intro_video_bg_image" value="{{get_static_option('home_page_01_intro_video_bg_image')}}">
                                    <button type="button" class="btn btn-info media_upload_form_btn" data-btntitle="{{__('Select Image')}}" data-modaltitle="{{__('Upload Image')}}" data-toggle="modal" data-target="#media_upload_modal">
                                        {{__($home_page_01_intro_video_bg_image_btn_label)}}
                                    </button>
                                </div>
                                <small class="form-text text-muted">{{__('allowed image format: jpg,jpeg,png')}}</small>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('backend.partials.media-upload.media-upload-markup')
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/backend/js/dropzone.js')}}"></script>
    @include('backend.partials.media-upload.media-js')
@endsection

