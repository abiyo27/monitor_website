@extends('backend.admin-master')
@section('site-title')
    {{__('Key Features Area')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Key Features Area Settings')}}</h4>
                        <form action="{{route('admin.homeone.key.features')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="home_page_01_key_features_title">{{__('Title')}}</label>
                                <input type="text" name="home_page_01_key_features_title" class="form-control"
                                       value="{{get_static_option('home_page_01_key_features_title')}}"
                                       id="home_page_01_key_features_title">
                            </div>
                            @if(
                            '05' == get_static_option('home_page_variant') ||
                            '06' == get_static_option('home_page_variant') ||
                            '07' == get_static_option('home_page_variant') ||
                            '08' == get_static_option('home_page_variant')
                            )
                                <div class="form-group">
                                    <label for="home_page_01_key_features_subtitle">{{__('Sub Title')}}</label>
                                    <input type="text" name="home_page_01_key_features_subtitle"
                                           value="{{get_static_option('home_page_01_key_features_subtitle')}}"
                                           class="form-control" id="home_page_01_key_features_subtitle">
                                </div>
                            @endif
                            @if(
                            '03' == get_static_option('home_page_variant') ||
                            '05' == get_static_option('home_page_variant') ||
                            '06' == get_static_option('home_page_variant') ||
                            '07' == get_static_option('home_page_variant') ||
                            '08' == get_static_option('home_page_variant')
                            )
                                <div class="form-group">
                                    <label for="home_page_01_key_features_items">{{__('Key Feature Items')}}</label>
                                    <input type="text" name="home_page_01_key_features_items" class="form-control"
                                           value="{{get_static_option('home_page_01_key_features_items')}}"
                                           id="home_page_01_key_features_items">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_description">{{__('Description')}}</label>
                                    <textarea name="home_page_01_key_features_description" class="form-control"
                                              id="home_page_01_key_features_description" cols="30"
                                              rows="10">{{get_static_option('home_page_01_key_features_description')}}</textarea>
                                </div>
                            @endif
                            @if(
                            '03' == get_static_option('home_page_variant') ||
                            '05' == get_static_option('home_page_variant') ||
                            '07' == get_static_option('home_page_variant')
                            )
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_one_icon">{{__('Button One Icon')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_one_icon"
                                           value="{{get_static_option('home_page_01_key_features_btn_one_icon')}}"
                                           class="form-control fa-icon-picker" id="home_page_01_key_features_btn_one_icon">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_one_text">{{__('Button One Text')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_one_text"
                                           class="form-control "
                                           value="{{get_static_option('home_page_01_key_features_btn_one_text')}}"
                                           id="home_page_01_key_features_btn_one_text">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_one_url">{{__('Button One URL')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_one_url" class="form-control"
                                           value="{{get_static_option('home_page_01_key_features_btn_one_url')}}"
                                           id="home_page_01_key_features_btn_one_url">
                                </div>

                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_two_icon">{{__('Button Two Icon')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_two_icon"
                                           value="{{get_static_option('home_page_01_key_features_btn_two_icon')}}"
                                           class="form-control fa-icon-picker" id="home_page_01_key_features_btn_two_icon">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_two_text">{{__('Button Two Text')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_two_text"
                                           class="form-control"
                                           value="{{get_static_option('home_page_01_key_features_btn_two_text')}}"
                                           id="home_page_01_key_features_btn_two_text">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_two_url">{{__('Button Two URL')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_two_url" class="form-control"
                                           value="{{get_static_option('home_page_01_key_features_btn_two_url')}}"
                                           id="home_page_01_key_features_btn_two_url">
                                </div>
                            @endif
                            @if(
                             '05' == get_static_option('home_page_variant') ||
                             '07' == get_static_option('home_page_variant')
                            )
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_three_icon">{{__('Button Three Icon')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_three_icon"
                                           value="{{get_static_option('home_page_01_key_features_btn_three_icon')}}"
                                           class="form-control fa-icon-picker" id="home_page_01_key_features_btn_three_icon">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_three_text">{{__('Button Three Text')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_three_text"
                                           class="form-control"
                                           value="{{get_static_option('home_page_01_key_features_btn_three_text')}}"
                                           id="home_page_01_key_features_btn_three_text">
                                </div>
                                <div class="form-group">
                                    <label for="home_page_01_key_features_btn_three_url">{{__('Button Three URL')}}</label>
                                    <input type="text" name="home_page_01_key_features_btn_three_url" class="form-control"
                                           value="{{get_static_option('home_page_01_key_features_btn_three_url')}}"
                                           id="home_page_01_key_features_btn_three_url">
                                </div>
                            @endif

                            <button type="submit"
                                    class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

