@extends('backend.admin-master')
@section('site-title')
    {{__('Price Plan Area')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @include('backend/partials/error')
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Price Plan Area Settings')}}</h4>
                        <form action="{{route('admin.homeone.price.plan')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('backend.partials.languages-nav')
                            <div class="tab-content margin-top-30" id="nav-tabContent">
                                @foreach($all_languages as $key => $lang)
                                <div class="tab-pane fade @if($key == 0) show active @endif" id="nav-home-{{$lang->slug}}" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="form-group">
                                        <label for="home_page_01_price_plan_title_{{$lang->slug}}">{{__('Title')}}</label>
                                        <input type="text" name="home_page_01_price_plan_title_{{$lang->slug}}" class="form-control" value="{{get_static_option('home_page_01_price_plan_title_'.$lang->slug)}}" id="home_page_01_price_plan_title_{{$lang->slug}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="home_page_01_price_plan_description_{{$lang->slug}}">{{__('Description')}}</label>
                                        <textarea name="home_page_01_price_plan_description_{{$lang->slug}}" class="form-control"   id="home_page_01_price_plan_description_{{$lang->slug}}" cols="30" rows="10">{{get_static_option('home_page_01_price_plan_description_'.$lang->slug)}}</textarea>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

