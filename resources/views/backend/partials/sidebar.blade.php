<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="{{route('admin.home')}}">
                @if(empty(get_static_option('site_admin_dark_mode')))
                {!! render_image_markup_by_attachment_id(get_static_option('site_logo')) !!}
                @else
                {!! render_image_markup_by_attachment_id(get_static_option('site_white_logo')) !!}
                @endif
            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <li class="{{active_menu('admin-home')}}">
                        <a href="{{route('admin.home')}}"
                           aria-expanded="true">
                            <i class="ti-dashboard"></i>
                            <span>@lang('dashboard')</span>
                        </a>
                    </li>
                    @if('super_admin' == auth()->user()->role)
                        <li
                                class="
                        {{active_menu('admin-home/new-user')}}
                                {{active_menu('admin-home/all-user')}}
                                        "
                        >
                            <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i>
                                <span>{{__('Admin Role Manage')}}</span></a>
                            <ul class="collapse">
                                <li class="{{active_menu('admin-home/all-user')}}"><a
                                            href="{{route('admin.all.user')}}">{{__('All Admin')}}</a></li>
                                <li class="{{active_menu('admin-home/new-user')}}"><a
                                            href="{{route('admin.new.user')}}">{{__('Add New Admin')}}</a></li>
                            </ul>
                        </li>
                        <li
                        class="main_dropdown
                        @if(request()->is(['admin-home/frontend/new-user','admin-home/frontend/all-user','admin-home/frontend/all-user/role'])) active @endif
                        ">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i>
                            <span>{{__('Users Manage')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/frontend/all-user')}}"><a
                                    href="{{route('admin.all.frontend.user')}}">{{__('All Users')}}</a></li>
                            <li class="{{active_menu('admin-home/frontend/new-user')}}"><a
                                    href="{{route('admin.frontend.new.user')}}">{{__('Add New User')}}</a></li>
                        </ul>
                    </li>
                    @endif
                    <li class="main_dropdown @if(request()->is(['admin-home/payment-logs/report','admin-home/payment-logs','admin-home/package/*'])) active @endif
                        ">
                    <a href="javascript:void(0)" aria-expanded="true">
                        <i class="ti-package"></i>
                        <span>{{__('Package Orders Manage')}}</span></a>
                    <ul class="collapse">
                        <li class="{{active_menu('admin-home/package/order-manage/all')}}"><a
                                    href="{{route('admin.package.order.manage.all')}}">{{__('All Order')}}</a></li>
                        <li class="{{active_menu('admin-home/package/order-manage/pending')}}"><a
                                    href="{{route('admin.package.order.manage.pending')}}">{{__('Pending Order')}}</a></li>
                        <li class="{{active_menu('admin-home/package/order-manage/in-progress')}}"><a
                                    href="{{route('admin.package.order.manage.in.progress')}}">{{__('In Progress Order')}}</a></li>
                        <li class="{{active_menu('admin-home/package/order-manage/completed')}}"><a
                                    href="{{route('admin.package.order.manage.completed')}}">{{__('Completed Order')}}</a></li>
                        <li class="{{active_menu('admin-home/package/order-manage/success-page')}}"><a
                                    href="{{route('admin.package.order.success.page')}}">{{__('Success Order Page')}}</a></li>
                        <li class="{{active_menu('admin-home/package/order-manage/cancel-page')}}"><a
                                    href="{{route('admin.package.order.cancel.page')}}">{{__('Cancel Order Page')}}</a></li>
                        <li class="{{active_menu('admin-home/package/order-page')}}">
                            <a href="{{route('admin.package.order.page')}}">{{__('Order Page Manage')}}</a>
                        </li>
                        <li class="{{active_menu('admin-home/package/order-report')}}">
                            <a href="{{route('admin.package.order.report')}}">{{__('Order Report')}}</a>
                        </li>
                        <li class="{{active_menu('admin-home/payment-logs')}}"><a
                                    href="{{route('admin.payment.logs')}}">{{__('All Payment Logs')}}</a></li>
                        <li class="{{active_menu('admin-home/payment-logs/report')}}"><a
                                    href="{{route('admin.payment.report')}}">{{__('Payment Report')}}</a></li>
                    </ul>
                </li>
                <li
                            class="{{active_menu('admin-home/blog')}}
                            {{active_menu('admin-home/blog-category')}}
                            {{active_menu('admin-home/new-blog')}}
                            {{active_menu('admin-home/blog-page')}}
                            @if(request()->is('admin-home/blog-edit/*')) active @endif
                                    "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-write"></i>
                            <span>{{__('Blog')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/blog')}}"><a
                                        href="{{route('admin.blog')}}">{{__('All Blog')}}</a></li>
                            <li class="{{active_menu('admin-home/blog-category')}}"><a
                                        href="{{route('admin.blog.category')}}">{{__('Category')}}</a></li>
                            <li class="{{active_menu('admin-home/new-blog')}}"><a
                                        href="{{route('admin.blog.new')}}">{{__('Add New Post')}}</a></li>
                            <li class="{{active_menu('admin-home/blog-page')}}"><a 
                                        href="{{route('admin.blog.page')}}" aria-expanded="true">{{__('Blog Page Settings')}}</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{active_menu('admin-home/page')}}
                        {{active_menu('admin-home/new-page')}}
                        @if(request()->is('admin-home/page-edit/*')) active @endif">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-write"></i>
                            <span>{{__('Pages')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/page')}}"><a
                                        href="{{route('admin.page')}}">{{__('All Pages')}}</a></li>
                            <li class="{{active_menu('admin-home/new-page')}}"><a
                                        href="{{route('admin.page.new')}}">{{__('Add New Page')}}</a></li>
                        </ul>
                    </li>
                    <li class="{{active_menu('admin-home/navbar-settings')}}
                        {{active_menu('admin-home/topbar')}}
                        {{active_menu('admin-home/media-upload/page')}}
                        {{active_menu('admin-home/home-variant')}}
                        ">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-palette"></i>
                            <span>{{__('Appearance Settings')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/navbar-settings')}}">
                                <a href="{{route('admin.navbar.settings')}}"
                                   aria-expanded="true">
                                    {{__('Nabvar Settings')}}
                                </a>
                            </li>
                            <li class="{{active_menu('admin-home/topbar')}}">
                                <a href="{{route('admin.topbar')}}"
                                   aria-expanded="true">
                                    {{__('Top Bar Settings')}}
                                </a>
                            </li>
                            <li class="{{active_menu('admin-home/media-upload/page')}}">
                                <a href="{{route('admin.upload.media.images.page')}}"
                                   aria-expanded="true">
                                    {{__('Media Images Manage')}}
                                </a>
                            </li>
                            <li class="{{active_menu('admin-home/home-variant')}}">
                                <a href="{{route('admin.home.variant')}}"
                                   aria-expanded="true">
                                  {{__('Home Variant')}}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{active_menu('admin-home/home-page/header')}}">
                        <a href="{{route('admin.home.header')}}" aria-expanded="true">
                            <i class="ti-control-forward"></i>
                            <span>{{__('Header Area')}}</span>
                        </a>
                    </li>

                    
                    <li class="{{active_menu('admin-home/keyfeatures')}}">
                        <a href="{{route('admin.keyfeatures')}}" aria-expanded="true"><i class="ti-control-forward"></i>
                            <span>{{__('Key Features')}}</span></a>
                    </li>
                    <li class="{{active_menu('admin-home/why-choose')}}">
                        <a href="{{route('admin.why.choose.us')}}" aria-expanded="true"><i
                                    class="ti-control-forward"></i> <span>{{__('Why Choose Us')}}</span></a>
                    </li>
                    <li class="{{active_menu('admin-home/full-width-features')}}">
                        <a href="{{route('admin.full.width.features')}}" aria-expanded="true"><i
                                    class="ti-control-forward"></i> <span>{{__('Full Width Features')}}</span></a>
                    </li>
                    <li class="{{active_menu('admin-home/price-plan')}}">
                        <a href="{{route('admin.price.plan')}}" aria-expanded="true"><i class="ti-control-forward"></i>
                            <span>{{__('Price Plan')}}</span></a>
                    </li>


                    <li class="{{active_menu('admin-home/team-member')}}">
                        <a href="{{route('admin.team.member')}}" aria-expanded="true"><i class="ti-control-forward"></i>
                            <span>{{__('Team Members')}}</span></a>
                    </li>
                    <li class="{{active_menu('admin-home/testimonial')}}">
                        <a href="{{route('admin.testimonial')}}" aria-expanded="true"><i class="ti-control-forward"></i>
                            <span>{{__('Testimonial')}}</span></a>
                    </li>
                    <li class="{{active_menu('admin-home/faq')}}">
                        <a href="{{route('admin.faq')}}" aria-expanded="true"><i class="ti-control-forward"></i>
                            <span>{{__('Faq')}}</span></a>
                    </li>
                    
                    <li class="{{active_menu('admin-home/popup-builder/all')}}">
                        <a href="{{route('admin.popup.builder.all')}}"
                           aria-expanded="true">
                            <i class="ti-shine"></i>
                            <span>{{__('Popup Builder')}}</span>
                        </a>
                    </li>
                    
                     
                    <li class="main_dropdown @if(request()->is('admin-home/form-builder/*')) active @endif">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-layers"></i>
                            <span>{{__('Form Builder')}}</span></a>
                            <ul class="collapse">
                                <li class="{{active_menu('admin-home/form-builder/order-form')}}"><a
                                            href="{{route('admin.form.builder.order')}}">{{__('Order Form')}}</a></li>
                                <li class="{{active_menu('admin-home/form-builder/contact-form')}}"><a
                                            href="{{route('admin.form.builder.contact')}}">{{__('Contact Form')}}</a></li>
                                </li>
                            </ul>
                    </li>
                    <li class="@if(request()->is('admin-home/home-page-01/*')) active @endif">
                        <a href="javascript:void(0)"
                           aria-expanded="true">
                            <i class="ti-home"></i>
                            <span>{{__('Home Page Manage')}}</span>
                        </a>
                        <ul class="collapse">
                            @if( get_static_option('home_page_variant') == '06')
                                <li class="{{active_menu('admin-home/home-page-01/key-features')}}"><a
                                            href="{{route('admin.homeone.key.features')}}">{{__('Key Features Area')}}</a>
                                </li>
                            @endif
                        <li class="{{active_menu('admin-home/home-page-01/why-choose')}}"><a
                                        href="{{route('admin.homeone.why.choose.us')}}">{{__('Why Coose Us Area')}}</a>
                            </li>
                            <li class="{{active_menu('admin-home/home-page-01/intro-video')}}"><a
                                        href="{{route('admin.homeone.intro.video')}}">{{__('Intro Video Area')}}</a>
                            </li>
                            <li class="{{active_menu('admin-home/home-page-01/price-plan')}}"><a
                                        href="{{route('admin.homeone.price.plan')}}">{{__('Pricing Plan Area')}}</a>
                            </li>
                            <li class="{{active_menu('admin-home/home-page-01/team-member')}}"><a
                                        href="{{route('admin.homeone.team.member')}}">{{__('Team Area')}}</a></li>
                            <li class="{{active_menu('admin-home/home-page-01/contact')}}"><a
                                        href="{{route('admin.homeone.contact')}}">{{__('Contact Area')}}</a></li>
                            <li class="{{active_menu('admin-home/home-page-01/faq')}}"><a
                                        href="{{route('admin.homeone.faq')}}">{{__('Faq Area')}}</a></li>
                            <li class="{{active_menu('admin-home/home-page-01/section-manage')}}"><a
                                        href="{{route('admin.homeone.section.manage')}}">
                                    {{__('Section Manage')}}</a></li>
                        </ul>
                    </li>
                    
                   
                    <li class="@if(request()->is('admin-home/footer/*')) active @endif">
                        <a href="javascript:void(0)"
                           aria-expanded="true">
                            <i class="ti-layout"></i>
                            <span>{{__('Footer Area')}}</span>
                        </a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/footer/general')}}"><a
                                        href="{{route('admin.footer.general')}}">{{__('General Settings')}}</a></li>
                            <li class="{{active_menu('admin-home/footer/about')}}"><a
                                        href="{{route('admin.footer.about')}}">{{__('About Widget')}}</a></li>
                            <li class="{{active_menu('admin-home/footer/useful-links')}}"><a
                                        href="{{route('admin.footer.useful.link')}}">{{__('Useful Links Widget')}}</a>
                            </li>
                            <li class="{{active_menu('admin-home/footer/recent-post')}}"><a
                                        href="{{route('admin.footer.recent.post')}}">{{__('Recent Posts Widget')}}</a>
                            </li>
                            <li class="{{active_menu('admin-home/footer/important-links')}}"><a
                                        href="{{route('admin.footer.important.link')}}">{{__('Important Links Widget')}}</a>
                            </li>
                        </ul>
                    </li>
                    
                    @if('super_admin' == auth()->user()->role || 'admin' == auth()->user()->role)
                        <li class="@if(request()->is('admin-home/general-settings/*')) active @endif">
                            <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i>
                                <span>{{__('General Settings')}}</span></a>
                            <ul class="collapse ">
                                <li class="{{active_menu('admin-home/general-settings/site-identity')}}"><a
                                            href="{{route('admin.general.site.identity')}}">{{__('Site Identity')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/basic-settings')}}"><a
                                            href="{{route('admin.general.basic.settings')}}">{{__('Basic Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/typography-settings')}}"><a
                                            href="{{route('admin.general.typography.settings')}}">{{__('Typography Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/seo-settings')}}"><a
                                            href="{{route('admin.general.seo.settings')}}">{{__('SEO Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/scripts')}}"><a
                                            href="{{route('admin.general.scripts.settings')}}">{{__('Third Party Scripts')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/email-template')}}"><a
                                            href="{{route('admin.general.email.template')}}">{{__('Email Template')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/email-settings')}}"><a
                                            href="{{route('admin.general.email.settings')}}">{{__('Email Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/smtp-settings')}}"><a
                                            href="{{route('admin.general.smtp.settings')}}">{{__('SMTP Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/page-settings')}}"><a
                                            href="{{route('admin.general.page.settings')}}">{{__('Page Settings')}}</a></li>
                                
                                            <li class="{{active_menu('admin-home/general-settings/payment-settings')}}"><a
                                                href="{{route('admin.general.payment.settings')}}">{{__('Payment Gateway Settings')}}</a></li>
                                <li class="{{active_menu('admin-home/general-settings/custom-css')}}"><a
                                            href="{{route('admin.general.custom.css')}}">{{__('Custom CSS')}}</a></li>
                                <li class="{{active_menu('admin-home/general-settings/custom-js')}}"><a
                                            href="{{route('admin.general.custom.js')}}">{{__('Custom JS')}}</a></li>
                                <li class="{{active_menu('admin-home/general-settings/gdpr-settings')}}"><a
                                            href="{{route('admin.general.gdpr.settings')}}">{{__('GDPR Compliant Cookies Settings')}}</a>
                                </li>

                               

                                <li class="{{active_menu('admin-home/general-settings/popup-settings')}}"><a
                                            href="{{route('admin.general.popup.settings')}}">{{__('Popup Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/sitemap-settings')}}"><a
                                            href="{{route('admin.general.sitemap.settings')}}">{{__('Sitemap Settings')}}</a>
                                </li>
                                <li class="{{active_menu('admin-home/general-settings/license-setting')}}"><a
                                            href="{{route('admin.general.license.settings')}}">{{__('Licence Settings')}}</a>
                                </li>

                                <li class="{{active_menu('admin-home/general-settings/cache-settings')}}"><a
                                            href="{{route('admin.general.cache.settings')}}">{{__('Cache Settings')}}</a>
                                </li>
                            </ul>
                        </li>
                        <li
                                class="@if(request()->is('admin-home/languages/*') || request()->is('admin-home/languages') ) active @endif">
                            <a href="{{route('admin.languages')}}" aria-expanded="true"><i class="ti-signal"></i>
                                <span>{{__('Languages')}}</span></a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
