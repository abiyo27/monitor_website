<?php

namespace App\Http\Controllers;

use App\FullWidthFeatrues;
use App\Language;
use Illuminate\Http\Request;

class FullWidthFeatruesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $all_full_width_features = FullWidthFeatrues::all()->groupBy('lang');
        $all_languages = Language::all();
        return view('backend.pages.full-width-features')->with([
            'all_full_width_features' => $all_full_width_features,
            'all_languages' => $all_languages
            ]);
    }

    public function store(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'description' => 'required|string',
            'image' => 'string|nullable',
            'lang' => 'string|required',
        ]);
        FullWidthFeatrues::create($request->all())->id;
        return redirect()->back()->with(['msg' => 'New Full Width Feature Added...','type' => 'success']);
    }

    public function update(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'description' => 'required|string',
            'image' => 'string|nullable',
            'lang' => 'string|required',
        ]);

        FullWidthFeatrues::find($request->id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'lang' =>  $request->lang,
            'image' => $request->image
        ]);
        return redirect()->back()->with(['msg' => 'New Full Width Feature Updated...','type' => 'success']);
    }

    public function delete($id){
        FullWidthFeatrues::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Delete Success...','type' => 'danger']);
    }
    public function bulk_action(Request $request){
        $all = FullWidthFeatrues::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }
}
