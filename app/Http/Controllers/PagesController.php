<?php

namespace App\Http\Controllers;

use App\Language;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $all_page = Page::all()->groupBy('lang');
        $all_language = Language::all();
        return view('backend.pages.page.index')->with([
            'all_page' => $all_page,
            'all_languages' => $all_language
        ]);
    }
    public function new_page(){
        $all_language = Language::all();
        return view('backend.pages.page.new')->with(['all_languages' => $all_language]);
    }
    public function store_new_page(Request $request){
        $this->validate($request,[
            'content' => 'nullable',
            'title' => 'required',
            'status' => 'required|string|max:191',
            'lang' => 'nullable',
            'slug' => 'nullable'
           
        ]);

        Page::create([
            'status' => $request->status,
            'content' => $request->page_content,
            'title' => $request->title,
            'lang' => $request->lang,
            'slug' => Str::slug($request->title)
        ]);

        return redirect()->back()->with([
            'msg' => 'New Page Created...',
            'type' => 'success'
        ]);
    }
    public function edit_page($id){
        $page_post = Page::find($id);
        $all_language = Language::all();
        return view('backend.pages.page.edit')->with([
            'page_post' => $page_post,
            'all_languages' => $all_language
        ]);
    }
    public function update_page(Request $request,$id){
        $this->validate($request,[
            'content' => 'nullable',
            'title' => 'required',
            'status' => 'required|string|max:191',
            'lang' => 'nullable',
            'slug' => 'nullable'
        ]);
        Page::where('id',$id)->update([
            'status' => $request->status,
            'content' => $request->page_content,
            'title' => $request->title,
            'lang' => $request->lang,
            'slug' => Str::slug($request->title)
        ]);


        return redirect()->back()->with([
            'msg' => 'Page updated...',
            'type' => 'success'
        ]);
    }
    public function delete_page(Request $request,$id){
        Page::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Page Delete Success...',
            'type' => 'danger'
        ]);
    }
    public function bulk_action(Request $request){
        $all = Page::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }
}
