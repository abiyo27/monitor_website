<?php

namespace App\Http\Controllers;

use App\Importantlink;
use App\Language;
use App\UsefulLink;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function about_widget(){
        $all_languages = Language::all();
        return view('backend.pages.footer.about')->with([
            'all_languages'=>$all_languages
            ]);
    }
    public function update_about_widget(Request $request){
        $all_languages = Language::all();
        $this->validate($request,[
            'about_widget_logo' => 'nullable|string',
            'about_widget_social_icon_one' => 'nullable|string|max:191',
            'about_widget_social_icon_two' => 'nullable|string|max:191',
            'about_widget_social_icon_three' => 'nullable|string|max:191',
            'about_widget_social_icon_four' => 'nullable|string|max:191',
            'about_widget_social_icon_one_url' => 'nullable|string|max:191',
            'about_widget_social_icon_two_url' => 'nullable|string|max:191',
            'about_widget_social_icon_three_url' => 'nullable|string|max:191',
            'about_widget_social_icon_four_url' => 'nullable|string|max:191',
        ]);
        $save_data = [
            'about_widget_logo',
            'about_widget_social_icon_one',
            'about_widget_social_icon_two',
            'about_widget_social_icon_three',
            'about_widget_social_icon_four',
            'about_widget_social_icon_one_url',
            'about_widget_social_icon_two_url',
            'about_widget_social_icon_three_url',
            'about_widget_social_icon_four_url',
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'about_widget_description_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'about_widget_description_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        return redirect()->back()->with([
            'msg' => 'About Widget Update Success...',
            'type' => 'success'
        ]);
    }

    public function useful_links_widget(){
        $all_languages = Language::all();
        $all_useful_link = UsefulLink::all()->groupBy('lang');
        return view('backend.pages.footer.useful-link')->with([
            'all_useful_links' => $all_useful_link,
            'all_languages' => $all_languages,
            
        ]);
    }
    public function new_useful_links_widget(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
            'lang' => 'required|string',
        ]);
        UsefulLink::create($request->all());
        return redirect()->back()->with([
            'msg' => 'New Useful Link Added...',
            'type' => 'success'
        ]);
    }
    public function update_useful_links_widget(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
            'lang' => 'required|string'
        ]);
        UsefulLink::where('id',$request->id)->update([
            'title' => $request->title,
            'url' => $request->url,
            'lang' => $request->lang,
        ]);
        return redirect()->back()->with([
            'msg' => 'Useful Link Updated...',
            'type' => 'success'
        ]);
    }
    public function update_widget_useful_links(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'useful_link_widget_title_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'useful_link_widget_title_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        return redirect()->back()->with([
            'msg' => 'Useful Widget Settings Success...',
            'type' => 'success'
        ]);
    }
    public function delete_useful_links_widget(Request $request,$id){
        UsefulLink::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Useful Link Delete...',
            'type' => 'danger'
        ]);
    }
    public function useful_link_bulk_action(Request $request){
        $all = UsefulLink::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }
    public function important_link_bulk_action(Request $request){
        $all = Importantlink::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }
    public function recent_post_widget(){
        
        $all_languages = Language::all();
        return view('backend.pages.footer.recent-post')->with([
            'all_languages' => $all_languages
            
        ]);
    }
    public function update_recent_post_widget(Request $request){
        $all_languages = Language::all();
        $this->validate($request,[
            'recent_post_widget_item' => 'required'
        ]);
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'recent_post_widget_title_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'recent_post_widget_title_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        update_static_option('recent_post_widget_item',$request->recent_post_widget_item);
        return redirect()->back()->with([
            'msg' => 'Recent Post Widget Update Success...',
            'type' => 'success'
        ]);
    }

    public function important_links_widget(){
        $all_languages = Language::all();
        $all_important_link = Importantlink::all()->groupBy('lang');
        return view('backend.pages.footer.important-links')->with([
            'all_important_links' => $all_important_link,
            'all_languages' => $all_languages
        ]);
    }
    public function update_widget_important_links(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'important_link_widget_title_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'important_link_widget_title_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        return redirect()->back()->with([
            'msg' => 'Important Widget Settings Success...',
            'type' => 'success'
        ]);
    }
    public function new_important_links_widget(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
            'lang' => 'required|string'
        ]);
        Importantlink::create($request->all());
        return redirect()->back()->with([
            'msg' => 'New Important Link Added...',
            'type' => 'success'
        ]);
    }
    public function update_important_links_widget(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
            'lang' => 'required|string'
        ]);
        Importantlink::where('id',$request->id)->update([
            'title' => $request->title,
            'url' => $request->url,
            'lang' => $request->lang
        ]);
        return redirect()->back()->with([
            'msg' => 'Important Link Updated...',
            'type' => 'success'
        ]);
    }

    public function delete_important_links_widget(Request $request,$id){
        Importantlink::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Important Link Delete...',
            'type' => 'danger'
        ]);
    }
    
    public function general_settings(){
        return view('backend.pages.footer.general-settings');
    }
    public function update_general_settings(Request $request){
        $this->validate($request,[
            'footer_bg_image' => 'nullable|string'
        ]);
        update_static_option('footer_bg_image',$request->footer_bg_image);
        return redirect()->back()->with([
            'msg' => 'Settings Updated...',
            'type' => 'success'
        ]);
    }
   
}
