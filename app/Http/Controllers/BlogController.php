<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use App\Language;
use App\Volunteer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;


class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $all_blog = Blog::all()->groupBy('lang');
        $all_language = Language::all();
        return view('backend.pages.blog.index')->with([
            'all_blogs' => $all_blog,
            'all_languages' => $all_language
        ]);
    }
    public function new_blog(){
        $all_language = Language::all();
        $all_category = BlogCategory::all();
        return view('backend.pages.blog.new')->with([
            'all_category' => $all_category,
            'all_languages' => $all_language
        ]);
    }
    public function store_new_blog(Request $request){
        $this->validate($request,[
           'category' => 'required',
           'blog_content' => 'required',
           'tags' => 'required',
           'title' => 'required',
           'image' => 'nullable|string',
           'lang' => 'required|string',
        ]);

        Blog::create([
            'blog_categories_id' => $request->category,
            'content' => $request->blog_content,
            'tags' => $request->tags,
            'title' => $request->title,
            'lang' => $request->lang,
            'image' => $request->image,
            'user_id' => Auth::user()->id
        ]);
        return redirect()->back()->with([
            'msg' => 'New Blog Post Added...',
            'type' => 'success'
        ]);
    }
    public function edit_blog($id){
        $all_language = Language::all();
        $all_category = BlogCategory::all();
        $blog_post = Blog::find($id);
        return view('backend.pages.blog.edit')->with([
            'all_category' => $all_category,
            'blog_post' => $blog_post,
            'all_languages' => $all_language,
        ]);
    }
    public function update_blog(Request $request,$id){
        $this->validate($request,[
            'category' => 'required',
           'blog_content' => 'required',
           'tags' => 'required',
           'title' => 'required',
           'image' => 'nullable|string',
           'lang' => 'required|string',
        ]);
        Blog::where('id',$id)->update([
            'blog_categories_id' => $request->category,
            'content' => $request->blog_content,
            'tags' => $request->tags,
            'title' => $request->title,
            'lang' => $request->lang,
            'image' => $request->image
        ]);
        return redirect()->back()->with([
            'msg' => 'Blog Post updated...',
            'type' => 'success'
        ]);
    }
    public function delete_blog(Request $request,$id){
        Blog::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Blog Post Delete Success...',
            'type' => 'danger'
        ]);
    }
    public function bulk_action_blog(Request $request){
        $all = Blog::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }

    public function category(){
        $all_category = BlogCategory::all()->groupBy('lang');
        $all_language = Language::all();
        return view('backend.pages.blog.category')->with([
            'all_categories' => $all_category,
            'all_languages' => $all_language
        ]);
    }
    public function new_category(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'status' => 'required|string|max:191',
            'lang' => 'required|string'
            
        ]);
        BlogCategory::create($request->all());
        return redirect()->back()->with([
            'msg' => 'New Category Added...',
            'type' => 'success'
        ]);
    }

    public function update_category(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'status' => 'required|string|max:191',
            'lang' => 'required|string'
        ]);

        BlogCategory::find($request->id)->update([
            'name' => $request->name,
            'status' => $request->status,
            'lang' => $request->lang,
        ]);

        return redirect()->back()->with([
            'msg' => 'Category Update Success...',
            'type' => 'success'
        ]);
    }

    public function delete_category(Request $request,$id){
        if (Blog::where('blog_categories_id',$id)->first()){
            return redirect()->back()->with([
                'msg' => 'You Can Not Delete This Category, It Already Associated With A Post...',
                'type' => 'danger'
            ]);
        }
        BlogCategory::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Category Delete Success...',
            'type' => 'danger'
        ]);
    }
    public function bulk_action_blog_category(Request $request){
        $all = BlogCategory::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }


}
