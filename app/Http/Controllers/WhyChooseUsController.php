<?php

namespace App\Http\Controllers;

use App\Language;
use App\WhyChooseUs;
use Illuminate\Http\Request;

class WhyChooseUsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $all_languages = Language::all();
        $why_choose_us = WhyChooseUs::all()->groupBy('lang');;
        return view('backend.pages.why-choose-us')->with([
            'why_choose_us' => $why_choose_us,
            'all_languages' => $all_languages
            ]);
    }

    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'icon' => 'required|string|max:191',
            'description' => 'required|string',
            'lang' => 'required|string',
        ]);
        WhyChooseUs::create($request->all());
        return redirect()->back()->with(['msg' => 'New Why Choose Us Item Added...','type' => 'success']);
    }

    public function update(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'icon' => 'required|string|max:191',
            'description' => 'required|string',
            'lang' => 'required|string',
        ]);
        WhyChooseUs::find($request->id)->update($request->all());
        return redirect()->back()->with(['msg' => 'Why Choose Us Item Updated...','type' => 'success']);
    }

    public function delete($id){
        WhyChooseUs::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Delete Success...','type' => 'danger']);
    }
    public function bulk_action(Request $request){
        $all = WhyChooseUs::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }
}
