<?php

namespace App\Http\Controllers;

use App\Language;
use App\StaticOption;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function header_area()
    {
        $all_languages = Language::all();
        return view('backend.pages.home.header')->with(['all_languages' => $all_languages]);
    }

    public function header_area_update(Request $request)
    {
        $home_page_variant = get_static_option('home_page_variant');
        $this->validate($request, [
            'home_page_header_right_image'=> 'nullable|string',
            'home_page_header_bg_image'=> 'nullable|string',
        ]);
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'home_page_header_title_' . $lang->slug=> 'nullable|string',
                'home_page_header_subtitle_' . $lang->slug=> 'nullable|string',
                'home_page_header_description_' . $lang->slug=> 'nullable|string',
                'home_page_header_btn_one_text_' . $lang->slug=> 'nullable|string',
                'home_page_header_btn_one_url_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'home_page_header_title_' . $lang->slug,
                'home_page_header_subtitle_' . $lang->slug,
                'home_page_header_description_' . $lang->slug,
                'home_page_header_btn_one_text_' . $lang->slug,
                'home_page_header_btn_one_url_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        update_static_option('home_page_'.$home_page_variant.'_header_right_image',$request->home_page_header_right_image);
        update_static_option('home_page_'.$home_page_variant.'_header_bg_image',$request->home_page_header_bg_image);
        return redirect()->back()->with([
            'msg' => 'Settings Updated ...',
            'type' => 'success'
        ]);
    }
    public function home_01_intro_video(){
        return view('backend.pages.home.home-01.intro-video');
    }
    public function home_01_update_intro_video(Request $request){
        $this->validate($request,[
           'home_page_01_intro_video_url' => 'required|string|max:191',
           'home_page_01_intro_video_image' => 'required|string',
           'home_page_01_intro_video_bg_image' => 'required|string',
        ]);
        $save_data = [
            'home_page_01_intro_video_url',
            'home_page_01_intro_video_image',
            'home_page_01_intro_video_bg_image',
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }

        return redirect()->back()->with([
            'msg' => 'Settings Updated ...',
            'type' => 'success'
        ]);
    }

    public function home_01_testimonial(){
        return view('backend.pages.home.home-01.testimonial');
    }
    public function home_01_update_testimonial(Request $request){

        $this->validate($request,[
            'home_page_01_testimonial_subtitle' => 'required|string',
            'home_page_01_testimonial_title' => 'required|string',
            'home_page_01_testimonial_description' => 'required|string',
            'home_page_01_testimonial_bg_image' => 'mimes:jpg,jpeg,png',
        ]);

        if ($request->hasFile('home_page_01_testimonial_bg_image')) {
            $image = $request->home_page_01_testimonial_bg_image;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'home-page-01-testimonial-bg-image.' . $image_extenstion;
            if (check_image_extension($image)) {
                $image->move('assets/uploads/', $image_name);
                update_static_option('home_page_01_testimonial_bg_image', $image_extenstion);
            }
        }
        $save_data = [
            'home_page_01_testimonial_subtitle',
            'home_page_01_testimonial_title',
            'home_page_01_testimonial_description'
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }

        return redirect()->back()->with([
            'msg' => 'Settings Updated ...',
            'type' => 'success'
        ]);
    }

    public function home_01_price_plan(){
        $all_languages = Language::all();
        return view('backend.pages.home.home-01.price-plan')->with(['all_languages' => $all_languages]);
    }
    public function home_01_update_price_plan(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'home_page_01_price_plan_title_' . $lang->slug=> 'nullable|string',
                'home_page_01_price_plan_description_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'home_page_01_price_plan_title_' . $lang->slug,
                'home_page_01_price_plan_description_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        return redirect()->back()->with([
            'msg' => __('Settings Updated'),
            'type' => 'success'
        ]);
    }
    public function home_01_team_member(){
        $all_languages = Language::all();
        return view('backend.pages.home.home-01.team-member')->with(['all_languages' => $all_languages]);
    }

    public function home_01_update_team_member(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'home_page_01_team_member_title_' . $lang->slug=> 'nullable|string',
                'home_page_01_team_member_description_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'home_page_01_team_member_title_' . $lang->slug,
                'home_page_01_team_member_description_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        return redirect()->back()->with([
            'msg' => __('Settings Updated'),
            'type' => 'success'
        ]);
    }
    public function home_01_contact(){
        $all_languages = Language::all();
        return view('backend.pages.home.home-01.contact')->with(['all_languages' => $all_languages]);
    }
    public function home_01_update_contact(Request $request){
        $this->validate($request, [
            'home_page_01_contact_bg_image'=> 'nullable|string',
        ]);
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'home_page_01_contact_title_' . $lang->slug=> 'nullable|string',
                'home_page_01_contact_description_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'home_page_01_contact_title_' . $lang->slug,
                'home_page_01_contact_description_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        update_static_option('home_page_01_contact_bg_image',$request->home_page_01_contact_bg_image);
        return redirect()->back()->with([
            'msg' => __('Settings Updated...'),
            'type' => 'success'
        ]);
        return redirect()->back()->with([
            'msg' => 'Settings Updated ...',
            'type' => 'success'
        ]);
    }
    public function home_01_why_choose_us(){
        $all_languages = Language::all();
        return view('backend.pages.home.home-01.why-choose-us')->with(['all_languages' => $all_languages]);
    }
    public function home_01_update_why_choose_us(Request $request){
        $this->validate($request, [
            'home_page_01_why_choose_us_bg_image'=> 'nullable|string',
        ]);
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'home_page_01_why_choose_us_title_' . $lang->slug=> 'nullable|string',
                'home_page_01_why_choose_us_description_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'home_page_01_why_choose_us_title_' . $lang->slug,
                'home_page_01_why_choose_us_description_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        update_static_option('home_page_01_why_choose_us_bg_image',$request->home_page_01_why_choose_us_bg_image);
        return redirect()->back()->with([
            'msg' => __('Settings Updated...'),
            'type' => 'success'
        ]);
    }
    public function home_01_faq(){
        $all_languages = Language::all();
        return view('backend.pages.home.home-01.faq')->with(['all_languages' => $all_languages]);
    }
    public function home_01_update_faq(Request $request){

        $this->validate($request, [
            'home_page_01_faq_right_side_image'=> 'nullable|string',
        ]);
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'home_page_01_faq_title_' . $lang->slug=> 'nullable|string',
                'home_page_01_faq_description_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'home_page_01_faq_title_' . $lang->slug,
                'home_page_01_faq_description_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        update_static_option('home_page_01_faq_right_side_image',$request->home_page_01_faq_right_side_image);
        return redirect()->back()->with([
            'msg' => __('Settings Updated'),
            'type' => 'success'
        ]);
    }
    
     public function home_01_section_manage(){
        return view('backend.pages.section-manage');
    }
    public function home_01_update_section_manage(Request $request){

        $this->validate($request,[
            'key_feature_section_status' => 'nullable|string',
            'full_width_features_section_status' => 'nullable|string',
            'why_us_section_status' => 'nullable|string',
            'testimonial_section_status' => 'nullable|string',
            'intro_video_section_status' => 'nullable|string',
            'price_plan_section_status' => 'nullable|string',
            'team_member_section_status' => 'nullable|string',
            'contact_section_status' => 'nullable|string',
            'faq_section_status' => 'nullable|string',
            'brand_logo_section_status' => 'nullable|string',
        ]);

        update_static_option('key_feature_section_status', $request->key_feature_section_status);
        update_static_option('full_width_features_section_status', $request->full_width_features_section_status);
        update_static_option('why_us_section_status', $request->why_us_section_status);
        update_static_option('testimonial_section_status', $request->testimonial_section_status);
        update_static_option('intro_video_section_status', $request->intro_video_section_status);
        update_static_option('price_plan_section_status', $request->price_plan_section_status);
        update_static_option('team_member_section_status', $request->team_member_section_status);
        update_static_option('contact_section_status', $request->contact_section_status);
        update_static_option('faq_section_status', $request->faq_section_status);
        update_static_option('brand_logo_section_status', $request->brand_logo_section_status);

        return redirect()->back()->with([
            'msg' => 'Settings Updated ...',
            'type' => 'success'
        ]);
    }
}
