<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Language;
use App\WhyChooseUs;
use App\Blog;
use App\ContactInfoItem;
use App\Counterup;
use App\KeyFeatures;
use App\PricePlan;
use App\Faq;
use App\TeamMember;
use App\Testimonial;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Artisan;

class AdminDashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }
    public function adminIndex(){

        $all_blogs = Blog::count();
        $total_admin = Admin::count();
        $total_testimonial = Testimonial::count();
        $total_team_member = TeamMember::count();
        $total_why_us = WhyChooseUs::count();
        $total_price_plan = PricePlan::count();
        $total_faq = Faq::count();
        $total_key_features = KeyFeatures::count();

        return view('backend.admin-home')->with([
            'blog_count' => $all_blogs,
            'total_admin' => $total_admin,
            'total_testimonial' => $total_testimonial,
            'total_team_member' => $total_team_member,
            'total_why_us' => $total_why_us,
            'total_price_plan' => $total_price_plan,
            'total_faq' => $total_faq,
            'total_key_features' => $total_key_features,
        ]);
    }
    public function site_identity(){
        return view('backend.general-settings.site-identity');
    }

    public function update_site_identity(Request $request){
        $this->validate($request,[
            'site_logo' => 'nullable|string',
            'site_favicon' => 'nullable|string',
            'site_breadcrumb_bg' => 'nullable|string',
            'site_white_logo' => 'nullable|string',
        ]);
        $fields = [
            'site_logo',
            'site_favicon',
            'site_breadcrumb_bg',
            'site_white_logo',
        ];
        foreach ($fields as $field){
            if ($request->has($field)){
                update_static_option($field, $request->$field);
            }
        }
        return redirect()->back()->with([
            'msg' => __('Settings Updated'),
            'type' => 'success'
        ]);
    }
    public function admin_settings(){
        return view('auth.admin.settings');
    }
    public function admin_profile_update(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191',
            'image' => 'nullable|string'
        ]);

        Admin::find(Auth::user()->id)->update(['name'=>$request->name,'email' => $request->email ,'image' => $request->image ]);
        return redirect()->back()->with(['msg' => 'Profile Update Success' , 'type' => 'success']);
    }
    public function admin_password_chagne(Request $request){
        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = Admin::findOrFail(Auth::guard('admin')->user()->id());

        if (Hash::check($request->old_password ,$user->password)){

            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();

            return redirect()->route('admin.login')->with(['msg'=>'Password Changed Successfully','type'=> 'success']);
        }

        return redirect()->back()->with(['msg'=>'Somethings Going Wrong! Please Try Again or Check Your Old Password','type'=> 'danger']);
    }
    public function adminLogout(){
        Auth::logout();
        return redirect()->route('admin.login')->with(['msg'=>'You Logged Out !!','type'=> 'danger']);
    }

    public function admin_profile(){
        return view('auth.admin.edit-profile');
    }
    public function admin_password(){
        return view('auth.admin.change-password');
    }

    public function blog_page(){
        $all_languages = Language::all();
        return view('backend.pages.blog')->with([
        'all_languages'=>$all_languages
        ]);
    }

    public function blog_page_update(Request $request){
        $all_languages = Language::all();
        $this->validate($request,[
            'blog_page_item' => 'required',
            'blog_page_recent_post_widget_item' => 'required',
        ]);
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'blog_page_title_' . $lang->slug=> 'nullable|string',
                'blog_page_category_widget_title_' . $lang->slug=> 'nullable|string',
                'blog_page_recent_post_widget_title_' . $lang->slug=> 'nullable|string'
            ]);
            $fields = [
                'blog_page_title_' . $lang->slug,
                'blog_page_category_widget_title_' . $lang->slug,
                'blog_page_recent_post_widget_title_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        $save_data = [
            'blog_page_item',
            'blog_page_recent_post_widget_item'
        ];
        foreach ($save_data as $data){
                update_static_option($data,$request->$data);
        }
        return redirect()->back()->with(['msg'=>__('Settings Update'),'type'=> 'success']);
    }
    public function basic_settings(){
        return view('backend.general-settings.basic');
    }
    public function update_basic_settings(Request $request){
        $this->validate($request,[
            'site_title' => 'required|string',
            'site_tag_line' => 'required|string',
            'site_footer_copyright' => 'required|string',
            'site_color' => 'required|string',
            'site_main_color_two' => 'required|string',
        ]);
        $save_data = [
            'site_title',
            'site_tag_line',
            'site_footer_copyright',
            'site_color',
            'site_main_color_two',
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}

            if($request->has($item)){
                update_static_option($item,$request->$item);
            }
        }
        return redirect()->back()->with(['msg'=> __('Settings Update'),'type'=> 'success']);
    }

    public function seo_settings(){
        return view('backend.general-settings.seo');
    }
    public function update_seo_settings(Request $request){
        $this->validate($request,[
            'site_meta_tags' => 'required|string',
            'site_meta_description' => 'required|string'
        ]);

        $save_data = [
            'site_meta_tags',
            'site_meta_description'
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }

        return redirect()->back()->with(['msg'=>__('SEO Settings Update'),'type'=> 'success']);
    }

    public function scripts_settings(){
        return view('backend.general-settings.thid-party');
    }

    public function update_scripts_settings(Request $request){

        $this->validate($request,[
            'site_disqus_key' => 'nullable|string',
            'tawk_api_key' => 'nullable|string',
            'site_google_analytics' => 'nullable|string',
        ]);

        $save_data = [
            'site_disqus_key',
            'site_google_analytics',
            'tawk_api_key'
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }

        return redirect()->back()->with(['msg'=>'Third Party Scripts Settings Updated..','type'=> 'success']);
    }
    public function email_template_settings(){
        return view('backend.general-settings.email-template');
    }

    public function update_email_template_settings(Request $request){

        $this->validate($request,[
            'site_global_email' => 'required|string',
            'site_global_email_template' => 'required|string',
        ]);

        $save_data = [
            'site_global_email',
            'site_global_email_template'
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }

        return redirect()->back()->with(['msg'=>'Email Settings Updated..','type'=> 'success']);
    }

    public function home_variant(){
        return view('backend.pages.home.home-variant');
    }

    public function update_home_variant(Request $request){
        $this->validate($request,[
           'home_page_variant' => 'required|string'
        ]);
        update_static_option('home_page_variant',$request->home_page_variant);
        return redirect()->back()->with(['msg'=>'Home Variant Settings Updated..','type'=> 'success']);
    }

    public function navbar_settings(){
        $all_languages = Language::all();
        return view('backend.pages.navbar-settings')->with(['all_languages' => $all_languages]);;
    }
    public function update_navbar_settings(Request $request){
        $all_languages = Language::all();
        $this->validate($request,[
            'navbar_button' => 'nullable|string',
        ]);
        foreach ($all_languages as $lang){
            $this->validate($request, [
                'navbar_button_text_' . $lang->slug=> 'nullable|string',
                'navbar_button_url_' . $lang->slug=> 'nullable|string',
            ]);
            $fields = [
                'navbar_button_text_' . $lang->slug,
                'navbar_button_url_' . $lang->slug,
            ];
            foreach ($fields as $field){
                if ($request->has($field)){
                    update_static_option($field,$request->$field);
                }
            }
        }
        update_static_option('navbar_button',$request->navbar_button);

        return redirect()->back()->with(['msg'=>'Navbar Settings Updated..','type'=> 'success']);
    }

    public function typography_settings(){
        $all_google_fonts = file_get_contents('assets/frontend/webfonts/google-fonts.json');
        return view('backend.general-settings.typograhpy')->with(['google_fonts' => json_decode($all_google_fonts)]);
    }
    public function update_typography_settings(Request $request){
        $this->validate($request,[
            'body_font_family' => 'required|string|max:191',
            'body_font_variant' => 'required',
            'heading_font' => 'nullable|string',
            'heading_font_family' => 'nullable|string|max:191',
            'heading_font_variant' => 'nullable',
        ]);

        $save_data = [
            'body_font_family',
            'heading_font_family',
        ];
        foreach ($save_data as $item){
            if (empty($request->$item)){continue;}
            update_static_option($item,$request->$item);
        }
        update_static_option('heading_font',$request->heading_font);
        update_static_option('body_font_variant',serialize($request->body_font_variant));
        update_static_option('heading_font_variant',serialize($request->heading_font_variant));

        return redirect()->back()->with(['msg'=>'Typography Settings Updated..','type'=> 'success']);
    }
    
    public function cache_settings(){
          return view('backend.general-settings.cache-settings');
    }
    
    public function update_cache_settings(Request $request){
        
         $this->validate($request,[
            'cache_type' => 'required|string'
        ]);
        
        Artisan::call($request->cache_type.':clear');
        
        return redirect()->back()->with(['msg'=>'Cache Cleaned...','type'=> 'success']);
    }
}
