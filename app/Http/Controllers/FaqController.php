<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Language;
use Illuminate\Http\Request;

class FaqController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $all_faq = Faq::all()->groupBy('lang');
        $all_languages = Language::all();
        return view('backend.pages.faq')->with([
            'all_faqs' => $all_faq,
            'all_languages' => $all_languages,
            ]);
    }

    public function store(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'description' => 'required|string',
            'lang' => 'required|string'
        ]);

        Faq::create($request->all());

        return redirect()->back()->with(['msg' => 'New Faq Added...','type' => 'success']);
    }

    public function update(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'description' => 'required|string',
            'lang' => 'required|string'
        ]);
        Faq::find($request->id)->update(['title' => $request->title,'description' => $request->description,'lang' => $request->lang]);
        return redirect()->back()->with(['msg' => 'Faq Updated...','type' => 'success']);
    }

    public function delete($id){
        Faq::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Delete Success...','type' => 'danger']);
    }
    public function bulk_action(Request $request){
        $all = Faq::find($request->ids);
        foreach($all as $item){
            $item->delete();
        }
        return response()->json(['status' => 'ok']);
    }
}
