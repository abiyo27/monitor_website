<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Page;
use App\WhyChooseUs;
use App\Blog;
use App\BlogCategory;
use App\FullWidthFeatrues;
use App\KeyFeatures;
use App\PricePlan;
use App\Faq;
use App\Language;
use App\Mail\AdminResetEmail;
use App\Mail\BasicMail;
use App\Order;
use App\PaymentLogs;
use App\TeamMember;
use App\User;
use App\Testimonial;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FrontendController extends Controller
{
    public function index(){
        $user_lang = get_user_lang();
        $all_key_features = KeyFeatures::where(['lang' => $user_lang])->get();
        $why_choose_us = WhyChooseUs::where(['lang' => $user_lang])->get();
        $all_testimonial = Testimonial::where(['lang' => $user_lang])->get();
        $all_price_plan = PricePlan::where(['lang' => $user_lang])->get();
        $all_full_width_features = FullWidthFeatrues::where(['lang' => $user_lang])->get();
        $all_faqs = Faq::where(['lang' => $user_lang])->get();
        $all_team_members = TeamMember::where(['lang' => $user_lang])->get();
        $all_blog = Blog::where(['lang' => $user_lang])->orderBy('id','desc')->take(get_static_option('news_update_number'))->get();

        return view('frontend.frontend-home')->with([
            'all_key_features' => $all_key_features,
            'why_choose_us' => $why_choose_us,
            'all_testimonial' => $all_testimonial,
            'all_blog' => $all_blog,
            'all_full_width_features' => $all_full_width_features,
            'all_faqs' => $all_faqs,
            'all_price_plan' => $all_price_plan,
            'all_team_members' => $all_team_members,
        ]);
    }
    public function ajax_login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required'),
            'password.min' => __('password length must be 6 characters')
        ]);

        if (Auth::guard('web')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {
            return response()->json([
                'msg' => __('login Success Redirecting'),
                'type' => 'danger',
                'status' => 'valid'
            ]);
        }
        return response()->json([
            'msg' => __('Username Or Password Doest Not Matched !!!'),
            'type' => 'danger',
            'status' => 'invalid'
        ]);
    }
    public function blog_page(){
        $user_lang = get_user_lang();
        $all_recent_blogs = Blog::where('lang',$user_lang)->orderBy('id','desc')->take(get_static_option('blog_page_recent_post_widget_item'))->get();
        $all_blogs = Blog::where('lang',$user_lang)->orderBy('id','desc')->paginate(get_static_option('blog_page_item'));
        $all_category = BlogCategory::where(['status'=>'publish','lang' => $user_lang])->orderBy('id','desc')->get();
        return view('frontend.pages.blog')->with([
            'all_blogs' => $all_blogs,
            'all_categories' => $all_category,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }
    public function category_wise_blog_page($id){
        $user_lang = get_user_lang();
        $all_blogs = Blog::where(['blog_categories_id' => $id,'lang' => $user_lang])->orderBy('id','desc')->paginate(get_static_option('blog_page_item'));
        $all_recent_blogs = Blog::where('lang',$user_lang)->orderBy('id','desc')->take(get_static_option('blog_page_recent_post_widget_item'))->get();
        $all_category = BlogCategory::where(['status'=>'publish','lang' => $user_lang])->orderBy('id','desc')->get();
        $category_name = BlogCategory::where(['id'=>$id,'status' => 'publish'])->first()->name;
        return view('frontend.pages.blog-category')->with([
            'all_blogs' => $all_blogs,
            'all_categories' => $all_category,
            'category_name' => $category_name,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }
    public function blog_search_page(Request $request){
        $user_lang = get_user_lang();
        $all_recent_blogs = Blog::where(['lang' => $user_lang])->orderBy('id','desc')->take(get_static_option('blog_page_recent_post_widget_item'))->get();
        $all_category = BlogCategory::where(['status' => 'publish','lang' => $user_lang])->orderBy('id','desc')->get();
        $all_blogs = Blog::where('title','LIKE','%'.$request->search.'%')
            ->orWhere('content','LIKE','%'.$request->search.'%')
            ->orWhere('tags','LIKE','%'.$request->search.'%')
            ->orderBy('id','desc')->paginate(get_static_option('blog_page_item'));

        return view('frontend.pages.blog-search')->with([
            'all_blogs' => $all_blogs,
            'all_categories' => $all_category,
            'search_term' => $request->search,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }

    public function blog_single_page($id,$any){

        $blog_post = Blog::where('id',$id)->first();
        $all_recent_blogs = Blog::where(['lang' => $blog_post->lang])->orderBy('id','desc')->paginate(get_static_option('blog_page_recent_post_widget_item'));
        $all_category = BlogCategory::where(['status' => 'publish','lang' => $blog_post->lang])->orderBy('id','desc')->get();
        return view('frontend.pages.blog-single')->with([
            'blog_post' => $blog_post,
            'all_categories' => $all_category,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }

    public function showAdminForgetPasswordForm(){
        return view('auth.admin.forget-password');
    }
    public function sendAdminForgetPasswordMail(Request $request){
        $this->validate($request,[
           'username' => 'required|string:max:191'
        ]);
        $user_info = Admin::where('username',$request->username)->orWhere('email',$request->username)->first();
        $token_id = Str::random(30);
        $existing_token = DB::table('password_resets')->where('email',$user_info->email)->delete();
        if (empty($existing_token)){
            DB::table('password_resets')->insert(['email' => $user_info->email, 'token' => $token_id]);
        }

        $message = 'Here is you password reset link, If you did not request to reset your password just ignore this mail. <a style="background-color:#444;color:#fff;text-decoration:none;padding: 10px 15px;border-radius: 3px;display: block;width: 130px;margin-top: 20px;" href="'.route('admin.reset.password',['user'=>$user_info->username,'token' => $token_id]).'">Click Reset Password</a>';
        if (sendEmail($user_info->email, $user_info->username, 'Reset Your Password', $message)){
            return redirect()->back()->with([
                'msg' => 'Check Your Mail For Reset Password Link',
                'type' => 'success'
            ]);
        }
        return redirect()->back()->with([
            'msg' => 'Something Wrong, Please Try Again!!',
            'type' => 'danger'
        ]);
    }

    public function showAdminResetPasswordForm($username,$token){
        return view('auth.admin.reset-password')->with([
            'username' => $username,
            'token' => $token
        ]);
    }
    public function AdminResetPassword(Request $request){
        $this->validate($request, [
            'token' => 'required',
            'username' => 'required',
            'password' => 'required|string|min:8|confirmed'
        ]);
        $user_info = Admin::where('username',$request->username)->first();
        $user = Admin::findOrFail($user_info->id);
        $token_iinfo = DB::table('password_resets')->where(['email' => $user_info->email,'token' => $request->token])->first();
        if (!empty($token_iinfo)){
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('admin.login')->with(['msg'=>'Password Changed Successfully','type'=> 'success']);
        }

        return redirect()->back()->with(['msg'=>'Somethings Going Wrong! Please Try Again or Check Your Old Password','type'=> 'danger']);
    }

    public function lang_change(Request $request){
        session()->put('lang', $request->lang);
        return redirect()->route('homepage');
    }

    public function home_page_change($id){
        
        $user_lang = get_user_lang();
        $all_key_features = KeyFeatures::where(['lang' => $user_lang])->get();
        $why_choose_us = WhyChooseUs::where(['lang' => $user_lang])->get();
        $all_testimonial = Testimonial::where(['lang' => $user_lang])->get();
        $all_price_plan = PricePlan::where(['lang' => $user_lang])->get();
        $all_full_width_features = FullWidthFeatrues::where(['lang' => $user_lang])->get();
        $all_faqs = Faq::where(['lang' => $user_lang])->get();
        $all_team_members = TeamMember::where(['lang' => $user_lang])->get();
        $all_blog = Blog::where(['lang' => $user_lang])->orderBy('id','desc')->take(get_static_option('news_update_number'))->get();
        
        return view('frontend.frontend-home-demo')->with([
            'home_page' => $id,
            'all_key_features' => $all_key_features,
            'why_choose_us' => $why_choose_us,
            'all_testimonial' => $all_testimonial,
            'all_blog' => $all_blog,
            'all_full_width_features' => $all_full_width_features,
            'all_faqs' => $all_faqs,
            'all_price_plan' => $all_price_plan,
            'all_team_members' => $all_team_members,
        ]);

    }

    public function send_contact_message(Request $request){
        $this->validate($request,[
            'email' => 'required|email|max:191',
            'name' => 'required|string|max:191',
            'subject' => 'required|string|max:191',
            'message' => 'required'
        ]);

        $subject = 'From '.get_static_option('site_title').' '.$request->subject;

        if (sendPlanEmail(get_static_option('site_global_email'),$request->name,$subject,$request->message,$request->email)){
            return redirect()->back()->with(['msg' => 'Thanks for your contact!!','type' => 'success']);
        }

        return redirect()->back()->with(['msg' => 'Something goes wrong, Please try again later !!','type' => 'danger']);
    }

    public function full_width_feature_details($id,$any){
        $single_feature = FullWidthFeatrues::where('id',$id)->first();

        return view('frontend.pages.feature-single')->with(['feature' => $single_feature]);
    }
    public function dynamic_single_page($slug){
        $page_post = Page::where('slug', $slug)->first();
        return view('frontend.pages.dynamic-single')->with([
            'page_post' => $page_post
        ]);
    }
    public function showUserForgetPasswordForm()
    {
        return view('frontend.user.forget-password');
    }

    public function sendUserForgetPasswordMail(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string:max:191'
        ]);
        $user_info = User::where('username', $request->username)->orWhere('email', $request->username)->first();
        if (!empty($user_info)) {
            $token_id = Str::random(30);
            $existing_token = DB::table('password_resets')->where('email', $user_info->email)->delete();
            if (empty($existing_token)) {
                DB::table('password_resets')->insert(['email' => $user_info->email, 'token' => $token_id]);
            }
            $message = __('Here is you password reset link, If you did not request to reset your password just ignore this mail.') . ' <a class="btn" href="' . route('user.reset.password', ['user' => $user_info->username, 'token' => $token_id]) . '">' . __('Click Reset Password') . '</a>';
            $data = [
                'username' => $user_info->username,
                'message' => $message
            ];
            Mail::to($user_info->email)->send(new AdminResetEmail($data));

            return redirect()->back()->with([
                'msg' => __('Check Your Mail For Reset Password Link'),
                'type' => 'success'
            ]);
        }
        return redirect()->back()->with([
            'msg' => __('Your Username or Email Is Wrong!!!'),
            'type' => 'danger'
        ]);
    }

    public function showUserResetPasswordForm($username, $token)
    {
        return view('frontend.user.reset-password')->with([
            'username' => $username,
            'token' => $token
        ]);
    }
    public function UserResetPassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'username' => 'required',
            'password' => 'required|string|min:8|confirmed'
        ]);
        $user_info = User::where('username', $request->username)->first();
        $user = User::findOrFail($user_info->id);
        $token_iinfo = DB::table('password_resets')->where(['email' => $user_info->email, 'token' => $request->token])->first();
        if (!empty($token_iinfo)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('user.login')->with(['msg' => __('Password Changed Successfully'), 'type' => 'success']);
        }

        return redirect()->back()->with(['msg' => __('Somethings Going Wrong! Please Try Again or Check Your Old Password'), 'type' => 'danger']);
    }
    public function price_plan_page()
    {
        $default_lang = Language::where('default', 1)->first();
        $lang = !empty(session()->get('lang')) ? session()->get('lang') : $default_lang->slug;
        $all_price_plan = PricePlan::where(['lang' => $lang])->get()->groupBy('categories_id');
        return view('frontend.pages.price-plan')->with(['all_price_plan' => $all_price_plan]);
    }

    public function order_confirm($id)
    {
        $order_details = Order::find($id);
        return view('frontend.payment.order-confirm')->with(['order_details' => $order_details]);
    }
    public function plan_order($id)
    {
        $order_details = PricePlan::find($id);
        return view('frontend.pages.package.order-page')->with([
            'order_details' => $order_details
        ]);
    }
    public function send_order_message(Request $request)
    {

        $validated_data = $this->get_filtered_data_from_request(get_static_option('order_page_form_fields'),$request);
        $all_attachment = $validated_data['all_attachment'];
        $all_field_serialize_data = $validated_data['field_data'];
        $package_detials = PricePlan::find($request->package);

        $order_id =Order::create([
            'custom_fields' => serialize($all_field_serialize_data),
            'attachment' => serialize($all_attachment),
            'status' => 'pending',
            'package_name' => $package_detials->title,
            'package_price' => $package_detials->price,
            'package_id' => $package_detials->id,
            'checkout_type' => !empty($request->checkout_type) ? $request->checkout_type : '',
            'user_id' => Auth::guard('web')->check() ? Auth::guard('web')->user()->id : 0,
        ])->id;

        if (!empty(get_static_option('site_payment_gateway'))) {
            return redirect()->route('frontend.order.confirm', $order_id);
        }


        $google_captcha_result = google_captcha_check($request->captcha_token);
        if ($google_captcha_result['success']) {

            $succ_msg = get_static_option('order_mail_' . get_user_lang() . '_success_message');
            $success_message = !empty($succ_msg) ? $succ_msg : __('Thanks for your order. we will get back to you very soon.');
            $order_rmail = get_static_option('order_page_form_mail');
            $order_mail = $order_rmail ? $order_rmail : get_static_option('site_global_email');
            //have to set condition for redirect in payment page with payment information
            if (!empty(get_static_option('site_payment_gateway'))) {
                return redirect()->route('frontend.order.confirm', $order_id);
            }
            Mail::to($order_mail)->send(new BasicMail([
                'subject' => __('You have a package order from').' '.get_static_option('site_'.get_default_language().'_title'),
                'message' => '',
            ]));
            return redirect()->back()->with(['msg' => $success_message, 'type' => 'success']);

        } else {
            return redirect()->back()->with(['msg' => __('Something goes wrong, Please try again later !!'), 'type' => 'danger']);
        }

    }
    public function order_payment_success($id)
    {
        $extract_id = substr($id,6);
        $extract_id =  substr($extract_id,0,-6);
        $order_details = Order::find($extract_id);
        if (empty($order_details)){
            return view('frontend.pages.404');
        }
        $package_details = PricePlan::find($order_details->package_id);
        $payment_details = PaymentLogs::where('order_id', $extract_id)->first();
        return view('frontend.payment.payment-success')->with(
            [
                'order_details' => $order_details,
                'package_details' => $package_details,
                'payment_details' => $payment_details,
            ]);
    }

    public function order_payment_cancel($id)
    {
        $order_details = Order::find($id);
        return view('frontend.payment.payment-cancel')->with(['order_details' => $order_details]);
    }
    public function flutterwave_pay_get()
    {
        return redirect_404_page();
    }
    public function generate_package_invoice(Request $request)
    {
        $payment_details = PaymentLogs::where(['order_id' => $request->id])->first();
        $order_details = Order::where(['id' => $request->id])->first();
        if (empty($order_details)) {
            return redirect_404_page();
        }
        $pdf = PDF::loadView('invoice.package-order', ['order_details' => $order_details,'payment_details' => $payment_details]);
        return $pdf->download('package-invoice.pdf');
    }
    public function order_details($id){
        $order_details = Order::find($id);
        $package_details = PricePlan::find($order_details->package_id);
        $payment_details = PaymentLogs::where('order_id', $id)->first();
        return view('frontend.pages.package.view-order')->with(
            [
                'order_details' => $order_details,
                'package_details' => $package_details,
                'payment_details' => $payment_details,
            ]);
    }


}
