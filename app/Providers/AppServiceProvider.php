<?php

namespace App\Providers;

use App\Blog;
use App\DonorsCategory;
use App\Importantlink;
use App\Language;
use App\SocialIcons;
use App\SupportInfo;
use App\UsefulLink;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
          $lang = !empty(session()->get('lang')) ? session()->get('lang') : Language::where('default',1)->first()->slug;

        $all_social_item = SocialIcons::all();
        $all_support_item = SupportInfo::all();
        $all_usefull_links = UsefulLink::all();
        $all_important_links = Importantlink::all();
        $all_recent_post = Blog::where(['lang' =>$lang ])->orderBy('id', 'DESC')->take(get_static_option('recent_post_widget_item'))->get();
        $all_language = Language::all();
        view::share([
            'all_usefull_links' => $all_usefull_links,
            'all_important_links' => $all_important_links,
            'all_recent_post' => $all_recent_post,
            'all_support_item' => $all_support_item,
            'all_social_item' => $all_social_item,
            'all_language' => $all_language
        ]);
    }
}
