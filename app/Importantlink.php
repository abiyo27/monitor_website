<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importantlink extends Model
{
    protected $table = 'importantlinks';
    protected $fillable = ['title','url','lang'];
}
