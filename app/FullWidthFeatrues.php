<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FullWidthFeatrues extends Model
{
    protected $table = 'full_width_featrues';
    protected $fillable = ['title','description','btn_status','image','lang'];
}
